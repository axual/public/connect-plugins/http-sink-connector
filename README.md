HTTP Sink Connector for Kafka Connect 
==========================================
[![Pipeline Status](https://gitlab.com/axual/public/connect-plugins/http-sink-connector/badges/master/pipeline.svg)](https://gitlab.com/connect-plugins/http-sink-connector/commits/master) 
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=axual-public-connect-plugins-http-sink-connector&metric=coverage&token=bd6c44e7cbdc4df9ccd4fbbc8972adc63d1ee91f)](https://sonarcloud.io/dashboard?id=axual-public-connect-plugins-http-sink-connector)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=axual-public-connect-plugins-http-sink-connector&metric=sqale_rating&token=bd6c44e7cbdc4df9ccd4fbbc8972adc63d1ee91f)](https://sonarcloud.io/dashboard?id=axual-public-connect-plugins-http-sink-connector)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=axual-public-connect-plugins-http-sink-connector&metric=alert_status&token=bd6c44e7cbdc4df9ccd4fbbc8972adc63d1ee91f)](https://sonarcloud.io/dashboard?id=axual-public-connect-plugins-http-sink-connector)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)


The HTTP Sink Connector is a connector to push records from Kafka topics to a remote HTTP Server.
The connector will by default create a JSON payload containing the topic name, record key and value.

The connector supports:
* All HTTP methods
* Static HTTP Header definition
* Kafka Header to HTTP Header forwarding
* Configurable HTTP Redirect support
* Configurable TLS Settings, including specifying valid Certificate Authorities
* TLS with Client Certificate support
* HTTP Basic Authentication

How to use
----------
The installation and configuration guidelines can be found on the [Project Wiki](https://gitlab.com/axual/public/connect-plugins/http-sink-connector/-/wikis/home)


Known Limitations
-----------------
* No dynamic endpoint support, with paths and/or query strings created from the Kafka record
* Kafka Header forwarding limited to Kafka Headers stored as UTF-8 Strings

Contributing
-----------------

Axual is interested in building the community; we would welcome any thoughts or 
[patches](https://gitlab.com/axual/public/connect-plugins/http-sink-connector/-/issues).
You can reach us [here](https://axual.com/contact/).

See [contributing](https://gitlab.com/axual/public/connect-plugins/http-sink-connector/blob/master/CONTRIBUTING.md).


License
-------
HTTP Sink Connector is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).
