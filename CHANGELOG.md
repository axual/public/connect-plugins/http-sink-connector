# Changelog
All notable changes to this project will be documented in this file.

## [1.1.0] 2024-07-19
* Update dependencies of project, based on Kafka Connect API 3.7.0. Works with Kafka Connect 2.8.2 

## [1.0.2] 2023-05-23
* [Issue #7](https://gitlab.com/axual/public/connect-plugins/http-sink-connector/-/issues/7) - Not possible to set charset as part of content type property
* [Dependency Update](https://gitlab.com/axual/public/connect-plugins/http-sink-connector/-/issues/6) - Updating dependencies and plugin versions
  Dependencies for compiling connector
  * Kafka Connect 2.5.0 -> 2.8.2

  Dependencies included with connector
  * Apache HttpComponents 4.5.12 -> 4.5.14
  * Jackson Databind 2.11.1 -> 2.14.2

  Test dependencies:
  * Junit Jupiter 5.6.2 -> 5.9.2
  * Mockito 3.3.3 -> 5.2.0
  * Slf4j 1.7.30 -> 1.7.36
  * Guava 29.0-jre -> 31.1-jre
  * Mockito 3.12.4 -> 5.2.0
  * Wiremock 2.27.1 -> 2.27.2

  Plugins:
  * jacoco-maven-plugin 0.8.5 -> 0.8.8
  * nexus-staging-maven-plugin 1.6.7 -> 1.6.13
  * license-maven-plugin 1.9 -> 1.20
  * maven-clean-plugin 3.1.0 -> 3.2.0
  * maven-compiler-plugin 3.8.1 -> 3.11.0
  * maven-jar-plugin 3.2.0 -> 3.3.0
  * maven-javadoc-plugin 3.2.0 -> 3.5.0
  * maven-assembly-plugin 3.2.0 -> 3.5.0
  * maven-resources-plugin 3.1.0 -> 3.3.0
  * maven-failsafe-plugin 3.0.0-M4 -> 3.0.0
  * maven-surefire-plugin 3.0.0-M4 -> 3.0.0
  * maven-deploy-plugin 2.8.2 -> 3.1.0
  * maven-install-plugin 3.2.0 -> 3.5.0

  Build pipeline:
  * pipeline-tools 1.6.0 -> 1.8.1
  * Added Dependency Track SBOM upload job to pipeline definition

  Removed plugin:
  * owasp dependency check plugin, replaced by Dependency Track


## [1.0.1] - 2023-03-24
* [Issue #4](https://gitlab.com/axual/public/connect-plugins/http-sink-connector/-/issues/4) -
  Add returned status information for not calls returning a non-OK status code

## [1.0.0] - 2020-09-04
* Initial release of HTTP Sink Connector

[master]:https://gitlab.com/axual/public/connect-plugins/http-sink-connector/-/compare/1.1.0...master
[1.1.0]:https://gitlab.com/axual/public/connect-plugins/http-sink-connector/-/compare/1.0.2...1.1.0
[1.0.2]:https://gitlab.com/axual/public/connect-plugins/http-sink-connector/-/compare/1.0.1...1.0.2
[1.0.1]:https://gitlab.com/axual/public/connect-plugins/http-sink-connector/-/compare/1.0.0...1.0.1
[1.0.0]: https://gitlab.com/axual/public/connect-plugins/http-sink-connector/-/tree/1.0.0
