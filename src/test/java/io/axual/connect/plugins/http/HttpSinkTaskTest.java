package io.axual.connect.plugins.http;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import io.axual.connect.plugins.http.exceptions.HttpSinkConnectorException;
import io.axual.connect.plugins.http.exceptions.MessageFormattingException;
import io.axual.connect.plugins.http.helpers.SslHelper;
import io.axual.connect.plugins.http.sender.HttpSender;
import io.axual.connect.plugins.http.sender.HttpSenderConfiguration;
import io.axual.connect.plugins.http.sender.HttpSenderResult;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.message.BasicStatusLine;
import org.apache.kafka.connect.sink.SinkRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HttpSinkTaskTest {

  public static final String EXPECTED_VERSION = "testing";
  public static final String EXPECTED_ENDPOINT = "http://somewhere.totest";
  public static final String EXPECTED_METHOD = "POST";

  @Mock
  HttpSender mockedSender;

  Map<String, String> getConfigs() {
    Map<String, String> configs = new HashMap<>();
    configs.put(HttpSinkConnectorConfig.ENDPOINT_CONFIG, EXPECTED_ENDPOINT);
    return configs;
  }

  @Test
  void version() {
    HttpSinkTask task = new HttpSinkTask();
    assertEquals(EXPECTED_VERSION, task.version());
  }

  @Test
  void start() {
    HttpSinkTask task = new HttpSinkTask();
    task.start(getConfigs());
    assertNotNull(task.config);
    assertNotNull(task.httpSenderConfiguration);
    assertEquals(EXPECTED_ENDPOINT, task.httpSenderConfiguration.getEndpoint());
    assertEquals(EXPECTED_METHOD, task.httpSenderConfiguration.getHttpMethod());
  }

  @Test
  void put_success() {
    HttpSinkTask task = new HttpSinkTask(mockedSender, SslHelper.INSTANCE);
    SinkRecord mockRecord = mock(SinkRecord.class);
    StatusLine statusLine = new BasicStatusLine(new ProtocolVersion("http", 1, 1), 204,
        "OK - Nothing to see");
    HttpSenderResult senderResult = new HttpSenderResult(true, statusLine, null);

    doReturn(senderResult).when(mockedSender).sendRecord(eq(mockRecord));

    Set<SinkRecord> records = Collections.singleton(mockRecord);
    assertDoesNotThrow(() -> task.put(records));
    verify(mockedSender, times(1)).sendRecord(eq(mockRecord));
  }

  @Test
  void put_failedWithException() {
    HttpSinkTask task = new HttpSinkTask(mockedSender, SslHelper.INSTANCE);
    SinkRecord mockRecord = mock(SinkRecord.class);
    MessageFormattingException exception = new MessageFormattingException("Could not format record content to HTTP request body");
    HttpSenderResult senderResult = new HttpSenderResult(false, null, exception);

    doReturn(senderResult).when(mockedSender).sendRecord(eq(mockRecord));

    Set<SinkRecord> records = Collections.singleton(mockRecord);
    assertThrows(HttpSinkConnectorException.class,
        () -> task.put(records));
    verify(mockedSender, times(1)).sendRecord(eq(mockRecord));
  }

  @Test
  void put_failedWithStatusLine() {
    HttpSinkTask task = new HttpSinkTask(mockedSender, SslHelper.INSTANCE);
    SinkRecord mockRecord = mock(SinkRecord.class);
    StatusLine statusLine = new BasicStatusLine(new ProtocolVersion("http", 1, 1), 500,
        "OK - Nothing to see");
    HttpSenderResult senderResult = new HttpSenderResult(false, statusLine, null);

    doReturn(senderResult).when(mockedSender).sendRecord(eq(mockRecord));

    Set<SinkRecord> records = Collections.singleton(mockRecord);
    assertThrows(HttpSinkConnectorException.class,
        () -> task.put(records));
    verify(mockedSender, times(1)).sendRecord(eq(mockRecord));
  }


  @Test
  void stop() {
    HttpSinkTask task = new HttpSinkTask(mockedSender, SslHelper.INSTANCE);
    task.httpSenderConfiguration = mock(HttpSenderConfiguration.class);
    task.config = mock(HttpSinkConnectorConfig.class);
    task.stop();
    assertNull(task.httpSenderConfiguration);
    assertNull(task.config);
    verify(mockedSender, times(1)).close();
  }
}
