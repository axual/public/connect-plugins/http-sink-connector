package io.axual.connect.plugins.http.helpers;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.google.common.io.Resources;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.KeyStore.Entry;
import java.security.KeyStore.TrustedCertificateEntry;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Stream;
import javax.net.ssl.SSLContext;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SslHelperTest {

  private static final String PEM_SINGLE_CA_PATH;
  private static final String PEM_SINGLE_CA;
  private static final String PEM_SINGLE_CA_FINGERPRINT_0 = cleanFingerprint(
      "F0:33:EA:31:A1:58:5F:11:69:AA:D7:FD:08:39:7E:47:74:09:61:01");

  private static final String PEM_MULTIPLE_CA_PATH;
  private static final String PEM_MULTIPLE_CA;
  private static final String PEM_MULTIPLE_CA_FINGERPRINT_0 = cleanFingerprint(
      "E1:A0:90:61:56:55:14:46:60:B5:B1:48:F0:FF:3C:EF:29:2D:70:46");
  private static final String PEM_MULTIPLE_CA_FINGERPRINT_1 = cleanFingerprint(
      "F0:33:EA:31:A1:58:5F:11:69:AA:D7:FD:08:39:7E:47:74:09:61:01");

  static {
    try {
      URL single_ca_url = Resources.getResource("ssl/ca_single.cer");
      URL multiple_ca_url = Resources.getResource("ssl/ca_multiple.cer");
      PEM_SINGLE_CA = Resources
          .toString(single_ca_url, StandardCharsets.UTF_8);
      PEM_MULTIPLE_CA = Resources
          .toString(multiple_ca_url, StandardCharsets.UTF_8);

      PEM_SINGLE_CA_PATH = new File(single_ca_url.toURI()).getAbsolutePath();
      PEM_MULTIPLE_CA_PATH = new File(multiple_ca_url.toURI()).getAbsolutePath();
    } catch (IOException | URISyntaxException ioe) {
      throw new RuntimeException("Could not load resource", ioe);
    }
  }

  @ParameterizedTest
  @MethodSource
  void getContext(Collection<String> certificateFiles, Collection<String> certificateStrings, Collection<String> expectedCertificateStrings) {
    SslHelper helper = spy(SslHelper.INSTANCE);
    SSLContext context = helper.getContext(certificateFiles,certificateStrings);
    assertNotNull(context);
    ArgumentCaptor<Collection<String>> stringsCaptor = ArgumentCaptor.forClass(Collection.class);
    verify(helper,times(1)).openTruststore(stringsCaptor.capture());

    Collection<String> caughCertificateStrings = stringsCaptor.getValue();
    assertArrayEquals(expectedCertificateStrings.toArray(),caughCertificateStrings.toArray());
  }

  private static Stream<Arguments> getContext(){
    return Stream.of(
        Arguments.of(
            Arrays.asList(
                PEM_SINGLE_CA_PATH),
            Arrays.asList(
                PEM_MULTIPLE_CA),
            Arrays.asList(
                PEM_MULTIPLE_CA,
                PEM_SINGLE_CA)),
        Arguments.of(
            Arrays.asList(
                PEM_SINGLE_CA_PATH,
                PEM_MULTIPLE_CA_PATH),
            Arrays.asList
                (PEM_MULTIPLE_CA),
            Arrays.asList(
                PEM_MULTIPLE_CA,
                PEM_SINGLE_CA,
                PEM_MULTIPLE_CA)),
        Arguments.of(
            Arrays.asList(
                PEM_SINGLE_CA_PATH,
                PEM_MULTIPLE_CA_PATH),
            Arrays.asList(
                PEM_MULTIPLE_CA,
                PEM_SINGLE_CA),
            Arrays.asList(
                PEM_MULTIPLE_CA,
                PEM_SINGLE_CA,
                PEM_SINGLE_CA,
                PEM_MULTIPLE_CA)),
        Arguments.of(
            singletonList(PEM_SINGLE_CA_PATH),
            emptyList(),
            singletonList(PEM_SINGLE_CA)),
        Arguments.of(
            emptyList(),
            singletonList(PEM_SINGLE_CA),
            singletonList(PEM_SINGLE_CA)),
        Arguments.of(
            singletonList(PEM_SINGLE_CA_PATH),
            null,
            singletonList(PEM_SINGLE_CA)),
        Arguments.of(
            null,
            singletonList(PEM_SINGLE_CA),
            singletonList(PEM_SINGLE_CA))
    );
  }

  @ParameterizedTest
  @MethodSource
  void getContextNoContent(Collection<String> certificateFiles, Collection<String> certificateStrings, Collection<String> expectedCertificateStrings) {
    SslHelper helper = spy(SslHelper.INSTANCE);
    SSLContext context = helper.getContext(certificateFiles,certificateStrings);
    assertNotNull(context);
    verify(helper,times(0)).openTruststore(any());
  }

  private static Stream<Arguments> getContextNoContent(){
    return Stream.of(
        Arguments.of(
            null,
            null,
            emptyList()),
        Arguments.of(
            emptyList(),
            emptyList(),
            emptyList())
    );
  }

  @Test
  void openTruststore()
      throws KeyStoreException, UnrecoverableEntryException, NoSuchAlgorithmException, CertificateEncodingException {
    Collection<String> caStrings = Arrays.asList(PEM_SINGLE_CA, PEM_MULTIPLE_CA);
    KeyStore keyStore = SslHelper.INSTANCE.openTruststore(caStrings);

    final int expectedRoot = 2; // One from PEM_SINGLE_CA and one from PEM_MULTIPLE_CA
    final int expectedInt = 1; // one from PEM_MULTIPLE_CA
    final int expectedSize = expectedRoot+expectedInt;
    assertNotNull(keyStore);
    assertEquals(expectedSize,keyStore.size());

    int countRoot = 0;
    int countInt = 0;
    Enumeration<String> iterator = keyStore.aliases();
    while (iterator.hasMoreElements()) {
      Entry entry = keyStore.getEntry(iterator.nextElement(), null);
      if(entry instanceof TrustedCertificateEntry){
        String fingerprint =  fingerprint(((TrustedCertificateEntry)entry).getTrustedCertificate().getEncoded());
        if(PEM_MULTIPLE_CA_FINGERPRINT_0.equals(fingerprint)){
          countInt++;
          continue;
        }
        if(PEM_MULTIPLE_CA_FINGERPRINT_1.equals(fingerprint)){
          countRoot++;
          continue;
        }
      }
    }

    assertEquals(expectedRoot,countRoot);
    assertEquals(expectedInt,countInt);
  }

  @ParameterizedTest(name = "{displayName}[{index}]")
  @MethodSource
  void getCertificatesFromFiles(Collection<String> files, Collection<String> fingerprints)
      throws CertificateEncodingException {
    Collection<String> certificateStrings = SslHelper.INSTANCE
        .getCertificateStringsFromFiles(files);
    getCertificatesFromString(certificateStrings, fingerprints);
  }

  private static Stream<Arguments> getCertificatesFromFiles() {
    return Stream.of(
        Arguments.of(Arrays.asList(PEM_SINGLE_CA_PATH), Arrays.asList(PEM_SINGLE_CA_FINGERPRINT_0)),
        Arguments.of(Arrays.asList(PEM_MULTIPLE_CA_PATH),
            Arrays.asList(PEM_MULTIPLE_CA_FINGERPRINT_0, PEM_MULTIPLE_CA_FINGERPRINT_1)),
        Arguments.arguments(Arrays.asList(PEM_SINGLE_CA_PATH, PEM_MULTIPLE_CA_PATH), Arrays
            .asList(PEM_SINGLE_CA_FINGERPRINT_0, PEM_MULTIPLE_CA_FINGERPRINT_0,
                PEM_MULTIPLE_CA_FINGERPRINT_1))
    );
  }


  @ParameterizedTest(name = "{displayName}[{index}]")
  @MethodSource
  void getCertificatesFromString(Collection<String> certificateStrings,
      Collection<String> fingerprints)
      throws CertificateEncodingException {
    Collection<Certificate> certificates = SslHelper.INSTANCE
        .getCertificatesFromString(certificateStrings);
    assertNotNull(certificates);
    assertEquals(fingerprints.size(), certificates.size());
    List<String> fingerprinted = new ArrayList<>(certificates.size());
    for (Certificate certificate : certificates) {
      fingerprinted.add(fingerprint(certificate.getEncoded()));
    }
    assertArrayEquals(fingerprints.toArray(), fingerprinted.toArray());
  }

  private static Stream<Arguments> getCertificatesFromString() {
    return Stream.of(
        Arguments.of(Arrays.asList(PEM_SINGLE_CA), Arrays.asList(PEM_SINGLE_CA_FINGERPRINT_0)),
        Arguments.of(Arrays.asList(PEM_MULTIPLE_CA),
            Arrays.asList(PEM_MULTIPLE_CA_FINGERPRINT_0, PEM_MULTIPLE_CA_FINGERPRINT_1))
    );
  }

  /**
   * Cleans the fingerprint by making it lowercase and removing the colon separator
   *
   * @param fingerprint the fingerprint scan to clean.
   * @return the cleaned fingerprint
   */
  public static String cleanFingerprint(final String fingerprint) {
    return fingerprint.toLowerCase().replaceAll(":", "");
  }

  /**
   * Creates a SHA1 Fingerprint
   *
   * @param encoded the data to fingerprint
   * @return SHA-1 HexBinary fingerprint in lowercase and with colon separator
   */
  public static String fingerprint(byte[] encoded) {
    return cleanFingerprint(DigestUtils.sha1Hex(encoded));
  }
}