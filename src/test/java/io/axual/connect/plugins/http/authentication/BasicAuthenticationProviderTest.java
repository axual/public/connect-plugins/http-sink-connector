package io.axual.connect.plugins.http.authentication;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import static io.axual.connect.plugins.http.HttpSinkConnectorConfig.ENDPOINT_CONFIG;
import static io.axual.connect.plugins.http.authentication.BasicAuthenticationProviderConfig.ENABLE_PREEMPTIVE_AUTHENTICATION_CONFIG;
import static io.axual.connect.plugins.http.authentication.BasicAuthenticationProviderConfig.PASSWORD_CONFIG;
import static io.axual.connect.plugins.http.authentication.BasicAuthenticationProviderConfig.USERNAME_CONFIG;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import io.axual.connect.plugins.http.authentication.BasicAuthenticationProvider.PreemptiveInterceptor;
import io.axual.connect.plugins.http.exceptions.ConfigurationException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.Credentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HttpContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BasicAuthenticationProviderTest {

  public static final String ENDPOINT = "https://somewhere:12345/over/there";
  public static final String USERNAME = "myUser";
  public static final String PASSWORD = "myPassword";
  public static final Boolean PREEMPTIVE = Boolean.TRUE;

  @Test
  void configure_NotPreempted() {
    BasicAuthenticationProvider provider = new BasicAuthenticationProvider();
    Map<String, Object> configs = new HashMap<>();
    configs.put(ENDPOINT_CONFIG, ENDPOINT);
    configs.put(USERNAME_CONFIG, USERNAME);
    configs.put(PASSWORD_CONFIG, PASSWORD);
    assertDoesNotThrow(() -> provider.configure(configs));
    assertNull(provider.preemptiveInterceptor);
    assertNotNull(provider.credentialsProvider);
    Credentials credentials = provider.credentialsProvider.getCredentials(AuthScope.ANY);
    assertNotNull(credentials);
    assertNotNull(credentials.getUserPrincipal());
    assertEquals(USERNAME, credentials.getUserPrincipal().getName());
    assertEquals(PASSWORD, credentials.getPassword());
  }

  @Test
  void configure_Preempted() {
    BasicAuthenticationProvider provider = new BasicAuthenticationProvider();
    Map<String, Object> configs = new HashMap<>();
    configs.put(ENDPOINT_CONFIG, ENDPOINT);
    configs.put(USERNAME_CONFIG, USERNAME);
    configs.put(PASSWORD_CONFIG, PASSWORD);
    configs.put(ENABLE_PREEMPTIVE_AUTHENTICATION_CONFIG, PREEMPTIVE);
    assertDoesNotThrow(() -> provider.configure(configs));
    assertNotNull(provider.credentialsProvider);
    assertNotNull(provider.preemptiveInterceptor);
    Credentials credentials = provider.credentialsProvider.getCredentials(AuthScope.ANY);
    assertNotNull(credentials);
    assertNotNull(credentials.getUserPrincipal());
    assertEquals(USERNAME, credentials.getUserPrincipal().getName());
    assertEquals(PASSWORD, credentials.getPassword());
  }

  @Test
  void addAuthentication_ConfiguredcPreempted()
      throws NoSuchFieldException, IllegalAccessException {
    BasicAuthenticationProvider provider = new BasicAuthenticationProvider();
    Map<String, Object> configs = new HashMap<>();
    configs.put(ENDPOINT_CONFIG, ENDPOINT);
    configs.put(USERNAME_CONFIG, USERNAME);
    configs.put(PASSWORD_CONFIG, PASSWORD);
    provider.configure(configs);

    HttpClientBuilder builder = HttpClientBuilder.create();
    HttpClientBuilder returned = provider.addAuthentication(builder);
    assertSame(builder, returned);
    // Not a nice check, but HttpClientBuilder are final, so no Mockito capturing possible
    Field fieldCredentialProvider = HttpClientBuilder.class.getDeclaredField("credentialsProvider");
    fieldCredentialProvider.setAccessible(true);
    assertEquals(provider.credentialsProvider, fieldCredentialProvider.get(returned));

    Field fieldRequestFirst = HttpClientBuilder.class.getDeclaredField("requestFirst");
    fieldRequestFirst.setAccessible(true);
    assertNull(fieldRequestFirst.get(returned));
  }

  @Test
  void addAuthentication_ConfiguredPreempted() throws NoSuchFieldException, IllegalAccessException {
    BasicAuthenticationProvider provider = new BasicAuthenticationProvider();
    Map<String, Object> configs = new HashMap<>();
    configs.put(ENDPOINT_CONFIG, ENDPOINT);
    configs.put(USERNAME_CONFIG, USERNAME);
    configs.put(PASSWORD_CONFIG, PASSWORD);
    configs.put(ENABLE_PREEMPTIVE_AUTHENTICATION_CONFIG, PREEMPTIVE);
    provider.configure(configs);

    HttpClientBuilder builder = HttpClientBuilder.create();
    HttpClientBuilder returned = provider.addAuthentication(builder);
    assertSame(builder, returned);
    // Not a nice check, but HttpClientBuilder are final, so no Mockito capturing possible
    Field fieldCredentialProvider = HttpClientBuilder.class.getDeclaredField("credentialsProvider");
    fieldCredentialProvider.setAccessible(true);
    assertEquals(provider.credentialsProvider, fieldCredentialProvider.get(returned));

    Field fieldRequestFirst = HttpClientBuilder.class.getDeclaredField("requestFirst");
    fieldRequestFirst.setAccessible(true);
    assertTrue(fieldRequestFirst.get(returned) instanceof List);
    List<?> requestFirst = (List<?>) fieldRequestFirst.get(returned);
    assertFalse(requestFirst.isEmpty());
    assertTrue(requestFirst.get(0) instanceof PreemptiveInterceptor);
  }

  @Test
  void addAuthentication_NotConfigured() {
    BasicAuthenticationProvider provider = new BasicAuthenticationProvider();
    assertThrows(ConfigurationException.class, () -> provider.addAuthentication(null));
  }

  @Test
  void testInterceptor_WithoutAuthScheme() {
    HttpContext mockContext = mock(HttpContext.class);
    AuthState mockAuthState = mock(AuthState.class);

    CredentialsProvider mockProvider = mock(CredentialsProvider.class);
    Credentials mockCredentials = mock(Credentials.class);
    doReturn(mockCredentials).when(mockProvider).getCredentials(eq(AuthScope.ANY));

    doReturn(mockAuthState).when(mockContext).getAttribute(eq(HttpClientContext.TARGET_AUTH_STATE));
    doReturn(mockProvider).when(mockContext).getAttribute(eq(HttpClientContext.CREDS_PROVIDER));

    doReturn(null).when(mockAuthState).getAuthScheme();

    PreemptiveInterceptor interceptor = new PreemptiveInterceptor();
    assertDoesNotThrow(() -> interceptor.process(null, mockContext));

    verify(mockAuthState, times(1)).update(any(BasicScheme.class), eq(mockCredentials));
  }

  @Test
  void testInterceptor_WithAuthScheme() {
    HttpContext mockContext = mock(HttpContext.class);
    AuthState mockAuthState = mock(AuthState.class);

    doReturn(mockAuthState).when(mockContext).getAttribute(eq(HttpClientContext.TARGET_AUTH_STATE));

    AuthScheme mockAuthScheme = mock(AuthScheme.class);
    doReturn(mockAuthScheme).when(mockAuthState).getAuthScheme();

    PreemptiveInterceptor interceptor = new PreemptiveInterceptor();
    assertDoesNotThrow(() -> interceptor.process(null, mockContext));

    verify(mockAuthState, times(0)).update(any(BasicScheme.class), any());
  }
}
