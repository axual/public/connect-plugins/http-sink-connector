package io.axual.connect.plugins.http.sender;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Collections;
import java.util.Map;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.ssl.SSLContexts;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.LoggerFactory;

@ExtendWith(MockitoExtension.class)
class HttpSenderConfigurationTest {

  public static final IAuthenticationProvider EXPECTED_AUTHENTICATION_PROVIDER = clientBuilder -> clientBuilder;
  public static final IMessageFormatter EXPECTED_MESSAGE_FORMATTER = (request,EXPECTED_CONTENT_TYPE) -> request;
  public static final IHeaderSelector EXPECTED_HEADER_SELECTOR = request -> request;
  public static final SSLConnectionSocketFactory EXPECTED_SSL_CONNECTION_SOCKET_FACTORY = new SSLConnectionSocketFactory(
      SSLContexts.createDefault());
  public static final String EXPECTED_ENDPOINT = "http://somewhere";
  public static final String EXPECTED_HTTP_METHOD = "POST";
  public static final ContentType EXPECTED_CONTENT_TYPE = ContentType.APPLICATION_JSON;

  public static final RequestConfig EXPECTED_REQUEST_CONFIG=RequestConfig.DEFAULT;
  public static final HttpSenderRetryStrategy EXPECTED_RETRY_STRATEGY= new HttpSenderRetryStrategy(
      Collections.singletonList(204),3,100);
 
  public static final Map<String,String> EXPECTED_STATIC_HEADERS = Collections.unmodifiableMap(Collections.emptyMap());
  
  public static final ContentLogger EXPECTED_CONTENT_LOGGER = new ContentLogger(LoggerFactory.getLogger("ContentLogger"));
  
  @Test
  void constructor() {
    assertDoesNotThrow(() -> new HttpSenderConfiguration(
        EXPECTED_AUTHENTICATION_PROVIDER, EXPECTED_MESSAGE_FORMATTER, EXPECTED_HEADER_SELECTOR,
        EXPECTED_ENDPOINT, EXPECTED_HTTP_METHOD,EXPECTED_SSL_CONNECTION_SOCKET_FACTORY,
        EXPECTED_REQUEST_CONFIG, EXPECTED_RETRY_STRATEGY, EXPECTED_CONTENT_TYPE, EXPECTED_STATIC_HEADERS,
        EXPECTED_CONTENT_LOGGER));

    assertThrows(NullPointerException.class, () -> new HttpSenderConfiguration(
        null, EXPECTED_MESSAGE_FORMATTER, EXPECTED_HEADER_SELECTOR,
        EXPECTED_ENDPOINT, EXPECTED_HTTP_METHOD,EXPECTED_SSL_CONNECTION_SOCKET_FACTORY,
        EXPECTED_REQUEST_CONFIG, EXPECTED_RETRY_STRATEGY, EXPECTED_CONTENT_TYPE, EXPECTED_STATIC_HEADERS,
        EXPECTED_CONTENT_LOGGER));

    assertThrows(NullPointerException.class, () -> new HttpSenderConfiguration(
        EXPECTED_AUTHENTICATION_PROVIDER, null, EXPECTED_HEADER_SELECTOR,
        EXPECTED_ENDPOINT, EXPECTED_HTTP_METHOD,EXPECTED_SSL_CONNECTION_SOCKET_FACTORY,
        EXPECTED_REQUEST_CONFIG, EXPECTED_RETRY_STRATEGY, EXPECTED_CONTENT_TYPE, EXPECTED_STATIC_HEADERS,
        EXPECTED_CONTENT_LOGGER));

    assertThrows(NullPointerException.class, () -> new HttpSenderConfiguration(
        EXPECTED_AUTHENTICATION_PROVIDER, EXPECTED_MESSAGE_FORMATTER, null,
        EXPECTED_ENDPOINT, EXPECTED_HTTP_METHOD,EXPECTED_SSL_CONNECTION_SOCKET_FACTORY,
        EXPECTED_REQUEST_CONFIG, EXPECTED_RETRY_STRATEGY, EXPECTED_CONTENT_TYPE, EXPECTED_STATIC_HEADERS,
        EXPECTED_CONTENT_LOGGER));

    assertThrows(NullPointerException.class, () -> new HttpSenderConfiguration(
        EXPECTED_AUTHENTICATION_PROVIDER, EXPECTED_MESSAGE_FORMATTER, EXPECTED_HEADER_SELECTOR,
        null, EXPECTED_HTTP_METHOD,EXPECTED_SSL_CONNECTION_SOCKET_FACTORY, EXPECTED_REQUEST_CONFIG,
        EXPECTED_RETRY_STRATEGY, EXPECTED_CONTENT_TYPE, EXPECTED_STATIC_HEADERS, EXPECTED_CONTENT_LOGGER));

    assertThrows(NullPointerException.class, () -> new HttpSenderConfiguration(
        EXPECTED_AUTHENTICATION_PROVIDER, EXPECTED_MESSAGE_FORMATTER, EXPECTED_HEADER_SELECTOR,
        EXPECTED_ENDPOINT, null,EXPECTED_SSL_CONNECTION_SOCKET_FACTORY, EXPECTED_REQUEST_CONFIG,
        EXPECTED_RETRY_STRATEGY, EXPECTED_CONTENT_TYPE, EXPECTED_STATIC_HEADERS, EXPECTED_CONTENT_LOGGER));

  }

  @Test
  void getAuthenticationProvider() {
    HttpSenderConfiguration configuration = new HttpSenderConfiguration(
        EXPECTED_AUTHENTICATION_PROVIDER, EXPECTED_MESSAGE_FORMATTER, EXPECTED_HEADER_SELECTOR,
        EXPECTED_ENDPOINT, EXPECTED_HTTP_METHOD,EXPECTED_SSL_CONNECTION_SOCKET_FACTORY,
        EXPECTED_REQUEST_CONFIG, EXPECTED_RETRY_STRATEGY, EXPECTED_CONTENT_TYPE, EXPECTED_STATIC_HEADERS,
        EXPECTED_CONTENT_LOGGER);
    assertEquals(EXPECTED_AUTHENTICATION_PROVIDER, configuration.getAuthenticationProvider());
  }

  @Test
  void getMessageFormatter() {
    HttpSenderConfiguration configuration = new HttpSenderConfiguration(
        EXPECTED_AUTHENTICATION_PROVIDER, EXPECTED_MESSAGE_FORMATTER, EXPECTED_HEADER_SELECTOR,
        EXPECTED_ENDPOINT, EXPECTED_HTTP_METHOD,EXPECTED_SSL_CONNECTION_SOCKET_FACTORY,
        EXPECTED_REQUEST_CONFIG, EXPECTED_RETRY_STRATEGY, EXPECTED_CONTENT_TYPE, EXPECTED_STATIC_HEADERS,
        EXPECTED_CONTENT_LOGGER);
    assertSame(EXPECTED_MESSAGE_FORMATTER, configuration.getMessageFormatter());
  }

  @Test
  void getHeaderSelector() {
    HttpSenderConfiguration configuration = new HttpSenderConfiguration(
        EXPECTED_AUTHENTICATION_PROVIDER, EXPECTED_MESSAGE_FORMATTER, EXPECTED_HEADER_SELECTOR,
        EXPECTED_ENDPOINT, EXPECTED_HTTP_METHOD,EXPECTED_SSL_CONNECTION_SOCKET_FACTORY,
        EXPECTED_REQUEST_CONFIG, EXPECTED_RETRY_STRATEGY, EXPECTED_CONTENT_TYPE, EXPECTED_STATIC_HEADERS,
        EXPECTED_CONTENT_LOGGER);
    assertSame(EXPECTED_HEADER_SELECTOR, configuration.getHeaderSelector());
  }

  @Test
  void getEndpoint() {
    HttpSenderConfiguration configuration = new HttpSenderConfiguration(
        EXPECTED_AUTHENTICATION_PROVIDER, EXPECTED_MESSAGE_FORMATTER, EXPECTED_HEADER_SELECTOR,
        EXPECTED_ENDPOINT, EXPECTED_HTTP_METHOD,EXPECTED_SSL_CONNECTION_SOCKET_FACTORY,
        EXPECTED_REQUEST_CONFIG, EXPECTED_RETRY_STRATEGY, EXPECTED_CONTENT_TYPE, EXPECTED_STATIC_HEADERS,
        EXPECTED_CONTENT_LOGGER);
    assertSame(EXPECTED_ENDPOINT, configuration.getEndpoint());
  }

  @Test
  void getHttpMethod() {
    HttpSenderConfiguration configuration = new HttpSenderConfiguration(
        EXPECTED_AUTHENTICATION_PROVIDER, EXPECTED_MESSAGE_FORMATTER, EXPECTED_HEADER_SELECTOR,
        EXPECTED_ENDPOINT, EXPECTED_HTTP_METHOD,EXPECTED_SSL_CONNECTION_SOCKET_FACTORY,
        EXPECTED_REQUEST_CONFIG, EXPECTED_RETRY_STRATEGY, EXPECTED_CONTENT_TYPE, EXPECTED_STATIC_HEADERS,
        EXPECTED_CONTENT_LOGGER);
    assertSame(EXPECTED_HTTP_METHOD, configuration.getHttpMethod());
  }

  @Test
  void getSslConnectionSocketFactory() {
    HttpSenderConfiguration configuration = new HttpSenderConfiguration(
        EXPECTED_AUTHENTICATION_PROVIDER, EXPECTED_MESSAGE_FORMATTER, EXPECTED_HEADER_SELECTOR,
        EXPECTED_ENDPOINT, EXPECTED_HTTP_METHOD,EXPECTED_SSL_CONNECTION_SOCKET_FACTORY,
        EXPECTED_REQUEST_CONFIG, EXPECTED_RETRY_STRATEGY, EXPECTED_CONTENT_TYPE, EXPECTED_STATIC_HEADERS,
        EXPECTED_CONTENT_LOGGER);
    assertSame(EXPECTED_SSL_CONNECTION_SOCKET_FACTORY, configuration.getSslConnectionSocketFactory());
  }

  @Test
  void getRequestConfig() {
    HttpSenderConfiguration configuration = new HttpSenderConfiguration(
        EXPECTED_AUTHENTICATION_PROVIDER, EXPECTED_MESSAGE_FORMATTER, EXPECTED_HEADER_SELECTOR,
        EXPECTED_ENDPOINT, EXPECTED_HTTP_METHOD,EXPECTED_SSL_CONNECTION_SOCKET_FACTORY,
        EXPECTED_REQUEST_CONFIG, EXPECTED_RETRY_STRATEGY, EXPECTED_CONTENT_TYPE, EXPECTED_STATIC_HEADERS,
        EXPECTED_CONTENT_LOGGER);
    assertSame(EXPECTED_REQUEST_CONFIG, configuration.getRequestConfig());
  }

  @Test
  void getRetryStrategy() {
    HttpSenderConfiguration configuration = new HttpSenderConfiguration(
        EXPECTED_AUTHENTICATION_PROVIDER, EXPECTED_MESSAGE_FORMATTER, EXPECTED_HEADER_SELECTOR,
        EXPECTED_ENDPOINT, EXPECTED_HTTP_METHOD,EXPECTED_SSL_CONNECTION_SOCKET_FACTORY,
        EXPECTED_REQUEST_CONFIG, EXPECTED_RETRY_STRATEGY, EXPECTED_CONTENT_TYPE, EXPECTED_STATIC_HEADERS,
        EXPECTED_CONTENT_LOGGER);
    assertSame(EXPECTED_RETRY_STRATEGY, configuration.getRetryStrategy());
  }

  @Test
  void getContentLogger() {
    HttpSenderConfiguration configuration = new HttpSenderConfiguration(
        EXPECTED_AUTHENTICATION_PROVIDER, EXPECTED_MESSAGE_FORMATTER, EXPECTED_HEADER_SELECTOR,
        EXPECTED_ENDPOINT, EXPECTED_HTTP_METHOD,EXPECTED_SSL_CONNECTION_SOCKET_FACTORY,
        EXPECTED_REQUEST_CONFIG, EXPECTED_RETRY_STRATEGY, EXPECTED_CONTENT_TYPE, EXPECTED_STATIC_HEADERS,
        EXPECTED_CONTENT_LOGGER);
    assertSame(EXPECTED_CONTENT_LOGGER, configuration.getContentLogger());
  }
}
