package io.axual.connect.plugins.http.sender;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.junit.jupiter.api.Test;

class HttpSenderRetryStrategyTest {

  public static final int VALID_MAX_RETRIES = 3;
  public static final int INVALID_MAX_RETRIES = -3;

  public static final int VALID_RETRY_WAIT = 392;
  public static final int INVALID_RETRY_WAIT = -392;
  public static final int VALID_STATUS_CODE_1 = 200;
  public static final int VALID_STATUS_CODE_2 = 203;
  public static final int VALID_STATUS_CODE_3 = 204;
  public static final int INVALID_STATUS_CODE_1 = 404;
  public static final List<Integer> VALID_STATUS_CODES = Arrays
      .asList(VALID_STATUS_CODE_1, VALID_STATUS_CODE_2, VALID_STATUS_CODE_3);
  public static final List<Integer> INVALID_STATUS_CODES = Collections.emptyList();

  @Test
  void constructor_Valid() {
    assertDoesNotThrow(
        () -> new HttpSenderRetryStrategy(VALID_STATUS_CODES, VALID_MAX_RETRIES, VALID_RETRY_WAIT));
  }

  @Test
  void constructor_Invalid() {
    assertThrows(IllegalArgumentException.class,
        () -> new HttpSenderRetryStrategy(INVALID_STATUS_CODES,
            VALID_MAX_RETRIES, VALID_RETRY_WAIT));
    assertThrows(IllegalArgumentException.class,
        () -> new HttpSenderRetryStrategy(VALID_STATUS_CODES,
            INVALID_MAX_RETRIES, VALID_RETRY_WAIT));
    assertThrows(IllegalArgumentException.class,
        () -> new HttpSenderRetryStrategy(VALID_STATUS_CODES,
            VALID_MAX_RETRIES, INVALID_RETRY_WAIT));
  }


  @Test
  void retryRequest_validCode() {
    HttpSenderRetryStrategy strategy = new HttpSenderRetryStrategy(VALID_STATUS_CODES,
        VALID_MAX_RETRIES,
        VALID_RETRY_WAIT);
    HttpResponse mockResponse = mock(HttpResponse.class);
    StatusLine mockStatusLine = mock(StatusLine.class);
    doReturn(mockStatusLine).when(mockResponse).getStatusLine();
    doReturn(VALID_STATUS_CODE_1).when(mockStatusLine).getStatusCode();

    assertFalse(strategy.retryRequest(mockResponse, 0, null));
  }

  @Test
  void retryRequest_InvalidCode() {
    HttpSenderRetryStrategy strategy = new HttpSenderRetryStrategy(VALID_STATUS_CODES,
        VALID_MAX_RETRIES,
        VALID_RETRY_WAIT);
    HttpResponse mockResponse = mock(HttpResponse.class);
    StatusLine mockStatusLine = mock(StatusLine.class);
    doReturn(mockStatusLine).when(mockResponse).getStatusLine();
    doReturn(INVALID_STATUS_CODE_1).when(mockStatusLine).getStatusCode();

    for (int retry = 0; retry <= VALID_MAX_RETRIES; retry++) {
      assertTrue(strategy.retryRequest(mockResponse, retry, null),String.format("Expected retry %d as true",retry));
    }
  }

  @Test
  void retryRequest_InvalidCodeMaxRetriesExceeded() {
    HttpSenderRetryStrategy strategy = new HttpSenderRetryStrategy(VALID_STATUS_CODES,
        VALID_MAX_RETRIES,
        VALID_RETRY_WAIT);
    HttpResponse mockResponse = mock(HttpResponse.class);
    StatusLine mockStatusLine = mock(StatusLine.class);
    doReturn(mockStatusLine).when(mockResponse).getStatusLine();
    doReturn(INVALID_STATUS_CODE_1).when(mockStatusLine).getStatusCode();

    assertFalse(strategy.retryRequest(mockResponse, VALID_MAX_RETRIES+1, null),String.format("Expected retry %d as true",VALID_MAX_RETRIES+1));
  }


  @Test
  void retryRequest_SocketTimeoutException() {
    HttpSenderRetryStrategy strategy = new HttpSenderRetryStrategy(VALID_STATUS_CODES,
        VALID_MAX_RETRIES,
        VALID_RETRY_WAIT);
    IOException exception = new SocketTimeoutException();

    for (int retry = 0; retry <= VALID_MAX_RETRIES; retry++) {
      assertTrue(strategy.retryRequest(exception, retry, null),String.format("Expected retry %d as true",retry));
    }
  }

  @Test
  void retryRequest_SocketTimeoutExceptionExceeded() {
    HttpSenderRetryStrategy strategy = new HttpSenderRetryStrategy(VALID_STATUS_CODES,
        VALID_MAX_RETRIES,
        VALID_RETRY_WAIT);
    IOException exception = new SocketTimeoutException();

    assertFalse(strategy.retryRequest(exception, VALID_MAX_RETRIES+1, null),String.format("Expected retry %d as true",VALID_MAX_RETRIES+1));
  }


  @Test
  void getRetryInterval() {
    HttpSenderRetryStrategy strategy = new HttpSenderRetryStrategy(VALID_STATUS_CODES,
        VALID_MAX_RETRIES,
        VALID_RETRY_WAIT);
    assertEquals(VALID_RETRY_WAIT, strategy.getRetryInterval());
  }
}