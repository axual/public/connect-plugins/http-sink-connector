package io.axual.connect.plugins.http.headerselection;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static io.axual.connect.plugins.http.headerselection.BasicHeaderSelectorConfig.HEADER_ALIAS_CONFIG;
import static io.axual.connect.plugins.http.headerselection.BasicHeaderSelectorConfig.HEADER_KEY_CONFIG_FORMAT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigException;
import org.junit.jupiter.api.Test;

class BasicHeaderSelectorConfigTest {

  public static final String ALIAS1 = "first";
  public static final String ALIAS2 = "second";
  public static final String ALIAS3 = "third";

  public static final String KEY1 = "first";
  public static final String KEY2 = "second";
  public static final String KEY3 = "third";

  public static final String ALIAS_CONFIG_VALUE = String.join(",", ALIAS1, ALIAS2, ALIAS3);
  public static final String ALIAS_WITH_EMPTY_CONFIG_VALUE = String
      .join(",", ALIAS1, "", ALIAS2, ALIAS3);

  public static final String EXPECTED_CONFIG_ALIAS1 = String
      .format(HEADER_KEY_CONFIG_FORMAT, ALIAS1);
  public static final String EXPECTED_CONFIG_ALIAS2 = String
      .format(HEADER_KEY_CONFIG_FORMAT, ALIAS2);
  public static final String EXPECTED_CONFIG_ALIAS3 = String
      .format(HEADER_KEY_CONFIG_FORMAT, ALIAS3);

  Map<String, Object> createConfig() {
    Map<String, Object> config = new HashMap<>();
    config.put(HEADER_ALIAS_CONFIG, ALIAS_CONFIG_VALUE);

    config.put(EXPECTED_CONFIG_ALIAS1, KEY1);
    config.put(EXPECTED_CONFIG_ALIAS2, KEY2);
    config.put(EXPECTED_CONFIG_ALIAS3, KEY3);

    return config;
  }

  @Test
  void enrichConfig_Valid() {
    ConfigDef enrichtedConfig = BasicHeaderSelectorConfig.enrichConfig(createConfig());
    assertTrue(enrichtedConfig.configKeys().keySet().contains(HEADER_ALIAS_CONFIG));
    assertTrue(enrichtedConfig.configKeys().keySet().contains(EXPECTED_CONFIG_ALIAS1));
    assertTrue(enrichtedConfig.configKeys().keySet().contains(EXPECTED_CONFIG_ALIAS2));
    assertTrue(enrichtedConfig.configKeys().keySet().contains(EXPECTED_CONFIG_ALIAS3));
  }

  @Test
  void enrichConfig_NoAlias() {
    Map<String, Object> config = createConfig();
    config.remove(HEADER_ALIAS_CONFIG);
    ConfigDef enrichtedConfig = BasicHeaderSelectorConfig.enrichConfig(config);
    assertTrue(enrichtedConfig.configKeys().keySet().contains(HEADER_ALIAS_CONFIG));
    assertFalse(enrichtedConfig.configKeys().keySet().contains(EXPECTED_CONFIG_ALIAS1));
    assertFalse(enrichtedConfig.configKeys().keySet().contains(EXPECTED_CONFIG_ALIAS2));
    assertFalse(enrichtedConfig.configKeys().keySet().contains(EXPECTED_CONFIG_ALIAS3));
  }

  @Test
  void enrichConfig_EmptyAlias() {
    Map<String, Object> config = createConfig();
    config.put(HEADER_ALIAS_CONFIG, ALIAS_WITH_EMPTY_CONFIG_VALUE);
    assertThrows(ConfigException.class,
        () -> BasicHeaderSelectorConfig.enrichConfig(config));
  }

  @Test
  void enrichConfig_Invalid() {
    Map<String, Object> config = createConfig();
    List<Object> invalidAliasList = Arrays.asList(ALIAS1, new Integer(1), ALIAS3);
    config.put(HEADER_ALIAS_CONFIG, invalidAliasList);
    assertThrows(ConfigException.class,
        () -> BasicHeaderSelectorConfig.enrichConfig(config));
  }

  @Test
  void getHeaderAliases() {
    Map<String, Object> config = createConfig();
    BasicHeaderSelectorConfig selectorConfig = new BasicHeaderSelectorConfig(config);
    List<String> aliases = selectorConfig.getHeaderAliases();
    assertTrue(aliases.contains(ALIAS1),"Expected list to contain "+ALIAS1);
    assertTrue(aliases.contains(ALIAS2),"Expected list to contain "+ALIAS2);
    assertTrue(aliases.contains(ALIAS3),"Expected list to contain "+ALIAS3);
  }

  @Test
  void getHeaderKey() {
    Map<String, Object> config = createConfig();
    BasicHeaderSelectorConfig selectorConfig = new BasicHeaderSelectorConfig(config);
    assertEquals(KEY1,selectorConfig.getHeaderKey(ALIAS1));
    assertEquals(KEY2,selectorConfig.getHeaderKey(ALIAS2));
    assertEquals(KEY3,selectorConfig.getHeaderKey(ALIAS3));
  }
}