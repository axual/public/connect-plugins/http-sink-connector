package io.axual.connect.plugins.http.formatter;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.NumericNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.databind.node.ValueNode;
import io.axual.connect.plugins.http.exceptions.MessageFormattingException;
import io.axual.connect.plugins.http.sender.HttpSenderRequest;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.kafka.common.record.TimestampType;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.data.SchemaBuilder;
import org.apache.kafka.connect.data.Struct;
import org.apache.kafka.connect.header.ConnectHeaders;
import org.apache.kafka.connect.header.Header;
import org.apache.kafka.connect.sink.SinkRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class JsonEnvelopeMessageFormatterTest {

  public static final String FIELD_INT8_NAME = "f_r_int8";
  public static final byte FIELD_INT8_VALUE = 21;
  public static final String FIELD_INT16_NAME = "f_r_int16";
  public static final short FIELD_INT16_VALUE = 432;
  public static final String FIELD_INT32_NAME = "f_r_int32";
  public static final int FIELD_INT32_VALUE = 6543;
  public static final String FIELD_INT64_NAME = "f_r_int64";
  public static final long FIELD_INT64_VALUE = 654321L;
  public static final String FIELD_FLOAT32_NAME = "f_r_float32";
  public static final float FIELD_FLOAT32_VALUE = 123.45f;
  public static final String FIELD_FLOAT64_NAME = "f_r_float64";
  public static final double FIELD_FLOAT64_VALUE = 5678.901234;
  public static final String FIELD_BOOLEAN_NAME = "f_r_boolean";
  public static final Boolean FIELD_BOOLEAN_VALUE = Boolean.TRUE;
  public static final String FIELD_BYTES_NAME = "f_r_bytes";
  public static final byte[] FIELD_BYTES_VALUE = new byte[]{0b1010101, 0b01010101, 0b1010101,
      0b01010101, 0b1010101, 0b01010101};
  public static final String FIELD_STRING_NAME = "f_r_string";
  public static final String FIELD_STRING_VALUE = "Hello world";

  public static final String FIELD_OPTIONAL_INT8_NAME = "f_o_int8";
  public static final byte FIELD_OPTIONAL_INT8_VALUE = 12;
  public static final String FIELD_OPTIONAL_INT16_NAME = "f_o_int16";
  public static final short FIELD_OPTIONAL_INT16_VALUE = 234;
  public static final String FIELD_OPTIONAL_INT32_NAME = "f_o_int32";
  public static final int FIELD_OPTIONAL_INT32_VALUE = 3456;
  public static final String FIELD_OPTIONAL_INT64_NAME = "f_o_int64";
  public static final long FIELD_OPTIONAL_INT64_VALUE = 123456L;
  public static final String FIELD_OPTIONAL_FLOAT32_NAME = "f_o_float32";
  public static final float FIELD_OPTIONAL_FLOAT32_VALUE = 5432.1f;
  public static final String FIELD_OPTIONAL_FLOAT64_NAME = "f_o_float64";
  public static final double FIELD_OPTIONAL_FLOAT64_VALUE = 901234.5678;
  public static final String FIELD_OPTIONAL_BOOLEAN_NAME = "f_o_boolean";
  public static final Boolean FIELD_OPTIONAL_BOOLEAN_VALUE = Boolean.TRUE;
  public static final String FIELD_OPTIONAL_BYTES_NAME = "f_o_bytes";
  public static final byte[] FIELD_OPTIONAL_BYTES_VALUE = new byte[]{0b1111111, 0b1000101,
      0b1111111, 0b1000101, 0b1111111, 0b1000101, 0b1111111, 0b1000101};
  public static final String FIELD_OPTIONAL_STRING_NAME = "f_o_string";
  public static final String FIELD_OPTIONAL_STRING_VALUE = "Hello moon";


  public static final Schema STRUCT_SCHEMA = SchemaBuilder.struct()
      .field(FIELD_INT8_NAME, Schema.INT8_SCHEMA)
      .field(FIELD_INT16_NAME, Schema.INT16_SCHEMA)
      .field(FIELD_INT32_NAME, Schema.INT32_SCHEMA)
      .field(FIELD_INT64_NAME, Schema.INT64_SCHEMA)
      .field(FIELD_FLOAT32_NAME, Schema.FLOAT32_SCHEMA)
      .field(FIELD_FLOAT64_NAME, Schema.FLOAT64_SCHEMA)
      .field(FIELD_BOOLEAN_NAME, Schema.BOOLEAN_SCHEMA)
      .field(FIELD_BYTES_NAME, Schema.BYTES_SCHEMA)
      .field(FIELD_STRING_NAME, Schema.STRING_SCHEMA)
      .field(FIELD_OPTIONAL_INT8_NAME, Schema.OPTIONAL_INT8_SCHEMA)
      .field(FIELD_OPTIONAL_INT16_NAME, Schema.OPTIONAL_INT16_SCHEMA)
      .field(FIELD_OPTIONAL_INT32_NAME, Schema.OPTIONAL_INT32_SCHEMA)
      .field(FIELD_OPTIONAL_INT64_NAME, Schema.OPTIONAL_INT64_SCHEMA)
      .field(FIELD_OPTIONAL_FLOAT32_NAME, Schema.OPTIONAL_FLOAT32_SCHEMA)
      .field(FIELD_OPTIONAL_FLOAT64_NAME, Schema.OPTIONAL_FLOAT64_SCHEMA)
      .field(FIELD_OPTIONAL_BOOLEAN_NAME, Schema.OPTIONAL_BOOLEAN_SCHEMA)
      .field(FIELD_OPTIONAL_BYTES_NAME, Schema.OPTIONAL_BYTES_SCHEMA)
      .field(FIELD_OPTIONAL_STRING_NAME, Schema.OPTIONAL_STRING_SCHEMA)
      .build();

  public static final Schema MAP_STRINGKEY_SCHEMA = SchemaBuilder
      .map(Schema.STRING_SCHEMA, STRUCT_SCHEMA);
  public static final Schema MAP_INTEGERKEY_SCHEMA = SchemaBuilder
      .map(Schema.INT32_SCHEMA, STRUCT_SCHEMA);

  public static final Schema ARRAY_STRUCT_SCHEMA = SchemaBuilder
      .array(STRUCT_SCHEMA);

  Struct createStructObject(
      boolean setOptionals) {
    Struct struct = new Struct(STRUCT_SCHEMA);
    struct.put(FIELD_INT8_NAME, FIELD_INT8_VALUE);
    struct.put(FIELD_INT16_NAME, FIELD_INT16_VALUE);
    struct.put(FIELD_INT32_NAME, FIELD_INT32_VALUE);
    struct.put(FIELD_INT64_NAME, FIELD_INT64_VALUE);
    struct.put(FIELD_FLOAT32_NAME, FIELD_FLOAT32_VALUE);
    struct.put(FIELD_FLOAT64_NAME, FIELD_FLOAT64_VALUE);
    struct.put(FIELD_BOOLEAN_NAME, FIELD_BOOLEAN_VALUE);
    struct.put(FIELD_BYTES_NAME, FIELD_BYTES_VALUE);
    struct.put(FIELD_STRING_NAME, FIELD_STRING_VALUE);

    if (setOptionals) {
      struct.put(FIELD_OPTIONAL_INT8_NAME, FIELD_OPTIONAL_INT8_VALUE);
      struct.put(FIELD_OPTIONAL_INT16_NAME, FIELD_OPTIONAL_INT16_VALUE);
      struct.put(FIELD_OPTIONAL_INT32_NAME, FIELD_OPTIONAL_INT32_VALUE);
      struct.put(FIELD_OPTIONAL_INT64_NAME, FIELD_OPTIONAL_INT64_VALUE);
      struct.put(FIELD_OPTIONAL_FLOAT32_NAME, FIELD_OPTIONAL_FLOAT32_VALUE);
      struct.put(FIELD_OPTIONAL_FLOAT64_NAME, FIELD_OPTIONAL_FLOAT64_VALUE);
      struct.put(FIELD_OPTIONAL_BOOLEAN_NAME, FIELD_OPTIONAL_BOOLEAN_VALUE);
      struct.put(FIELD_OPTIONAL_BYTES_NAME, FIELD_OPTIONAL_BYTES_VALUE);
      struct.put(FIELD_OPTIONAL_STRING_NAME, FIELD_OPTIONAL_STRING_VALUE);
    }
    return struct;
  }


  List<Struct> createArrayOfStructObject(boolean setOptionals) {
    return IntStream.range(0, 5).mapToObj(i -> createStructObject(setOptionals))
        .collect(Collectors.toList());
  }

  Map<Integer, Struct> createIntegerStructMap(boolean setOptionals) {
    return IntStream.range(0, 5).boxed()
        .collect(Collectors.toMap(i -> i, i -> createStructObject(setOptionals)));
  }

  Map<String, Struct> createStringStructMap(boolean setOptionals) {
    return IntStream.range(0, 5).boxed()
        .collect(Collectors.toMap(i -> i.toString(), i -> createStructObject(setOptionals)));
  }

  @ParameterizedTest(name = "Enabling optional fields {argumentsWithNames}")
  @ValueSource(booleans = {true, false})
  void formatMessage_Struct(boolean optionalSet) throws IOException {
    Struct struct = createStructObject(optionalSet);
    JsonEnvelopeMessageFormatter formatter = new JsonEnvelopeMessageFormatter();
    final SinkRecord record = new SinkRecord(TOPIC, PARTITION, struct.schema(), struct,
        struct.schema(),
        struct, KAFKA_OFFSET, TIMESTAMP, TIMESTAMP_TYPE, HEADERS);
    HttpSenderRequest senderRequest = new HttpSenderRequest(record);
    HttpSenderRequest result = formatter.formatMessage(senderRequest,EXPECTED_CONTENT_TYPE);

    assertSame(senderRequest, result);
    assertNotNull(result.getSelectedHeaders());
    assertTrue(result.getSelectedHeaders().isEmpty());

    JsonNode rootNode = verifyAndExtractJsonFromEntity(result.getEntity());
    assertTrue(rootNode instanceof ObjectNode);

    verifyStructEntity(EXPECTED_FIELD_NAME_KEY, struct, (ObjectNode) rootNode, optionalSet);
    verifyStructEntity(EXPECTED_FIELD_NAME_VALUE, struct, (ObjectNode) rootNode, optionalSet);
  }

  @ParameterizedTest(name = "Enabling optional fields {argumentsWithNames}")
  @ValueSource(booleans = {true, false})
  void formatMessage_StructArray(boolean optionalSet) throws IOException {
    List<Struct> array = createArrayOfStructObject(optionalSet);
    JsonEnvelopeMessageFormatter formatter = new JsonEnvelopeMessageFormatter();
    final SinkRecord record = new SinkRecord(TOPIC, PARTITION, ARRAY_STRUCT_SCHEMA, array,
        ARRAY_STRUCT_SCHEMA, array, KAFKA_OFFSET, TIMESTAMP, TIMESTAMP_TYPE, HEADERS);
    HttpSenderRequest senderRequest = new HttpSenderRequest(record);
    HttpSenderRequest result = formatter.formatMessage(senderRequest,EXPECTED_CONTENT_TYPE);

    JsonNode rootNode = verifyAndExtractJsonFromEntity(result.getEntity());
    assertTrue(rootNode instanceof ObjectNode);
    JsonNode keyNode = rootNode.get(EXPECTED_FIELD_NAME_KEY);
    assertTrue(keyNode instanceof ArrayNode);
    JsonNode valueNode = rootNode.get(EXPECTED_FIELD_NAME_VALUE);
    assertTrue(valueNode instanceof ArrayNode);

    assertEquals(array.size(), keyNode.size());
    assertEquals(array.size(), valueNode.size());

    for (int i = 0; i < array.size(); i++) {
      Struct struct = array.get(i);
      JsonNode keyElementNode = keyNode.get(i);
      JsonNode valueElementNode = valueNode.get(i);

      verifyStructNode(struct, keyElementNode, optionalSet);
      verifyStructNode(struct, valueElementNode, optionalSet);
    }
  }

  @ParameterizedTest(name = "Enabling optional fields {argumentsWithNames}")
  @ValueSource(booleans = {true, false})
  void formatMessage_StringStructMap(boolean optionalSet) throws IOException {
    Map<String, Struct> map = createStringStructMap(optionalSet);
    JsonEnvelopeMessageFormatter formatter = new JsonEnvelopeMessageFormatter();
    final SinkRecord record = new SinkRecord(TOPIC, PARTITION, null, null,
        MAP_STRINGKEY_SCHEMA, map, KAFKA_OFFSET, TIMESTAMP, TIMESTAMP_TYPE, HEADERS);
    HttpSenderRequest senderRequest = new HttpSenderRequest(record);
    HttpSenderRequest result = formatter.formatMessage(senderRequest,EXPECTED_CONTENT_TYPE);

    JsonNode rootNode = verifyAndExtractJsonFromEntity(result.getEntity());
    assertTrue(rootNode instanceof ObjectNode);
    JsonNode nullKeyNode = rootNode.get(EXPECTED_FIELD_NAME_KEY);
    assertTrue(nullKeyNode instanceof NullNode);

    JsonNode mapNode = rootNode.get(EXPECTED_FIELD_NAME_VALUE);
    assertTrue(mapNode instanceof ObjectNode);

    assertEquals(map.size(), mapNode.size());

    for (Map.Entry<String, Struct> entry : map.entrySet()) {
      Struct struct = entry.getValue();
      JsonNode elementNode = mapNode.get(entry.getKey());

      verifyStructNode(struct, elementNode, optionalSet);
    }
  }

  @ParameterizedTest(name = "Enabling optional fields {argumentsWithNames}")
  @ValueSource(booleans = {true, false})
  void formatMessage_IntegerStructMap(boolean optionalSet) throws IOException {
    Map<Integer, Struct> map = createIntegerStructMap(optionalSet);
    JsonEnvelopeMessageFormatter formatter = new JsonEnvelopeMessageFormatter();
    final SinkRecord record = new SinkRecord(TOPIC, PARTITION, null, null,
        MAP_INTEGERKEY_SCHEMA, map, KAFKA_OFFSET, TIMESTAMP, TIMESTAMP_TYPE, HEADERS);
    HttpSenderRequest senderRequest = new HttpSenderRequest(record);
    HttpSenderRequest result = formatter.formatMessage(senderRequest,EXPECTED_CONTENT_TYPE);

    JsonNode rootNode = verifyAndExtractJsonFromEntity(result.getEntity());
    assertTrue(rootNode instanceof ObjectNode);
    JsonNode nullKeyNode = rootNode.get(EXPECTED_FIELD_NAME_KEY);
    assertTrue(nullKeyNode instanceof NullNode);

    JsonNode mapNode = rootNode.get(EXPECTED_FIELD_NAME_VALUE);
    assertTrue(mapNode instanceof ArrayNode);

    assertEquals(map.size(), mapNode.size());

    for (Map.Entry<Integer, Struct> entry : map.entrySet()) {
      Struct struct = entry.getValue();
      JsonNode elementNode = mapNode.get(entry.getKey());
      assertTrue(elementNode instanceof ObjectNode);

      JsonNode keyNode = elementNode.get(EXPECTED_FIELD_NAME_KEY);
      assertTrue(keyNode instanceof NumericNode);
      assertEquals(entry.getKey(), keyNode.intValue());

      JsonNode valueNode = elementNode.get(EXPECTED_FIELD_NAME_VALUE);
      verifyStructNode(struct, valueNode, optionalSet);
    }
  }

  @ParameterizedTest
  @MethodSource
  void formatMessage_NoSchema(Object input, Class<?> nodeClazz, Object expected)
      throws IOException {
    JsonEnvelopeMessageFormatter formatter = new JsonEnvelopeMessageFormatter();
    final SinkRecord record = new SinkRecord(TOPIC, PARTITION, null, null,
        null, input, KAFKA_OFFSET, TIMESTAMP, TIMESTAMP_TYPE, HEADERS);
    HttpSenderRequest senderRequest = new HttpSenderRequest(record);
    HttpSenderRequest result = formatter.formatMessage(senderRequest,EXPECTED_CONTENT_TYPE);

    JsonNode rootNode = verifyAndExtractJsonFromEntity(result.getEntity());
    assertTrue(rootNode instanceof ObjectNode);
    JsonNode nullKeyNode = rootNode.get(EXPECTED_FIELD_NAME_KEY);
    assertTrue(nullKeyNode instanceof NullNode);

    JsonNode valueNode = rootNode.get(EXPECTED_FIELD_NAME_VALUE);
    assertTrue(nodeClazz.isInstance(valueNode));
    assertEquals(expected, valueNode);
  }

  private static Stream<Arguments> formatMessage_NoSchema() {
    final byte[] bytes = {0b10101, 0b01010101, 0b01010100, 0b01000100};
    final String bytesBase64 = Base64.encodeBase64String(bytes);
    final Float floatValue = new Float("10.10");
    final Double floatValueAsDouble = new Double("10.10");
    final Double doubleValue = new Double("11.11");
    final Date otherObject = new Date();

    return Stream.of(
        Arguments.of("SomeString", TextNode.class, JsonNodeFactory.instance.textNode("SomeString")),
        Arguments.of(bytes, TextNode.class, JsonNodeFactory.instance.textNode(bytesBase64)),
        Arguments.of(ByteBuffer.wrap(bytes), TextNode.class,
            JsonNodeFactory.instance.textNode(bytesBase64)),
        Arguments.of(floatValue, NumericNode.class,
            JsonNodeFactory.instance.numberNode(floatValueAsDouble)),
        Arguments
            .of(doubleValue, NumericNode.class, JsonNodeFactory.instance.numberNode(doubleValue)),
        Arguments.of(new Byte("12"), NumericNode.class, JsonNodeFactory.instance.numberNode(12)),
        Arguments.of(new Short("13"), NumericNode.class, JsonNodeFactory.instance.numberNode(13)),
        Arguments.of(new Integer("14"), NumericNode.class, JsonNodeFactory.instance.numberNode(14)),
        Arguments.of(new Long("15"), NumericNode.class, JsonNodeFactory.instance.numberNode(15)),
        Arguments.of(otherObject, TextNode.class,
            JsonNodeFactory.instance.textNode(otherObject.toString()))
    );
  }

  JsonNode verifyAndExtractJsonFromEntity(HttpEntity entity) throws IOException {
    assertNotNull(entity);
    assertNotNull(entity.getContentType());
    assertEquals(ContentType.APPLICATION_JSON.toString(), entity.getContentType().getValue());

    ObjectMapper mapper = new ObjectMapper();
    return mapper.readTree(entity.getContent());
  }

  void verifyStructEntity(String fieldName, Struct struct, ObjectNode root, boolean optionalSet)
      throws IOException {

    assertTrue(root.has(fieldName));
    verifyStructNode(struct, root.findValue(fieldName), optionalSet);
  }

  void verifyStructNode(Struct struct, JsonNode node, boolean optionalsSet) throws IOException {
    {
      JsonNode fieldNode = node.get(FIELD_INT8_NAME);
      Byte fieldStruct = struct.getInt8(FIELD_INT8_NAME);
      assertTrue(fieldNode instanceof ValueNode);
      assertEquals((long) fieldStruct, fieldNode.asLong());
    }
    {
      JsonNode fieldNode = node.get(FIELD_INT16_NAME);
      Short fieldStruct = struct.getInt16(FIELD_INT16_NAME);
      assertTrue(fieldNode instanceof ValueNode);
      assertEquals((long) fieldStruct, fieldNode.asLong());
    }
    {
      JsonNode fieldNode = node.get(FIELD_INT32_NAME);
      Integer fieldStruct = struct.getInt32(FIELD_INT32_NAME);
      assertTrue(fieldNode instanceof ValueNode);
      assertEquals((long) fieldStruct, fieldNode.asLong());
    }
    {
      JsonNode fieldNode = node.get(FIELD_INT64_NAME);
      Long fieldStruct = struct.getInt64(FIELD_INT64_NAME);
      assertTrue(fieldNode instanceof ValueNode);
      assertEquals((long) fieldStruct, fieldNode.asLong());
    }
    {
      JsonNode fieldNode = node.get(FIELD_BOOLEAN_NAME);
      Boolean fieldStruct = struct.getBoolean(FIELD_BOOLEAN_NAME);
      assertTrue(fieldNode instanceof ValueNode);
      assertEquals(fieldStruct, fieldNode.asBoolean());
    }
    {
      JsonNode fieldNode = node.get(FIELD_BYTES_NAME);
      byte[] fieldStruct = struct.getBytes(FIELD_BYTES_NAME);
      assertTrue(fieldNode instanceof ValueNode);
      assertArrayEquals(fieldStruct, fieldNode.binaryValue());
    }
    {
      JsonNode fieldNode = node.get(FIELD_STRING_NAME);
      String fieldStruct = struct.getString(FIELD_STRING_NAME);
      assertTrue(fieldNode instanceof ValueNode);
      assertEquals(fieldStruct, fieldNode.asText());
    }
    {
      JsonNode fieldNode = node.get(FIELD_FLOAT32_NAME);
      Float fieldStruct = struct.getFloat32(FIELD_FLOAT32_NAME);
      assertTrue(fieldNode instanceof ValueNode);
      assertEquals(fieldStruct, fieldNode.floatValue());
    }
    {
      JsonNode fieldNode = node.get(FIELD_FLOAT64_NAME);
      Double fieldStruct = struct.getFloat64(FIELD_FLOAT64_NAME);
      assertTrue(fieldNode instanceof ValueNode);
      assertEquals(fieldStruct, fieldNode.doubleValue());
    }

    if (optionalsSet) {
      {
        JsonNode fieldNode = node.get(FIELD_OPTIONAL_INT8_NAME);
        Byte fieldStruct = struct.getInt8(FIELD_OPTIONAL_INT8_NAME);
        assertTrue(fieldNode instanceof ValueNode);
        assertEquals((long) fieldStruct, fieldNode.asLong());
      }
      {
        JsonNode fieldNode = node.get(FIELD_OPTIONAL_INT16_NAME);
        Short fieldStruct = struct.getInt16(FIELD_OPTIONAL_INT16_NAME);
        assertTrue(fieldNode instanceof ValueNode);
        assertEquals((long) fieldStruct, fieldNode.asLong());
      }
      {
        JsonNode fieldNode = node.get(FIELD_OPTIONAL_INT32_NAME);
        Integer fieldStruct = struct.getInt32(FIELD_OPTIONAL_INT32_NAME);
        assertTrue(fieldNode instanceof ValueNode);
        assertEquals((long) fieldStruct, fieldNode.asLong());
      }
      {
        JsonNode fieldNode = node.get(FIELD_OPTIONAL_INT64_NAME);
        Long fieldStruct = struct.getInt64(FIELD_OPTIONAL_INT64_NAME);
        assertTrue(fieldNode instanceof ValueNode);
        assertEquals((long) fieldStruct, fieldNode.asLong());
      }
      {
        JsonNode fieldNode = node.get(FIELD_OPTIONAL_BOOLEAN_NAME);
        Boolean fieldStruct = struct.getBoolean(FIELD_OPTIONAL_BOOLEAN_NAME);
        assertTrue(fieldNode instanceof ValueNode);
        assertEquals(fieldStruct, fieldNode.asBoolean());
      }
      {
        JsonNode fieldNode = node.get(FIELD_BYTES_NAME);
        byte[] fieldStruct = struct.getBytes(FIELD_BYTES_NAME);
        assertTrue(fieldNode instanceof ValueNode);
        assertArrayEquals(fieldStruct, fieldNode.binaryValue());
      }
      {
        JsonNode fieldNode = node.get(FIELD_OPTIONAL_STRING_NAME);
        String fieldStruct = struct.getString(FIELD_OPTIONAL_STRING_NAME);
        assertTrue(fieldNode instanceof ValueNode);
        assertEquals(fieldStruct, fieldNode.asText());
      }
      {
        JsonNode fieldNode = node.get(FIELD_OPTIONAL_FLOAT32_NAME);
        Float fieldStruct = struct.getFloat32(FIELD_OPTIONAL_FLOAT32_NAME);
        assertTrue(fieldNode instanceof ValueNode);
        assertEquals(fieldStruct, fieldNode.floatValue());
      }
      {
        JsonNode fieldNode = node.get(FIELD_OPTIONAL_FLOAT64_NAME);
        Double fieldStruct = struct.getFloat64(FIELD_OPTIONAL_FLOAT64_NAME);
        assertTrue(fieldNode instanceof ValueNode);
        assertEquals(fieldStruct, fieldNode.doubleValue());
      }
    }
  }

  public static final String TOPIC = "test-topic";
  public static final int PARTITION = 0;
  public static final Schema KEY_SCHEMA = Schema.OPTIONAL_STRING_SCHEMA;
  public static final Object STRING_KEY = "Hello";
  public static final Schema VALUE_SCHEMA = Schema.OPTIONAL_STRING_SCHEMA;
  public static final Object STRING_VALUE = "World";
  public static final long KAFKA_OFFSET = 1L;
  public static final Long TIMESTAMP = 123L;
  public static final TimestampType TIMESTAMP_TYPE = TimestampType.CREATE_TIME;
  public static final Iterable<Header> HEADERS = new ConnectHeaders();

  public static final String EXPECTED_FIELD_NAME_TOPIC = "topic";
  public static final String EXPECTED_FIELD_NAME_KEY = "key";
  public static final String EXPECTED_FIELD_NAME_VALUE = "value";

  public static final ContentType EXPECTED_CONTENT_TYPE=ContentType.APPLICATION_JSON;


  @Test
  void createJsonMessage_Exception() throws IOException {
    final SinkRecord record = new SinkRecord(TOPIC, PARTITION, KEY_SCHEMA, STRING_KEY,
        VALUE_SCHEMA,
        STRING_VALUE, KAFKA_OFFSET, TIMESTAMP, TIMESTAMP_TYPE, HEADERS);
    ObjectMapper mapper = mock(ObjectMapper.class);
    JsonProcessingException jsonProcessingException = mock(JsonProcessingException.class);
    doThrow(jsonProcessingException).when(mapper).writeValueAsString(any());

    JsonEnvelopeMessageFormatter formatter = new JsonEnvelopeMessageFormatter(mapper,
        JsonNodeFactory.instance);
    assertThrows(MessageFormattingException.class, () -> formatter.createJsonMessage(record,EXPECTED_CONTENT_TYPE));
  }

  @Test
  void createJsonMessage_Correct() throws IOException {
    final SinkRecord record = new SinkRecord(TOPIC, PARTITION, KEY_SCHEMA, STRING_KEY,
        VALUE_SCHEMA,
        STRING_VALUE, KAFKA_OFFSET, TIMESTAMP, TIMESTAMP_TYPE, HEADERS);
    JsonEnvelopeMessageFormatter formatter = new JsonEnvelopeMessageFormatter();
    StringEntity entity = formatter.createJsonMessage(record,EXPECTED_CONTENT_TYPE);

    assertNotNull(entity);
    assertNotNull(entity.getContentType());
    assertEquals(ContentType.APPLICATION_JSON.toString(), entity.getContentType().getValue());

    ObjectMapper mapper = new ObjectMapper();
    JsonNode tree = mapper.readTree(entity.getContent());
    assertTrue(tree instanceof ObjectNode);
    ObjectNode root = ((ObjectNode) tree);

    assertTrue(root.has(EXPECTED_FIELD_NAME_TOPIC));
    assertTrue(root.findValue(EXPECTED_FIELD_NAME_TOPIC) instanceof TextNode);
    assertEquals(TOPIC, root.findValue(EXPECTED_FIELD_NAME_TOPIC).textValue());

    assertTrue(root.has(EXPECTED_FIELD_NAME_KEY));
    assertTrue(root.findValue(EXPECTED_FIELD_NAME_KEY) instanceof TextNode);
    assertEquals(STRING_KEY, root.findValue(EXPECTED_FIELD_NAME_KEY).textValue());

    assertTrue(root.has(EXPECTED_FIELD_NAME_VALUE));
    assertTrue(root.findValue(EXPECTED_FIELD_NAME_VALUE) instanceof TextNode);
    assertEquals(STRING_VALUE, root.findValue(EXPECTED_FIELD_NAME_VALUE).textValue());
  }

  @Test
  void createNode_NullSchema_NullValue() {
    final String INPUT = null;
    JsonEnvelopeMessageFormatter formatter = new JsonEnvelopeMessageFormatter();
    JsonNode node = formatter.createNode(null, INPUT);
    assertTrue(node instanceof NullNode);
  }

  @Test
  void createNode_NullSchema_StringValue() {
    final String INPUT = "someText";
    JsonEnvelopeMessageFormatter formatter = new JsonEnvelopeMessageFormatter();
    JsonNode node = formatter.createNode(null, INPUT);
    assertTrue(node instanceof TextNode);
    assertEquals(INPUT, node.textValue());
  }

  @Test
  void createNode_StringSchema_StringValue() {
    final String INPUT = "someText";
    JsonEnvelopeMessageFormatter formatter = new JsonEnvelopeMessageFormatter();
    JsonNode node = formatter.createNode(Schema.STRING_SCHEMA, INPUT);
    assertTrue(node instanceof TextNode);
    assertEquals(INPUT, node.textValue());
  }

  @Test
  void createNode_OptionalStringSchema_StringValue() {
    final String INPUT = "someText";
    JsonEnvelopeMessageFormatter formatter = new JsonEnvelopeMessageFormatter();
    JsonNode node = formatter.createNode(Schema.OPTIONAL_STRING_SCHEMA, INPUT);
    assertTrue(node instanceof TextNode);
    assertEquals(INPUT, node.textValue());
  }

  @Test
  void createNode_OtherSchema_StringValue() {
    final String INPUT = "someText";
    JsonEnvelopeMessageFormatter formatter = new JsonEnvelopeMessageFormatter();
    assertThrows(MessageFormattingException.class,
        () -> formatter.createNode(Schema.BYTES_SCHEMA, INPUT));
  }
}
