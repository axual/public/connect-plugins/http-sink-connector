package io.axual.connect.plugins.http;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 - 2023 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.config.ConfigException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ContentTypeValidatorTest {

    ContentTypeValidator validator = new ContentTypeValidator();

    public static Stream<Arguments> ensureValid_Correct() {
        return Stream.of(
                Arguments.of("application/json"),
                Arguments.of("application/soap+xml"),
                Arguments.of("application/gofish"),
                Arguments.of("someother/mimetype"),
                Arguments.of("application/json; charset=utf-8"),
                Arguments.of("application/json; charset=iso-8859-1")
        );
    }

    @ParameterizedTest
    @MethodSource
    void ensureValid_Correct(Object value) {
        assertDoesNotThrow(() -> validator.ensureValid("key", value));
    }

    @ParameterizedTest
    @MethodSource
    void ensureValid_Incorrect(Object value) {
        assertThrows(ConfigException.class, () -> validator.ensureValid("key", value));
    }

    public static Stream<Arguments> ensureValid_Incorrect() {
        return Stream.of(
                Arguments.of(""),
                Arguments.of((String) null),
                Arguments.of(Integer.valueOf(1)),
                Arguments.of("application/json; charset=utf-12345")
        );
    }

}
