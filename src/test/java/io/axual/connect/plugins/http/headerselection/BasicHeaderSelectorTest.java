package io.axual.connect.plugins.http.headerselection;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import static io.axual.connect.plugins.http.headerselection.BasicHeaderSelectorConfig.HEADER_ALIAS_CONFIG;
import static io.axual.connect.plugins.http.headerselection.BasicHeaderSelectorConfig.HEADER_KEY_CONFIG_FORMAT;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.axual.connect.plugins.http.sender.HttpSenderRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.record.TimestampType;
import org.apache.kafka.connect.header.ConnectHeaders;
import org.apache.kafka.connect.header.Headers;
import org.apache.kafka.connect.sink.SinkRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class BasicHeaderSelectorTest {


  @ParameterizedTest
  @MethodSource
  void selectHeaders(Collection<String> headerKeys, Headers headers,
      Collection<Header> expectedHeaders) {
    SinkRecord record = new SinkRecord("test", 0, null, null, null, null, 0, 0L,
        TimestampType.CREATE_TIME, headers);
    final HttpSenderRequest request = new HttpSenderRequest(record);

    Map<String, String> config = new HashMap<>();
    ArrayList<String> aliases = new ArrayList<>(headerKeys.size());
    int aliasCounter = 1;
    for (String key : headerKeys) {
      String alias = "ALIAS" + aliasCounter++;
      aliases.add(alias);
      config.put(String.format(HEADER_KEY_CONFIG_FORMAT, alias), key);
    }
    config.put(HEADER_ALIAS_CONFIG, String.join(",", aliases));

    BasicHeaderSelector selector = new BasicHeaderSelector();
    selector.configure(config);

    HttpSenderRequest processedRequest = selector.selectHeaders(request);
    assertSame(request, processedRequest);
    assertEquals(expectedHeaders.size(), processedRequest.getSelectedHeaders().size());
    for (Header expected : expectedHeaders) {
      boolean found = false;
      for (Header returned : processedRequest.getSelectedHeaders()) {
        if (expected.getName().equals(returned.getName()) && expected.getValue()
            .equals(returned.getValue())) {
          found = true;
        }
      }
      assertTrue(found, "Expected header in list " + expected);
    }
  }

  private static Stream<Arguments> selectHeaders() {
    ConnectHeaders connectHeaders = new ConnectHeaders();
    Set<String> headerKeys = new HashSet<>();
    List<Header> expectedHttpHeaders = new ArrayList<>();

    final String KEY1 = "someKey1";
    final String VALUE1 = "someValue1";
    headerKeys.add(KEY1);
    connectHeaders.add(KEY1, VALUE1, null);
    expectedHttpHeaders.add(new BasicHeader(KEY1, VALUE1));

    final String KEY2 = "someKey2";
    final String VALUE2 = "someValue2";
    headerKeys.add(KEY2);
    connectHeaders.add(KEY2, VALUE2, null);
    expectedHttpHeaders.add(new BasicHeader(KEY2, VALUE2));

    final String KEY3 = "someKey3";
    final String VALUE3 = "someValue3";
    headerKeys.add(KEY3);
    connectHeaders.add(KEY3, VALUE3, null);
    expectedHttpHeaders.add(new BasicHeader(KEY3, VALUE3));

    final String KEY4 = "extraKey";
    final List<String> VALUE4 = emptyList();
    Set<String> headerKeysExtra = new HashSet<>(headerKeys);
    ConnectHeaders connectHeadersExtra = connectHeaders.duplicate();

    headerKeysExtra.add(KEY4);
    connectHeadersExtra.add(KEY4, VALUE4, null);

    return Stream.of(
        // connect headers match requested headers exactly
        Arguments.of(headerKeys, connectHeaders, expectedHttpHeaders),

        // connect headers misses a requested header
        Arguments.of(headerKeysExtra, connectHeaders, expectedHttpHeaders),

        // connect headers has no String object for the requested extra header
        Arguments.of(headerKeysExtra, connectHeadersExtra, expectedHttpHeaders),

        // Header expected, none supplied
        Arguments.of(headerKeys, new ConnectHeaders(), emptySet()),

        // No headers expected, ignore all given
        Arguments.of(emptySet(), connectHeaders, emptySet()),

        // No headers expected, none given
        Arguments.of(emptySet(), new ConnectHeaders(), emptySet())
    );
  }

  @Test
  void configDefinition_withAliases() {
    final String alias1 = "alias1";
    final String alias2 = "alias2";
    final String expectedKeyConfigAlias1 = String.format(HEADER_KEY_CONFIG_FORMAT, alias1);
    final String expectedKeyConfigAlias2 = String.format(HEADER_KEY_CONFIG_FORMAT, alias2);

    Map<String, Object> configs = new HashMap<>();
    configs.put(HEADER_ALIAS_CONFIG, String.join(",", alias1, alias2));

    BasicHeaderSelector selector = new BasicHeaderSelector();
    ConfigDef selectorConfig = selector.configDefinition(configs);
    assertTrue(selectorConfig.configKeys().containsKey(HEADER_ALIAS_CONFIG),"Expected Config definition " + HEADER_ALIAS_CONFIG);
    assertTrue(selectorConfig.configKeys().containsKey(expectedKeyConfigAlias1),"Expected Config definition " + expectedKeyConfigAlias1);
    assertTrue(selectorConfig.configKeys().containsKey(expectedKeyConfigAlias2),"Expected Config definition " + expectedKeyConfigAlias2);
  }

  @Test
  void configDefinition_withoutAliases() {
    Map<String, Object> configs = new HashMap<>();

    BasicHeaderSelector selector = new BasicHeaderSelector();
    ConfigDef selectorConfig = selector.configDefinition(configs);
    assertTrue(selectorConfig.configKeys().containsKey(HEADER_ALIAS_CONFIG),"Expected Config definition " + HEADER_ALIAS_CONFIG);
  }
}
