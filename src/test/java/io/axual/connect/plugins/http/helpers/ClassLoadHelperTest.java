package io.axual.connect.plugins.http.helpers;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.axual.connect.plugins.http.authentication.NoopAuthenticationProvider;
import io.axual.connect.plugins.http.exceptions.ConfigurationException;
import io.axual.connect.plugins.http.sender.IAuthenticationProvider;
import io.axual.connect.plugins.http.sender.IMessageFormatter;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.Test;

class ClassLoadHelperTest {

  public static final Class<?> EXISTING_CLASS = NoopAuthenticationProvider.class;
  public static final String EXISTING_CLASS_NAME = EXISTING_CLASS.getName();
  public static final String NON_EXISTING_CLASS_NAME = "this.class.does.not.Exist";

  @Test
  void newInstance_Valid() {
    IAuthenticationProvider provider = ClassLoadHelper.INSTANCE.newInstance(IAuthenticationProvider.class,EXISTING_CLASS_NAME);
    assertTrue(provider instanceof NoopAuthenticationProvider);
  }

  @Test
  void classForName_Exists() {
    Class<? extends IAuthenticationProvider> authProvClass = ClassLoadHelper.INSTANCE.classForName(IAuthenticationProvider.class,EXISTING_CLASS_NAME);
    assertNotNull(authProvClass);
    assertEquals(EXISTING_CLASS,authProvClass);
  }

  @Test
  void classForName_NotExists() {
    assertThrows( ConfigurationException.class, ()->ClassLoadHelper.INSTANCE.classForName(IAuthenticationProvider.class,NON_EXISTING_CLASS_NAME));
  }

  @Test
  void classForName_InvalidBase() {
    assertThrows( ConfigurationException.class, ()->ClassLoadHelper.INSTANCE.classForName(IMessageFormatter.class,EXISTING_CLASS_NAME));
  }

  @Test
  void createInstance_Valid() {
    NoopAuthenticationProvider instance = ClassLoadHelper.INSTANCE.createInstance(
        NoopAuthenticationProvider.class);
    assertNotNull(instance);
  }

  @Test
  void createInstance_NullClass() {
    assertThrows(ConfigurationException.class, ()->ClassLoadHelper.INSTANCE.createInstance(null));
  }

  @Test
  void createInstance_InvalidConstructor() {
    assertThrows(ConfigurationException.class, ()->ClassLoadHelper.INSTANCE.createInstance(
        InvalidConstructorImpl.class));
  }

  @Test
  void createInstance_ExceptionConstructing() {
    assertThrows(ConfigurationException.class, ()->ClassLoadHelper.INSTANCE.createInstance(
        ExceptionThrowingImpl.class));
  }

  static class InvalidConstructorImpl implements IAuthenticationProvider{

    public InvalidConstructorImpl(String argument1){
      // Meant to fail loadClass
    }

    @Override
    public HttpClientBuilder addAuthentication(HttpClientBuilder clientBuilder) {
      return clientBuilder;
    }
  }

  static class ExceptionThrowingImpl implements IAuthenticationProvider{

    public ExceptionThrowingImpl(){
      throw new UnsupportedOperationException();
    }

    @Override
    public HttpClientBuilder addAuthentication(HttpClientBuilder clientBuilder) {
      return clientBuilder;
    }
  }
}