package io.axual.connect.plugins.http.sender;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.Test;

class IAuthenticationProviderTest {

  @Test
  void implementationTest() {
    final AtomicReference<Map<String, ?>> configRef = new AtomicReference<>(null);
    final AtomicReference<HttpClientBuilder> builderRef = new AtomicReference<>(null);

    IAuthenticationProvider provider = new IAuthenticationProvider() {
      @Override
      public void configure(Map<String, ?> configs) {
        configRef.set(configs);
      }

      @Override
      public HttpClientBuilder addAuthentication(HttpClientBuilder clientBuilder) {
        builderRef.set(clientBuilder);
        return clientBuilder;
      }
    };

    final Map<String, String> config = Collections
        .unmodifiableMap(Collections.singletonMap("Key", "Value"));
    provider.configure(config);

    final HttpClientBuilder builder = HttpClientBuilder.create();
    provider.addAuthentication(builder);

    assertSame(config, configRef.get());
    assertSame(builder, builderRef.get());
  }

  @Test
  void lambdaTest() {
    final AtomicReference<HttpClientBuilder> builderRef = new AtomicReference<>(null);
    IAuthenticationProvider provider = httpBuilder -> {
      builderRef.set(httpBuilder);
      return httpBuilder;
    };

    final Map<String, String> config = Collections
        .unmodifiableMap(Collections.singletonMap("Key", "Value"));
    provider.configure(config);

    final HttpClientBuilder builder = HttpClientBuilder.create();
    provider.addAuthentication(builder);

    assertSame(builder, builderRef.get());
  }

}
