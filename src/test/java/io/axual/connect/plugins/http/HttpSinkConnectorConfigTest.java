package io.axual.connect.plugins.http;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static io.axual.connect.plugins.http.HttpSinkConnectorConfig.*;
import static java.util.Collections.emptyMap;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.axual.connect.plugins.http.HttpSinkConnectorConfig.ClassValidator;
import io.axual.connect.plugins.http.authentication.NoopAuthenticationProvider;
import io.axual.connect.plugins.http.exceptions.ConfigurationException;
import io.axual.connect.plugins.http.formatter.JsonEnvelopeMessageFormatter;
import io.axual.connect.plugins.http.headerselection.BasicHeaderSelector;
import io.axual.connect.plugins.http.sender.IConfigurable;
import io.axual.connect.plugins.http.sender.IMessageFormatter;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import javax.net.ssl.SSLContext;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigDef.Importance;
import org.apache.kafka.common.config.ConfigDef.Type;
import org.apache.kafka.common.config.ConfigException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class HttpSinkConnectorConfigTest {

  public static final String CONNECTOR_NAME = "testConnector";
  public static final String EXPECTED_ENDPOINT = "http://somewhere.totest";
  public static final String EXPECTED_METHOD = "PUT";
  public static final String EXPECTED_CONTENT_LOGGER_NAME = "SomeTestLogger";
  public static final String EXPECTED_PROTOCOL_1 = "SSL Protocol 1";
  public static final String EXPECTED_PROTOCOL_2 = "SSL Protocol 2";
  public static final String EXPECTED_PROTOCOL_3 = "SSL Protocol 3";

  public static final String EXPECTED_CIPHER_SUITE_1 = "Cipher Suite 1";
  public static final String EXPECTED_CIPHER_SUITE_2 = "Cipher Suite 2";
  public static final String EXPECTED_CIPHER_SUITE_3 = "Cipher Suite 3";

  public static final Boolean EXPECTED_SSL_ENABLE_HOSTNAME_VERIFICATION = Boolean.FALSE;

  public static final Integer[] EXPECTED_DEFAULT_STATUS_CODES= {200,204};
  public static final String EXPECTED_DEFAULT_CONTENT_LOGGER_NAME = CONNECTOR_NAME;
  public static final String EXPECTED_DEFAULT_METHOD = "POST";
  public static final Boolean EXPECTED_DEFAULT_SSL_ENABLE_HOSTNAME_VERIFICATION = Boolean.TRUE;
  public static final String[] EXPECTED_DEFAULT_SSL_PROTOCOLS;
  public static final String[] EXPECTED_DEFAULT_SSL_CIPHER_SUITES;
  public static final String[] EXPECTED_DEFAULT_SSL_CERTIFICATE_AUTHORITY_FILE_LOCATION = new String[0];
  public static final String[] EXPECTED_DEFAULT_SSL_CERTIFICATE_AUTHORITY_CERTIFICATES = new String[0];

  public static final String STATIC_HEADER_ALIAS_1 = "static_header_alias1";
  public static final String STATIC_HEADER_ALIAS_2 = "static_header_alias2";
  public static final String STATIC_HEADER_ALIAS_3 = "static_header_alias3";
  public static final String STATIC_HEADER_KEY_1 = "staticHeaderKey1";
  public static final String STATIC_HEADER_KEY_2 = "staticHeaderKey2";
  public static final String STATIC_HEADER_KEY_3 = "staticHeaderKey3";
  public static final String STATIC_HEADER_VALUE_1 = "staticHeaderValue1";
  public static final String STATIC_HEADER_VALUE_2 = "staticHeaderValue2";
  public static final String STATIC_HEADER_VALUE_3 = "staticHeaderValue3";


  static {
    // load defaults
    String[] protocols, cipherSuites;

    try {
      SSLContext defaultContext = SSLContext.getDefault();
      protocols = defaultContext.getSupportedSSLParameters().getProtocols();
      cipherSuites = defaultContext.getSupportedSSLParameters().getCipherSuites();
    } catch (NoSuchAlgorithmException e) {
      protocols = System.getProperty("https.protocols").split(" *. *");
      cipherSuites = System.getProperty("https.cipherSuites").split(" *. *");
    }
    EXPECTED_DEFAULT_SSL_PROTOCOLS = protocols;
    EXPECTED_DEFAULT_SSL_CIPHER_SUITES = cipherSuites;
  }

  Map<String, String> getMinimalConfiguration() {
    Map<String, String> configs = new HashMap<>();
    configs.put(ENDPOINT_CONFIG, EXPECTED_ENDPOINT);
    return configs;
  }

  Map<String, String> getFullConfiguration() {
    Map<String, String> configs = getMinimalConfiguration();
    configs.put(CONTENT_LOGGER_ENABLED_CONFIG, "true");
    configs.put("name", CONNECTOR_NAME);
    configs.put(CONTENT_LOGGER_NAME_CONFIG, EXPECTED_CONTENT_LOGGER_NAME);
    configs.put(METHOD_CONFIG, EXPECTED_METHOD);
    configs.put(SSL_PROTOCOL_CONFIG,
        String.join(",", EXPECTED_PROTOCOL_1, EXPECTED_PROTOCOL_2, EXPECTED_PROTOCOL_3));
    configs.put(SSL_CIPHER_SUITES_CONFIG,
        String
            .join(",", EXPECTED_CIPHER_SUITE_1, EXPECTED_CIPHER_SUITE_2, EXPECTED_CIPHER_SUITE_3));

    configs.put(SSL_ENABLE_HOSTNAME_VERIFICATION_CONFIG,
        Boolean.toString(EXPECTED_SSL_ENABLE_HOSTNAME_VERIFICATION));
    return configs;
  }

  @Test
  void getConfigurationDefinition_Basic() {
    ConfigDef configDef = HttpSinkConnectorConfig.getConfigurationDefinition(emptyMap());
    assertNotNull(configDef);
    assertFalse(configDef.names().isEmpty());
  }

  public static class ConfigurableImpl implements IConfigurable{
    static ConfigDef definedConfig = new ConfigDef()
        .define("Key1", Type.STRING, Importance.HIGH,"SomeDoc")
        .define("Key2", Type.INT, Importance.MEDIUM,"SomeDoc2")
        .define("Key3", Type.LONG, Importance.LOW,"SomeDoc3")
        ;

    @Override
    public ConfigDef configDefinition(Map<String, ?> configs) {
      return definedConfig;
    }
  }

  @Test
  void getConfigurationDefinition_Enriched() {
    ConfigDef formatterConfigDef = new ConfigurableImpl().configDefinition(emptyMap());
    ConfigDef authConfigDef = new ConfigurableImpl().configDefinition(emptyMap());
    ConfigDef headerConfigDef = new ConfigurableImpl().configDefinition(emptyMap());

    Map<String,String> configProps = getFullConfiguration();
    configProps.put(MESSAGE_FORMATTER_CLASS_CONFIG, ConfigurableImpl.class.getName());
    configProps.put(HEADER_SELECTOR_CLASS_CONFIG, ConfigurableImpl.class.getName());
    configProps.put(AUTHENTICATION_PROVIDER_CLASS_CONFIG, ConfigurableImpl.class.getName());

    List<String> staticHeaderAliases = Arrays.asList(STATIC_HEADER_ALIAS_1,STATIC_HEADER_ALIAS_2, STATIC_HEADER_ALIAS_3);
    configProps.put(STATIC_HEADER_ALIAS_CONFIG, String.join(",",staticHeaderAliases));

    ConfigDef connectorConfigDef = HttpSinkConnectorConfig.getConfigurationDefinition(configProps);

    for(String sourceConfigKey: formatterConfigDef.names()){
      String prefixedKey = MESSAGE_FORMATTER_CLASS_PARAM_PREFIX+sourceConfigKey;
      assertTrue(connectorConfigDef.names().contains(prefixedKey), "Expecting the prefixed key "+prefixedKey);
    }
    for(String sourceConfigKey: authConfigDef.names()){
      String prefixedKey = AUTHENTICATION_PROVIDER_CLASS_PARAM_PREFIX+sourceConfigKey;
      assertTrue(connectorConfigDef.names().contains(prefixedKey), "Expecting the prefixed key "+prefixedKey);
    }
    for(String sourceConfigKey: headerConfigDef.names()){
      String prefixedKey = HEADER_SELECTOR_CLASS_PARAM_PREFIX+sourceConfigKey;
      assertTrue(connectorConfigDef.names().contains(prefixedKey), "Expecting the prefixed key "+prefixedKey);
    }

    for(String alias: staticHeaderAliases){
      String expectedKeyForName=String.format(STATIC_HEADER_NAME_CONFIG_FORMAT, alias);
      String expectedKeyForValue=String.format(STATIC_HEADER_VALUE_CONFIG_FORMAT, alias);

      assertTrue(connectorConfigDef.names().contains(expectedKeyForName), "Expecting the static header key "+expectedKeyForName);
      assertTrue(connectorConfigDef.names().contains(expectedKeyForValue), "Expecting the static header key "+expectedKeyForValue);
    }
  }

  @Test
  void testConstructor_CorrectConfiguration() {
    assertDoesNotThrow(() -> new HttpSinkConnectorConfig(getFullConfiguration()));
  }

  @Test
  void testConstructor_IncorrectConfiguration() {
    Map<String, String> props = getFullConfiguration();
    getMinimalConfiguration().keySet().forEach(key -> props.remove(key));
    assertThrows(ConfigException.class, () -> new HttpSinkConnectorConfig(props));
  }

  @Test
  void testConstructor_InvalidAuthenticationProvider() {
    Map<String,String> configs = getFullConfiguration();
    configs.put(AUTHENTICATION_PROVIDER_CLASS_CONFIG,String.class.getName());
    assertThrows(ConfigurationException.class,()-> new HttpSinkConnectorConfig(configs));
  }

  @Test
  void testConstructor_InvalidMessageFormatter() {
    Map<String,String> configs = getFullConfiguration();
    configs.put(MESSAGE_FORMATTER_CLASS_CONFIG,String.class.getName());
    assertThrows(ConfigurationException.class,()-> new HttpSinkConnectorConfig(configs));
  }

  @Test
  void testConstructor_InvalidHeaderSelector() {
    Map<String,String> configs = getFullConfiguration();
    configs.put(HEADER_SELECTOR_CLASS_CONFIG,String.class.getName());
    assertThrows(ConfigurationException.class,()-> new HttpSinkConnectorConfig(configs));
  }

  @ParameterizedTest
  @ValueSource(strings = {"application/json", "application/json; charset=utf-8", " application/soap+xml; charset=iso-8859-1", "othermimetype", "othermimetype; charset=iso-8859-1"})
  void testConstructor_ValidContentTypes(String contentType) {
    Map<String,String> configs = getFullConfiguration();
    configs.put(CONTENT_TYPE_CONFIG,contentType);
    assertDoesNotThrow(()-> new HttpSinkConnectorConfig(configs));
  }
  @ParameterizedTest
  @MethodSource
  void testConstructor_InvalidContentTypes(String contentType) {
    Map<String,String> configs = getFullConfiguration();
    configs.put(CONTENT_TYPE_CONFIG,contentType);
    assertThrows(ConfigException.class,()-> new HttpSinkConnectorConfig(configs));
  }
  static Stream<Arguments> testConstructor_InvalidContentTypes(){
    return Stream.of(
            Arguments.of(""),
            Arguments.of("someother/mime; charset=utf-12345")
    );
  }

  @Test
  void verifyDefaults() {
    HttpSinkConnectorConfig connectorConfig = new HttpSinkConnectorConfig(
        getMinimalConfiguration());
    assertNull(connectorConfig.getContentLoggerName());
    assertEquals(EXPECTED_DEFAULT_METHOD, connectorConfig.getMethod());
    assertArrayEquals(EXPECTED_DEFAULT_STATUS_CODES, connectorConfig.getValidStatusCodes().toArray(new Integer[0]));
    assertEquals(EXPECTED_DEFAULT_SSL_ENABLE_HOSTNAME_VERIFICATION,
        connectorConfig.getSslEnableHostnameVerification());
    assertArrayEquals(EXPECTED_DEFAULT_SSL_CIPHER_SUITES,
        connectorConfig.getSslCipherSuites().toArray());
    assertArrayEquals(EXPECTED_DEFAULT_SSL_PROTOCOLS, connectorConfig.getSslProtocols().toArray());
    assertArrayEquals(EXPECTED_DEFAULT_SSL_CERTIFICATE_AUTHORITY_FILE_LOCATION, connectorConfig.getSslCertificateAuthorityFileLocation().toArray());
    assertArrayEquals(EXPECTED_DEFAULT_SSL_CERTIFICATE_AUTHORITY_CERTIFICATES,connectorConfig.getSslCertificateAuthorityCertificates().toArray());
  }

  @Test
  void getEndpoint() {
    HttpSinkConnectorConfig connectorConfig = new HttpSinkConnectorConfig(getFullConfiguration());
    assertEquals(EXPECTED_ENDPOINT, connectorConfig.getEndpoint());
  }

  @Test
  void getMethod() {
    HttpSinkConnectorConfig connectorConfig = new HttpSinkConnectorConfig(getFullConfiguration());
    assertEquals(EXPECTED_METHOD, connectorConfig.getMethod());
  }

  @Test
  void getContentLoggerName_Enabled() {
    HttpSinkConnectorConfig connectorConfig = new HttpSinkConnectorConfig(getFullConfiguration());
    assertEquals(EXPECTED_CONTENT_LOGGER_NAME, connectorConfig.getContentLoggerName());
  }

  @Test
  void getContentLoggerName_Disabled() {
    Map<String,String> config = getFullConfiguration();
    config.put(CONTENT_LOGGER_ENABLED_CONFIG, "false");
    HttpSinkConnectorConfig connectorConfig = new HttpSinkConnectorConfig(config);
    assertNull( connectorConfig.getContentLoggerName());
  }

  @Test
  void teAuthenticationProvider() {
    HttpSinkConnectorConfig connectorConfig = new HttpSinkConnectorConfig(getFullConfiguration());
    assertEquals(NoopAuthenticationProvider.class,
        connectorConfig.getAuthenticationProvider().getClass());
  }

  @Test
  void getMessageFormatter() {
    HttpSinkConnectorConfig connectorConfig = new HttpSinkConnectorConfig(getFullConfiguration());
    assertEquals(JsonEnvelopeMessageFormatter.class,
        connectorConfig.getMessageFormatter().getClass());
  }

  @Test
  void getHeaderSelector() {
    HttpSinkConnectorConfig connectorConfig = new HttpSinkConnectorConfig(getFullConfiguration());
    assertEquals(BasicHeaderSelector.class, connectorConfig.getHeaderSelector().getClass());
  }

  @Test
  void getSslProtocols() {
    HttpSinkConnectorConfig connectorConfig = new HttpSinkConnectorConfig(getFullConfiguration());
    List<String> protocols = connectorConfig.getSslProtocols();
    assertNotNull(protocols);
    assertTrue(protocols.contains(EXPECTED_PROTOCOL_1),
        "Expected value not in list: " + EXPECTED_PROTOCOL_1);
    assertTrue(protocols.contains(EXPECTED_PROTOCOL_2),
        "Expected value not in list: " + EXPECTED_PROTOCOL_2);
    assertTrue(protocols.contains(EXPECTED_PROTOCOL_3),
        "Expected value not in list: " + EXPECTED_PROTOCOL_3);
  }

  @Test
  void getSslCipherSuited() {
    HttpSinkConnectorConfig connectorConfig = new HttpSinkConnectorConfig(getFullConfiguration());
    List<String> cipherSuites = connectorConfig.getSslCipherSuites();
    assertNotNull(cipherSuites);
    assertTrue(cipherSuites.contains(EXPECTED_CIPHER_SUITE_1),
        "Expected value not in list: " + EXPECTED_CIPHER_SUITE_1);
    assertTrue(cipherSuites.contains(EXPECTED_CIPHER_SUITE_2),
        "Expected value not in list: " + EXPECTED_CIPHER_SUITE_2);
    assertTrue(cipherSuites.contains(EXPECTED_CIPHER_SUITE_3),
        "Expected value not in list: " + EXPECTED_CIPHER_SUITE_3);
  }

  @Test
  void getSslHostnameVerificationEnabled() {
    HttpSinkConnectorConfig connectorConfig = new HttpSinkConnectorConfig(getFullConfiguration());
    assertEquals(EXPECTED_SSL_ENABLE_HOSTNAME_VERIFICATION,
        connectorConfig.getSslEnableHostnameVerification());
  }

  @Test
  void getStaticHeaders_NoneSet(){
    HttpSinkConnectorConfig connectorConfig = new HttpSinkConnectorConfig(getMinimalConfiguration());
    Map<String,String> staticHeaders = connectorConfig.getStaticHeaders();
    assertNotNull(staticHeaders);
    assertTrue(staticHeaders.isEmpty());
  }

  @Test
  void getStaticHeaders_Set(){
    Map<String,String> configProps = getFullConfiguration();
    configProps.put(STATIC_HEADER_ALIAS_CONFIG, String.join(",",STATIC_HEADER_ALIAS_1,STATIC_HEADER_ALIAS_2, STATIC_HEADER_ALIAS_3));

    configProps.put(String.format(STATIC_HEADER_NAME_CONFIG_FORMAT, STATIC_HEADER_ALIAS_1), STATIC_HEADER_KEY_1);
    configProps.put(String.format(STATIC_HEADER_VALUE_CONFIG_FORMAT, STATIC_HEADER_ALIAS_1), STATIC_HEADER_VALUE_1);

    configProps.put(String.format(STATIC_HEADER_NAME_CONFIG_FORMAT, STATIC_HEADER_ALIAS_2), STATIC_HEADER_KEY_2);
    configProps.put(String.format(STATIC_HEADER_VALUE_CONFIG_FORMAT, STATIC_HEADER_ALIAS_2), STATIC_HEADER_VALUE_2);

    configProps.put(String.format(STATIC_HEADER_NAME_CONFIG_FORMAT, STATIC_HEADER_ALIAS_3), STATIC_HEADER_KEY_3);
    configProps.put(String.format(STATIC_HEADER_VALUE_CONFIG_FORMAT, STATIC_HEADER_ALIAS_3), STATIC_HEADER_VALUE_3);

    HttpSinkConnectorConfig connectorConfig = new HttpSinkConnectorConfig(configProps);
    Map<String,String> staticHeaders = connectorConfig.getStaticHeaders();
    assertNotNull(staticHeaders);
    assertFalse(staticHeaders.isEmpty());

    assertTrue(staticHeaders.containsKey(STATIC_HEADER_KEY_1));
    assertTrue(staticHeaders.containsKey(STATIC_HEADER_KEY_2));
    assertTrue(staticHeaders.containsKey(STATIC_HEADER_KEY_3));

    assertEquals(STATIC_HEADER_VALUE_1, staticHeaders.get(STATIC_HEADER_KEY_1));
    assertEquals(STATIC_HEADER_VALUE_2, staticHeaders.get(STATIC_HEADER_KEY_2));
    assertEquals(STATIC_HEADER_VALUE_3, staticHeaders.get(STATIC_HEADER_KEY_3));
  }


  @Test
  void getStaticHeaders_FieldNotSet(){
    Map<String,String> configProps = getFullConfiguration();
    configProps.put(STATIC_HEADER_ALIAS_CONFIG, String.join(",",STATIC_HEADER_ALIAS_1,STATIC_HEADER_ALIAS_2, STATIC_HEADER_ALIAS_3));

    configProps.put(String.format(STATIC_HEADER_NAME_CONFIG_FORMAT, STATIC_HEADER_ALIAS_1), STATIC_HEADER_KEY_1);
    configProps.put(String.format(STATIC_HEADER_VALUE_CONFIG_FORMAT, STATIC_HEADER_ALIAS_1), STATIC_HEADER_VALUE_1);

    configProps.put(String.format(STATIC_HEADER_NAME_CONFIG_FORMAT, STATIC_HEADER_ALIAS_2), STATIC_HEADER_KEY_2);
    configProps.put(String.format(STATIC_HEADER_VALUE_CONFIG_FORMAT, STATIC_HEADER_ALIAS_2), STATIC_HEADER_VALUE_2);

    assertThrows(ConfigException.class,()->new HttpSinkConnectorConfig(configProps));
  }

  @Test
  void classValidator_Null(){
    assertThrows(IllegalArgumentException.class,()->new ClassValidator(null));
  }

  @Test
  void classValidator_ClassCorrect(){

    ClassValidator validator = new ClassValidator(IMessageFormatter.class);

    assertDoesNotThrow(()->validator.ensureValid("Name",JsonEnvelopeMessageFormatter.class.getName()));
  }

  @Test
  void classValidator_ClassNotFound(){

    ClassValidator validator = new ClassValidator(IMessageFormatter.class);

    assertThrows(ConfigException.class,()->validator.ensureValid("Name","does.not.exist"));
  }

  @Test
  void classValidator_ClassWrongType(){

    ClassValidator validator = new ClassValidator(IMessageFormatter.class);

    final String className = NoopAuthenticationProvider.class.getName();
    assertThrows(ConfigException.class,()->validator.ensureValid("Name",className));
  }

  @Test
  void classValidator_ClassNull(){

    ClassValidator validator = new ClassValidator(IMessageFormatter.class);

    assertThrows(ConfigException.class,()->validator.ensureValid("Name",null));
  }

}