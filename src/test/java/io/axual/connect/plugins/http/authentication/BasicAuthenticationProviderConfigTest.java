package io.axual.connect.plugins.http.authentication;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static io.axual.connect.plugins.http.HttpSinkConnectorConfig.ENDPOINT_CONFIG;
import static io.axual.connect.plugins.http.authentication.BasicAuthenticationProviderConfig.ENABLE_PREEMPTIVE_AUTHENTICATION_CONFIG;
import static io.axual.connect.plugins.http.authentication.BasicAuthenticationProviderConfig.PASSWORD_CONFIG;
import static io.axual.connect.plugins.http.authentication.BasicAuthenticationProviderConfig.USERNAME_CONFIG;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.common.config.ConfigException;
import org.apache.kafka.common.config.types.Password;
import org.junit.jupiter.api.Test;

class BasicAuthenticationProviderConfigTest {

  public static final String ENDPOINT = "https://somewhere:12345/over/there";
  public static final String USERNAME_SET = "myUser";
  public static final String USERNAME_EMPTY = "";
  public static final String USERNAME_NULL = null;
  public static final Password PASSWORD_SET = new Password("cantGuessMe");
  public static final Password PASSWORD_EMPTY = new Password("");
  public static final Password PASSWORD_NULL = null;

  public static final Boolean EXPECTED_DEFAULT_PREEMPTIVE_AUTH_ENABLED = Boolean.FALSE;
  public static final Boolean PREEMPTIVE_AUTH_ENABLED = Boolean.TRUE;

  @Test
  void getUsername_NotSet() {
    Map<String, Object> configProps = new HashMap<>();
    configProps.put(ENDPOINT_CONFIG, ENDPOINT);
    configProps.put(PASSWORD_CONFIG, PASSWORD_SET);

    assertThrows(ConfigException.class, () -> new BasicAuthenticationProviderConfig(configProps));
  }

  @Test
  void getUsername_Set() {
    Map<String, Object> configProps = new HashMap<>();
    configProps.put(ENDPOINT_CONFIG, ENDPOINT);
    configProps.put(USERNAME_CONFIG, USERNAME_SET);
    configProps.put(PASSWORD_CONFIG, PASSWORD_SET);

    BasicAuthenticationProviderConfig config = new BasicAuthenticationProviderConfig(configProps);
    assertEquals(USERNAME_SET, config.getUsername());
  }

  @Test
  void getUsername_Empty() {
    Map<String, Object> configProps = new HashMap<>();
    configProps.put(ENDPOINT_CONFIG, ENDPOINT);
    configProps.put(USERNAME_CONFIG, USERNAME_EMPTY);
    configProps.put(PASSWORD_CONFIG, PASSWORD_SET);

    BasicAuthenticationProviderConfig config = new BasicAuthenticationProviderConfig(configProps);
    assertEquals(USERNAME_EMPTY, config.getUsername());
  }

  @Test
  void getUsername_Null() {
    Map<String, Object> configProps = new HashMap<>();
    configProps.put(ENDPOINT_CONFIG, ENDPOINT);
    configProps.put(USERNAME_CONFIG, USERNAME_NULL);
    configProps.put(PASSWORD_CONFIG, PASSWORD_SET);

    BasicAuthenticationProviderConfig config = new BasicAuthenticationProviderConfig(configProps);
    assertNull(config.getUsername());
  }

  @Test
  void getPassword_NotSet() {
    Map<String, Object> configProps = new HashMap<>();
    configProps.put(ENDPOINT_CONFIG, ENDPOINT);
    configProps.put(USERNAME_CONFIG, USERNAME_SET);

    assertThrows(ConfigException.class, () -> new BasicAuthenticationProviderConfig(configProps));
  }

  @Test
  void getPassword_Set() {
    Map<String, Object> configProps = new HashMap<>();
    configProps.put(ENDPOINT_CONFIG, ENDPOINT);
    configProps.put(USERNAME_CONFIG, USERNAME_SET);
    configProps.put(PASSWORD_CONFIG, PASSWORD_SET);

    BasicAuthenticationProviderConfig config = new BasicAuthenticationProviderConfig(configProps);
    assertEquals(PASSWORD_SET, config.getPassword());
  }

  @Test
  void getPassword_Empty() {
    Map<String, Object> configProps = new HashMap<>();
    configProps.put(ENDPOINT_CONFIG, ENDPOINT);
    configProps.put(USERNAME_CONFIG, USERNAME_SET);
    configProps.put(PASSWORD_CONFIG, PASSWORD_EMPTY);

    BasicAuthenticationProviderConfig config = new BasicAuthenticationProviderConfig(configProps);
    assertEquals(PASSWORD_EMPTY, config.getPassword());
  }

  @Test
  void getPassword_Null() {
    Map<String, Object> configProps = new HashMap<>();
    configProps.put(ENDPOINT_CONFIG, ENDPOINT);
    configProps.put(USERNAME_CONFIG, USERNAME_SET);
    configProps.put(PASSWORD_CONFIG, PASSWORD_NULL);

    BasicAuthenticationProviderConfig config = new BasicAuthenticationProviderConfig(configProps);
    assertNull(config.getPassword());
  }

  @Test
  void isPreemptiveAuthenticationEnabled_NotSet() {
    Map<String, Object> configProps = new HashMap<>();
    configProps.put(ENDPOINT_CONFIG, ENDPOINT);
    configProps.put(USERNAME_CONFIG, USERNAME_SET);
    configProps.put(PASSWORD_CONFIG, PASSWORD_SET);
    BasicAuthenticationProviderConfig config = new BasicAuthenticationProviderConfig(configProps);

    assertEquals(EXPECTED_DEFAULT_PREEMPTIVE_AUTH_ENABLED,config.isPreemptiveAuthenticationEnabled());
  }

  @Test
  void isPreemptiveAuthenticationEnabled_Set() {
    Map<String, Object> configProps = new HashMap<>();
    configProps.put(ENDPOINT_CONFIG, ENDPOINT);
    configProps.put(USERNAME_CONFIG, USERNAME_SET);
    configProps.put(PASSWORD_CONFIG, PASSWORD_SET);
    configProps.put(ENABLE_PREEMPTIVE_AUTHENTICATION_CONFIG, PREEMPTIVE_AUTH_ENABLED);

    BasicAuthenticationProviderConfig config = new BasicAuthenticationProviderConfig(configProps);

    assertEquals(PREEMPTIVE_AUTH_ENABLED,config.isPreemptiveAuthenticationEnabled());
  }


}