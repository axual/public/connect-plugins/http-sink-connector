package io.axual.connect.plugins.http.authentication;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static io.axual.connect.plugins.http.HttpSinkConnectorConfig.ENDPOINT_CONFIG;
import static java.util.Collections.singletonMap;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.net.URI;
import java.util.Map;
import java.util.stream.Stream;
import org.apache.kafka.common.config.ConfigDef;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class AuthenticationProviderConfigTest {

  public static final ConfigDef EMPTY_CONFIG_DEF = new ConfigDef();
  public static final String HTTP_SCHEME = "http";
  public static final String HTTPS_SCHEME = "https";
  public static final String EMPTY_SCHEME = "";

  public static final int HTTP_DEFAULT_PORT = 80;
  public static final int HTTPS_DEFAULT_PORT = 443;

  public static final String HOST_1 = "somewhere";
  public static final String HOST_2 = "here";
  public static final String EMPTY_HOST = "";

  public static final int PORT_1 = 1080;
  public static final int PORT_2 = 1999;
  public static final int NEGATIVE_HOST = -100;
  public static final int EXPECTED_UNSET_NEGATIVE = -1;


  @ParameterizedTest
  @MethodSource
  void getEndpointUri(Map<String, String> props, URI expectedValue,
      Class<? extends Throwable> expectedException) {
    if (expectedException != null) {
      assertThrows(expectedException,
          () -> new AuthenticationProviderConfig(EMPTY_CONFIG_DEF, props));
    } else {
      AuthenticationProviderConfig config = new AuthenticationProviderConfig(EMPTY_CONFIG_DEF,
          props);
      assertEquals(expectedValue, config.getEndpointUri());
    }
  }

  private static Stream<Arguments> getEndpointUri() {
    return Stream.of(
        Arguments.of(singletonMap(ENDPOINT_CONFIG, HTTP_SCHEME + "://" + HOST_1),
            URI.create(HTTP_SCHEME + "://" + HOST_1), null),
        Arguments.of(singletonMap(ENDPOINT_CONFIG,
            HTTPS_SCHEME + "://" + HOST_2 + ":" + Integer.valueOf(PORT_2)),
            URI.create(HTTPS_SCHEME + "://" + HOST_2 + ":" + Integer.valueOf(PORT_2)), null),
        Arguments.of(singletonMap(ENDPOINT_CONFIG,
            HTTPS_SCHEME + "://" + EMPTY_HOST + ":" + Integer.valueOf(PORT_1)),
            URI.create(HTTPS_SCHEME + "://" + EMPTY_HOST + ":" + Integer.valueOf(PORT_1)), null),
        Arguments.of(singletonMap(ENDPOINT_CONFIG,
            EMPTY_SCHEME + "://" + HOST_1 + ":" + Integer.valueOf(PORT_1)), null,
            IllegalArgumentException.class)
    );
  }

  @ParameterizedTest
  @MethodSource
  void getEndpointScheme(Map<String, String> props, String expectedValue) {
    AuthenticationProviderConfig config = new AuthenticationProviderConfig(EMPTY_CONFIG_DEF,
        props);
    assertEquals(expectedValue, config.getEndpointScheme());
  }

  private static Stream<Arguments> getEndpointScheme() {
    return Stream.of(
        Arguments.of(singletonMap(ENDPOINT_CONFIG, HTTP_SCHEME + "://" + HOST_1), HTTP_SCHEME),
        Arguments.of(singletonMap(ENDPOINT_CONFIG,
            HTTPS_SCHEME + "://" + HOST_2 + ":" + Integer.valueOf(PORT_2)), HTTPS_SCHEME)
    );
  }

  @ParameterizedTest
  @MethodSource
  void getEndpointHost(Map<String, String> props, String expectedValue) {
    AuthenticationProviderConfig config = new AuthenticationProviderConfig(EMPTY_CONFIG_DEF,
        props);
    assertEquals(expectedValue, config.getEndpointHost());
  }

  private static Stream<Arguments> getEndpointHost() {
    return Stream.of(
        Arguments.of(singletonMap(ENDPOINT_CONFIG, HTTP_SCHEME + "://" + HOST_1), HOST_1),
        Arguments.of(singletonMap(ENDPOINT_CONFIG, HTTP_SCHEME + "://" + HOST_2), HOST_2),
        Arguments.of(singletonMap(ENDPOINT_CONFIG,
            HTTPS_SCHEME + "://" + EMPTY_HOST + ":" + Integer.valueOf(PORT_2)), null)
    );
  }

  @ParameterizedTest
  @MethodSource
  void getEndpointPort(Map<String, String> props, int expectedValue) {
    AuthenticationProviderConfig config = new AuthenticationProviderConfig(EMPTY_CONFIG_DEF,
        props);
    assertEquals(expectedValue, config.getEndpointPort());
  }

  private static Stream<Arguments> getEndpointPort() {
    return Stream.of(
        Arguments
            .of(singletonMap(ENDPOINT_CONFIG, HTTP_SCHEME + "://" + HOST_1), HTTP_DEFAULT_PORT),
        Arguments
            .of(singletonMap(ENDPOINT_CONFIG, HTTPS_SCHEME + "://" + HOST_2), HTTPS_DEFAULT_PORT),
        Arguments.of(singletonMap(ENDPOINT_CONFIG, HTTP_SCHEME + "://" + NEGATIVE_HOST),
            HTTP_DEFAULT_PORT),
        Arguments.of(singletonMap(ENDPOINT_CONFIG, HTTPS_SCHEME + "://" + NEGATIVE_HOST),
            HTTPS_DEFAULT_PORT),
        Arguments.of(singletonMap(ENDPOINT_CONFIG,
            HTTPS_SCHEME + "://" + HOST_1 + ":" + Integer.valueOf(PORT_1)), PORT_1),
        Arguments.of(singletonMap(ENDPOINT_CONFIG,
            HTTPS_SCHEME + "://" + HOST_1 + ":" + Integer.valueOf(PORT_2)), PORT_2)
    );
  }

  @Test
  void TestEqualsAndHash() {
    ConfigDef empty = new ConfigDef();
    Map<String, String> configMap1 = singletonMap(ENDPOINT_CONFIG, "http:/somewhere/");
    AuthenticationProviderConfig config1 = new AuthenticationProviderConfig(empty, configMap1);
    AuthenticationProviderConfig configCopy = new AuthenticationProviderConfig(empty, configMap1);
    Map<String, String> configMap2 = singletonMap(ENDPOINT_CONFIG, "http:/somewhere/else");
    AuthenticationProviderConfig config2 = new AuthenticationProviderConfig(empty, configMap2);

    AuthenticationProviderConfig configNull = null;

    assertEquals(config1,config1);
    assertEquals(config1,configCopy);
    assertNotEquals(config1,config2);
    assertNotEquals(config1,configNull);

    assertEquals(config1.hashCode(), configCopy.hashCode());
  }
}