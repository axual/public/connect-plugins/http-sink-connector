package io.axual.connect.plugins.http;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import com.google.common.io.Resources;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.matching.UrlPattern;

import org.apache.http.ssl.SSLContexts;
import org.apache.kafka.common.config.Config;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigValue;
import org.apache.kafka.connect.connector.ConnectorContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import io.axual.connect.plugins.http.exceptions.HttpSinkConnectorException;
import io.axual.connect.plugins.http.helpers.SslHelper;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static io.axual.connect.plugins.http.HttpSinkConnectorConfig.ENDPOINT_CONFIG;
import static io.axual.connect.plugins.http.HttpSinkConnectorConfig.HEADER_SELECTOR_CLASS_PARAM_PREFIX;
import static io.axual.connect.plugins.http.HttpSinkConnectorConfig.SSL_CERTIFICATE_AUTHORITY_FILE_LOCATION_CONFIG;
import static io.axual.connect.plugins.http.headerselection.BasicHeaderSelectorConfig.HEADER_ALIAS_CONFIG;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class HttpSinkConnectorTest {

  public static final String CONFIG_KEY = "TestKey";
  public static final String CONFIG_VALUE = "TestValue";
  public static final Map<String, String> CONFIGS = Collections
      .singletonMap(CONFIG_KEY, CONFIG_VALUE);

  @Mock
  ConnectorContext mockContext;

  @Mock
  SslHelper mockSslHelper;

  @Test
  void start() {
    HttpSinkConnector connector = new HttpSinkConnector();
    connector.start(CONFIGS);

    verifyConfigs(connector.connectorConfig);
  }

  @Test
  void start_failedRepeat() {
    HttpSinkConnector connector = new HttpSinkConnector();
    connector.initialize(mockContext);
    connector.start(CONFIGS);
    verifyConfigs(connector.connectorConfig);

    assertDoesNotThrow(() -> connector.start(CONFIGS));
    verify(mockContext, times(1)).raiseError(any(HttpSinkConnectorException.class));
  }

  void verifyConfigs(Map<String, String> toCompare) {
    assertNotSame(CONFIGS, toCompare);
    assertEquals(CONFIGS.size(), toCompare.size());
    assertTrue(toCompare.containsKey(CONFIG_KEY));
    assertEquals(CONFIG_VALUE, toCompare.get(CONFIG_KEY));
  }

  @Test
  void taskClass() {
    HttpSinkConnector connector = new HttpSinkConnector();
    assertEquals(HttpSinkTask.class, connector.taskClass());
  }

  @Test
  void taskConfigs() {
    HttpSinkConnector connector = new HttpSinkConnector();
    connector.start(CONFIGS);
    final int maxTasks = 3;

    List<Map<String, String>> taskConfigs = connector.taskConfigs(maxTasks);

    assertEquals(maxTasks, taskConfigs.size());
    for (Map<String, String> taskConfig : taskConfigs) {
      verifyConfigs(taskConfig);
    }
  }

  @Test
  void stop() {
    HttpSinkConnector connector = new HttpSinkConnector();

    connector.start(CONFIGS);
    assertFalse(connector.connectorConfig.isEmpty());
    connector.stop();
    assertTrue(connector.connectorConfig.isEmpty());

  }

  @Test
  void config() {
    HttpSinkConnector connector = new HttpSinkConnector();
    ConfigDef config = connector.config();
    assertNotNull(config);
  }

  @Test
  void version() {
    final String expectedVersion = "testing";
    HttpSinkConnector connector = new HttpSinkConnector();
    final String version = connector.version();
    assertEquals(expectedVersion, version);
  }

  @Test
  void validate_ValidNonSsl() {
    final WireMockConfiguration wiremockConfig = wireMockConfig()
        .bindAddress("0.0.0.0")
        .httpDisabled(false)
        .dynamicPort();
    final String path = "/testing";
    WireMockServer server = new WireMockServer(wiremockConfig);
    final UrlPattern urlPattern = urlEqualTo(path);
    server.stubFor(post(urlPattern).willReturn(aResponse().withStatus(204)));
    server.start();

    Map<String, String> configs = new HashMap<>();
    configs.put(ENDPOINT_CONFIG, server.baseUrl() + path);

    HttpSinkConnector connector = new HttpSinkConnector(mockSslHelper);
    Config result = connector.validate(configs);
    assertNotNull(result);
    assertTrue(result.configValues().stream().allMatch(cv->cv.errorMessages().isEmpty()));

    server.verify(0, postRequestedFor(urlPattern));
    verify(mockSslHelper, times(0)).getContext(any(),any());
  }

  @Test
  void validate_InvalidBasicHeaderSelector(){
    Map<String, String> configs = new HashMap<>();
    configs.put(ENDPOINT_CONFIG, "http://unreachable");
    final String headerAliasConfig = HEADER_SELECTOR_CLASS_PARAM_PREFIX + HEADER_ALIAS_CONFIG;
    final String headerAlias = "one";
    configs.put(headerAliasConfig, headerAlias);

    final String expectedConfigFail =
        HEADER_SELECTOR_CLASS_PARAM_PREFIX + HEADER_ALIAS_CONFIG + "." + headerAlias + ".key";

    HttpSinkConnector connector = new HttpSinkConnector(mockSslHelper);
    Config result = connector.validate(configs);
    final Optional<ConfigValue> foundConfigValue = result.configValues().stream()
        .filter(configValue -> expectedConfigFail.contentEquals(configValue.name())).findAny();

    assertTrue(foundConfigValue.isPresent(), "Expect to find the config key "+expectedConfigFail);
    assertTrue(!foundConfigValue.get().errorMessages().isEmpty(),"Expect to find errors for the config key "+expectedConfigFail);
  }

  @Test
  void validate_InvalidNotReachable() {
    final WireMockConfiguration wiremockConfig = wireMockConfig()
        .bindAddress("0.0.0.0")
        .httpDisabled(false)
        .dynamicPort();
    final String path = "/testing";
    WireMockServer server = new WireMockServer(wiremockConfig);
    server.start();

    Map<String, String> configs = new HashMap<>();
    configs.put(ENDPOINT_CONFIG, server.baseUrl() + path);
    server.stop();

    HttpSinkConnector connector = new HttpSinkConnector(mockSslHelper);
    Config result = connector.validate(configs);
    assertNotNull(result);
    assertTrue(result.configValues().stream()
        .filter(cv->cv.name().contentEquals(ENDPOINT_CONFIG))
        .allMatch(cv->!cv.errorMessages().isEmpty()));

  }

  @Test
  void validate_MalFormedHost() {

    Map<String, String> configs = new HashMap<>();
    configs.put(ENDPOINT_CONFIG, "://malformed/");

    HttpSinkConnector connector = new HttpSinkConnector(mockSslHelper);
    Config result = connector.validate(configs);
    assertNotNull(result);
    assertTrue(result.configValues().stream()
        .filter(cv->cv.name().contentEquals(ENDPOINT_CONFIG))
        .allMatch(cv->!cv.errorMessages().isEmpty()));
  }

  @Test
  void validate_ValidWithSsl() throws URISyntaxException {
    final String multipleCAFileLocation = new File(
        Resources.getResource("ssl/ca_multiple.cer")
            .toURI()).getAbsolutePath();
    final String serverWithSanKeystoreLocation = new File(
        Resources.getResource("ssl/http-sink-connector-server-with-san.server.keystore.jks")
            .toURI()).getAbsolutePath();
    final String serverWithSanKeystorePassword = "keystore_http-sink-connector-server-with-san";
    final String serverWithSanKeyPassword = "key_http-sink-connector-server-with-san";

    final WireMockConfiguration wiremockConfig = wireMockConfig()
        .bindAddress("0.0.0.0")
        .httpDisabled(true)
        .keystoreType("JKS")
        .keystorePath(serverWithSanKeystoreLocation)
        .keystorePassword(serverWithSanKeystorePassword)
        .keyManagerPassword(serverWithSanKeyPassword)
        .dynamicHttpsPort();

    final String path = "/testing";
    WireMockServer server = new WireMockServer(wiremockConfig);
    final UrlPattern urlPattern = urlEqualTo(path);
    server.stubFor(post(urlPattern).willReturn(aResponse().withStatus(204)));
    server.start();

    Map<String, String> configs = new HashMap<>();
    configs.put(ENDPOINT_CONFIG, server.baseUrl() + path);
    configs.put(SSL_CERTIFICATE_AUTHORITY_FILE_LOCATION_CONFIG,multipleCAFileLocation);

    HttpSinkConnector connector = new HttpSinkConnector();
    Config result = connector.validate(configs);
    assertNotNull(result);
    assertTrue(result.configValues().stream().allMatch(cv->cv.errorMessages().isEmpty()));

    server.verify(0, postRequestedFor(urlPattern));
  }

  @Test
  void validate_SslHandshakeError() throws URISyntaxException {
    final String serverWithSanKeystoreLocation = new File(
        Resources.getResource("ssl/http-sink-connector-server-with-san.server.keystore.jks")
            .toURI()).getAbsolutePath();
    final String serverWithSanKeystorePassword = "keystore_http-sink-connector-server-with-san";
    final String serverWithSanKeyPassword = "key_http-sink-connector-server-with-san";

    final WireMockConfiguration wiremockConfig = wireMockConfig()
        .bindAddress("0.0.0.0")
        .httpDisabled(true)
        .keystoreType("JKS")
        .keystorePath(serverWithSanKeystoreLocation)
        .keystorePassword(serverWithSanKeystorePassword)
        .keyManagerPassword(serverWithSanKeyPassword)
        .dynamicHttpsPort();

    final String path = "/testing";
    WireMockServer server = new WireMockServer(wiremockConfig);
    final UrlPattern urlPattern = urlEqualTo(path);
    server.stubFor(post(urlPattern).willReturn(aResponse().withStatus(204)));
    server.start();

    Map<String, String> configs = new HashMap<>();
    configs.put(ENDPOINT_CONFIG, server.baseUrl() + path);

    doReturn(SSLContexts.createSystemDefault()).when(mockSslHelper).getContext(any(),any());
    HttpSinkConnector connector = new HttpSinkConnector(mockSslHelper);
    Config result = connector.validate(configs);
    assertNotNull(result);

   assertTrue(result.configValues().stream()
       .filter(cv->cv.name().contentEquals(ENDPOINT_CONFIG))
   .allMatch(cv->!cv.errorMessages().isEmpty()));

    server.verify(0, postRequestedFor(urlPattern));
    verify(mockSslHelper,times(1)).getContext(any(),any());
  }
}
