package io.axual.connect.plugins.http.sender;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.google.common.io.Resources;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.MappingBuilder;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.DefaultHostnameVerifier;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.ssl.SSLContexts;
import org.junit.jupiter.api.Named;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;

import io.axual.connect.plugins.http.authentication.BasicAuthenticationProvider;
import io.axual.connect.plugins.http.authentication.NoopAuthenticationProvider;
import io.axual.connect.plugins.http.exceptions.HttpSinkConnectorException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static io.axual.connect.plugins.http.HttpSinkConnectorConfig.ENDPOINT_CONFIG;
import static io.axual.connect.plugins.http.authentication.BasicAuthenticationProviderConfig.ENABLE_PREEMPTIVE_AUTHENTICATION_CONFIG;
import static io.axual.connect.plugins.http.authentication.BasicAuthenticationProviderConfig.PASSWORD_CONFIG;
import static io.axual.connect.plugins.http.authentication.BasicAuthenticationProviderConfig.USERNAME_CONFIG;
import static org.junit.jupiter.api.Assertions.*;

class HttpSenderIT {

  // Use in simplelogger properties to set debug log level
  public static final String CONTENT_LOGGER_NAME = "HttpSinkIT-Content";
  public static final String EXPECTED_PATH = "/input/test";
  public static final String REDIRECT_PATH = "/redirect/test";
  public static final String EXPECTED_BASIC_AUTH_USER = "me";
  public static final String EXPECTED_BASIC_AUTH_PASS = "some";

  public static final int EXPECTED_STATUS_CODE = 204;
  public static final String EXPECTED_STATUS_MESSAGE = "Thank you";

  static BiConsumer<WireMockServer, HttpSenderResult> createSuccessVerifier(String contentType) {
    ContentType realContentType = ContentType.parse(contentType);
    return  (server, result) -> {
      assertTrue(result.isSuccess());
      assertNotNull(result.getStatusLine());
      assertEquals(EXPECTED_STATUS_CODE, result.getStatusLine().getStatusCode());
      assertEquals(EXPECTED_STATUS_MESSAGE, result.getStatusLine().getReasonPhrase());

      server.verify(1, postRequestedFor(urlEqualTo(EXPECTED_PATH))
      .withHeader("Content-Type", equalToIgnoreCase(realContentType.toString())));
    };
  }
  private static Stream<Arguments> simpleCallTest() {
    try {
      final WireMockConfiguration noSslConfig = wireMockConfig()
          .bindAddress("0.0.0.0")
          .httpDisabled(false)
          .dynamicPort();

      final String serverWithSanKeystoreLocation = new File(
          Resources.getResource("ssl/http-sink-connector-server-with-san.server.keystore.jks")
              .toURI()).getAbsolutePath();
      final String serverWithSanKeystorePassword = "keystore_http-sink-connector-server-with-san";
      final String serverWithSanKeyPassword = "key_http-sink-connector-server-with-san";

      final WireMockConfiguration sslWithSanConfig = wireMockConfig()
          .bindAddress("0.0.0.0")
          .httpDisabled(true)
          .keystoreType("JKS")
          .keystorePath(serverWithSanKeystoreLocation)
          .keystorePassword(serverWithSanKeystorePassword)
          .keyManagerPassword(serverWithSanKeyPassword)
          .dynamicHttpsPort();

      final String serverWithoutSanKeystoreLocation = new File(
          Resources.getResource("ssl/http-sink-connector-server-without-san.server.keystore.jks")
              .toURI()).getAbsolutePath();
      final String serverWithoutSanKeystorePassword = "keystore_http-sink-connector-server-without-san";
      final String serverWithoutSanKeyPassword = "key_http-sink-connector-server-without-san";

      final WireMockConfiguration sslWithoutSanConfig = wireMockConfig()
          .bindAddress("0.0.0.0")
          .httpDisabled(true)
          .keystoreType("JKS")
          .keystorePath(serverWithoutSanKeystoreLocation)
          .keystorePassword(serverWithoutSanKeystorePassword)
          .keyManagerPassword(serverWithoutSanKeyPassword)
          .dynamicHttpsPort();

      final SSLConnectionSocketFactory defaultSslSf = new SSLConnectionSocketFactory(
          SSLContexts.createDefault());

      final File trustStoreFile = new File(
          Resources.getResource("ssl/http-sink-connector-server.truststore.jks")
              .toURI());
      final String truststorePassword = "truststore_http-sink-connector-server-with-san";
      final SSLConnectionSocketFactory trustingSslSfNoHostnameVerification = new SSLConnectionSocketFactory(
          SSLContexts.custom()
              .loadTrustMaterial(trustStoreFile, truststorePassword.toCharArray())
              .build(), NoopHostnameVerifier.INSTANCE);

      final SSLConnectionSocketFactory trustingSslSfDefaultHostnameVerification = new SSLConnectionSocketFactory(
          SSLContexts.custom()
              .loadTrustMaterial(trustStoreFile, truststorePassword.toCharArray())
              .build(), new DefaultHostnameVerifier());

      final String typeJsonUtf8="application/json; charset=utf-8";
      final String typeJsonIso8859dot1="application/json; charset=ISO-8859-1";
      BiConsumer<WireMockServer, HttpSenderResult> verifySuccessUtf8 = createSuccessVerifier(typeJsonUtf8);
      BiConsumer<WireMockServer, HttpSenderResult> verifySuccessIso8859dot1 = createSuccessVerifier(typeJsonIso8859dot1);

      BiConsumer<WireMockServer, HttpSenderResult> verifyFailedSslHandshakeError = (server, result) -> {
        assertFalse(result.isSuccess());
        assertNull(result.getStatusLine());
        assertInstanceOf(SSLHandshakeException.class, result.getException());
      };

      BiConsumer<WireMockServer, HttpSenderResult> verifyFailedSslPeerVerification = (server, result) -> {
        assertFalse(result.isSuccess());
        assertNull(result.getStatusLine());
        assertInstanceOf(SSLPeerUnverifiedException.class, result.getException());
      };

      Map<String, String> staticHeaders = new HashMap<>();
      staticHeaders.put("Header-1","Value-1");
      staticHeaders.put("Header-2","Value-2");



      return Stream.of(
              Arguments.of(Named.of("HTTP, no Factory, ISO-8859-1", noSslConfig), null, verifySuccessIso8859dot1, false, false, staticHeaders, typeJsonIso8859dot1),
              Arguments.of(Named.of("HTTP, no Factory", noSslConfig), null, verifySuccessUtf8, false, false, staticHeaders, typeJsonUtf8),
              Arguments.of(Named.of("HTTP, no Factory", noSslConfig), null, verifySuccessUtf8, false, true, staticHeaders, typeJsonUtf8),
              Arguments.of(Named.of("HTTP, default factory", noSslConfig), defaultSslSf, verifySuccessUtf8, false, false, staticHeaders, typeJsonUtf8),
              Arguments.of(Named.of("HTTP, default factory, Basic Auth enabled", noSslConfig), defaultSslSf, verifySuccessUtf8, true, false, staticHeaders, typeJsonUtf8),
              Arguments.of(Named.of("HTTPS with SAN, default factory", sslWithSanConfig), defaultSslSf, verifyFailedSslHandshakeError, false, false, staticHeaders, typeJsonUtf8),
              Arguments.of(Named.of("HTTPS with SAN, custom factory, no hostname verification", sslWithSanConfig), trustingSslSfNoHostnameVerification, verifySuccessUtf8, false, false, staticHeaders, typeJsonUtf8),
              Arguments.of(Named.of("HTTPS with SAN, custom factory, with hostname verification", sslWithSanConfig), trustingSslSfDefaultHostnameVerification, verifySuccessUtf8, false, false, staticHeaders, typeJsonUtf8),
              Arguments.of(Named.of("HTTPS with SAN, custom factory, with hostname verification, Basic Auth enabled", sslWithSanConfig), trustingSslSfDefaultHostnameVerification, verifySuccessUtf8, true, false, staticHeaders, typeJsonUtf8),
              Arguments.of(Named.of("HTTPS without SAN, custom factory, no hostname verification", sslWithoutSanConfig), trustingSslSfNoHostnameVerification, verifySuccessUtf8, false, false, staticHeaders, typeJsonUtf8),
              Arguments.of(Named.of("HTTPS without SAN, custom factory, with hostname verification", sslWithoutSanConfig), trustingSslSfDefaultHostnameVerification, verifyFailedSslPeerVerification, false,false, staticHeaders, typeJsonUtf8)
      );
    } catch (URISyntaxException | NoSuchAlgorithmException | KeyStoreException | CertificateException | IOException | KeyManagementException e) {
      throw new HttpSinkConnectorException("Could not load resources for test", e);
    }
  }

  @ParameterizedTest
  @MethodSource("simpleCallTest")
  void simpleCallTest(
      WireMockConfiguration config,
      SSLConnectionSocketFactory sslConnectionSocketFactory,
      BiConsumer<WireMockServer, HttpSenderResult> consumer, boolean enableBasicAuth, boolean enableRedirect, Map<String,String> staticHeaders, String contentType) {
    ContentType realContentType = ContentType.parse(contentType);
    WireMockServer server = new WireMockServer(config);
    try {
      server.start();
      server.resetAll();
      final String path = EXPECTED_PATH;
      final String fullUrl = server.baseUrl() + (enableRedirect?REDIRECT_PATH:EXPECTED_PATH);
      final int statusCode = EXPECTED_STATUS_CODE;
      final String statusMessage = EXPECTED_STATUS_MESSAGE;

      MappingBuilder mappingBuilder = post(urlEqualTo(path)).willReturn(
          aResponse()
              .withStatus(statusCode)
              .withStatusMessage(statusMessage)
      );

      if (enableBasicAuth) {
        mappingBuilder.withBasicAuth(EXPECTED_BASIC_AUTH_USER, EXPECTED_BASIC_AUTH_PASS);
      }

      for(Map.Entry<String,String> header:staticHeaders.entrySet()){
        mappingBuilder.withHeader(header.getKey(), equalTo(header.getValue()));
      }

      server.stubFor(mappingBuilder);


      MappingBuilder redirectMappingBuilder = post(urlEqualTo(REDIRECT_PATH)).willReturn(
          aResponse().withStatus(307).withHeader("Location",path)
      );

      server.stubFor(redirectMappingBuilder);

      final String toSend = "Hello there";
      IMessageFormatter formatter = (request,contentTypeFmt) -> {
        request.setEntity(new StringEntity(toSend, realContentType));
        return request;
      };

      IAuthenticationProvider authenticationProvider;

      if (enableBasicAuth) {
        authenticationProvider = new BasicAuthenticationProvider();
        Map<String, Object> basicAuthConfig = new HashMap<>();
        basicAuthConfig.put(ENDPOINT_CONFIG, fullUrl);
        basicAuthConfig.put(USERNAME_CONFIG, EXPECTED_BASIC_AUTH_USER);
        basicAuthConfig.put(PASSWORD_CONFIG, EXPECTED_BASIC_AUTH_PASS);
        basicAuthConfig.put(ENABLE_PREEMPTIVE_AUTHENTICATION_CONFIG, Boolean.TRUE);
        authenticationProvider.configure(basicAuthConfig);
      } else {
        authenticationProvider = new NoopAuthenticationProvider();
      }
      IHeaderSelector headerSelector = selector -> selector;

      RequestConfig requestConfig = RequestConfig.DEFAULT;
      HttpSenderRetryStrategy retryStrategy = new HttpSenderRetryStrategy(
          Arrays.asList(EXPECTED_STATUS_CODE), 3, 100);

      HttpSenderConfiguration configuration = new HttpSenderConfiguration(
          authenticationProvider, formatter, headerSelector, fullUrl, "poSt",
          sslConnectionSocketFactory, requestConfig, retryStrategy, realContentType, staticHeaders,
          new ContentLogger(LoggerFactory.getLogger("HttpSinkIT-Content")));

      try (HttpSender sender = new HttpSender()) {
        sender.configure(configuration);

        HttpSenderResult result = sender.sendRecord(null);
        assertNotNull(result);
        if (consumer != null) {
          consumer.accept(server, result);
        }
      }


    } finally {
      server.shutdown();
    }
  }
}
