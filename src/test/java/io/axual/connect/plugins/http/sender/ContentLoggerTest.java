package io.axual.connect.plugins.http.sender;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

@ExtendWith(MockitoExtension.class)
class ContentLoggerTest {

  @Mock
  Logger mockedLogger;

  @Mock
  HttpUriRequest mockRequest;

  @Mock
  CloseableHttpResponse mockResponse;

  @Mock
  Throwable mockThrowable;

  @InjectMocks
  @Spy
  ContentLogger logger;

  @Test
  void logWithNullLogger() {
    final ContentLogger contentLogger = spy(new ContentLogger(null));

    contentLogger.log(mockRequest, mockResponse, mockThrowable);
    verify(contentLogger, times(0)).createNode(any(Throwable.class));
    verify(contentLogger, times(0)).createNode(any(HttpUriRequest.class));
    verify(contentLogger, times(0)).createNode(any(CloseableHttpResponse.class));
  }

  @Test
  void logAll() {
    final String contentRequest = "Req1";
    final String contentResponse = "Res1";
    final String contentException = "Thr1";

    doReturn(JsonNodeFactory.instance.textNode(contentRequest)).when(logger)
        .createNode(eq(mockRequest));
    doReturn(JsonNodeFactory.instance.textNode(contentResponse)).when(logger).createNode(eq(
        mockResponse));
    doReturn(JsonNodeFactory.instance.textNode(contentException)).when(logger).createNode(eq(
        mockThrowable));

    logger.log(mockRequest, mockResponse, mockThrowable);
    verify(mockedLogger, times(1)).debug(any());
  }

  @Test
  void logRequestResponse() {
    final String contentRequest = "Req1";
    final String contentResponse = "Res1";
    final String contentException = "Thr1";

    doReturn(JsonNodeFactory.instance.textNode(contentRequest)).when(logger)
        .createNode(eq(mockRequest));
    doReturn(JsonNodeFactory.instance.textNode(contentResponse)).when(logger).createNode(eq(
        mockResponse));
    doReturn(JsonNodeFactory.instance.textNode(contentException)).when(logger).createNode(
        (Throwable) isNull());

    logger.log(mockRequest, mockResponse);
    verify(mockedLogger, times(1)).debug(any());
  }

  @Test
  void logRequestThrowable() {
    final String contentRequest = "Req1";
    final String contentResponse = "Res1";
    final String contentException = "Thr1";

    doReturn(JsonNodeFactory.instance.textNode(contentRequest)).when(logger)
        .createNode(eq(mockRequest));
    doReturn(JsonNodeFactory.instance.textNode(contentResponse)).when(logger).createNode(
        (CloseableHttpResponse) isNull());
    doReturn(JsonNodeFactory.instance.textNode(contentException)).when(logger).createNode(eq(
        mockThrowable));

    logger.log(mockRequest, mockThrowable);
    verify(mockedLogger, times(1)).debug(any());
  }

  @Test
  void createRequestNode() {
    final HttpEntityEnclosingRequestBase mockRequest = mock(HttpEntityEnclosingRequestBase.class);

    final Header[] headers = new Header[0];
    doReturn(headers).when(mockRequest).getAllHeaders();
    final JsonNode headerNode = JsonNodeFactory.instance.numberNode(1);
    doReturn(headerNode).when(logger).createHeaders(eq(headers));

    final URI uri = URI.create("http://somewhere/");
    doReturn(uri).when(mockRequest).getURI();

    final String method = "POST IT";
    doReturn(method).when(mockRequest).getMethod();

    final HttpEntity mockEntity = mock(HttpEntity.class);
    doReturn(mockEntity).when(mockRequest).getEntity();
    final JsonNode entityNode = JsonNodeFactory.instance.numberNode(2);
    doReturn(entityNode).when(logger).createEntity(eq(mockEntity));

    final JsonNode requestNode = logger.createNode(mockRequest);

    assertTrue(requestNode instanceof ObjectNode);
    assertTrue(requestNode.has("method"));
    assertEquals(method, requestNode.get("method").textValue());
    assertTrue(requestNode.has("endpoint"));
    assertEquals(uri.toString(), requestNode.get("endpoint").textValue());
    assertTrue(requestNode.has("headers"));
    assertEquals(headerNode, requestNode.get("headers"));
    assertTrue(requestNode.has("content"));
    assertEquals(entityNode, requestNode.get("content"));

  }

  @Test
  void createRequestNode_NotHttpEntityEnclosingRequestBase() {
    final HttpUriRequest request = mock(HttpUriRequest.class);
    JsonNode requestNode = logger.createNode(request);
    assertTrue(requestNode instanceof TextNode);
    assertTrue(requestNode.textValue().startsWith("LOG-ERROR"));
  }

  @Test
  void createRequestNode_Null() {
    final HttpUriRequest request = null;
    JsonNode requestNode = logger.createNode(request);
    assertTrue(requestNode instanceof NullNode);
  }

  @Test
  void createResponseNode() {
    final CloseableHttpResponse mockResponse = mock(CloseableHttpResponse.class);

    final Header[] headers = new Header[0];
    doReturn(headers).when(mockResponse).getAllHeaders();

    final StatusLine mockStatusLine = mock(StatusLine.class);
    doReturn(mockStatusLine).when(mockResponse).getStatusLine();

    final HttpEntity mockEntity = mock(HttpEntity.class);
    doReturn(mockEntity).when(mockResponse).getEntity();

    final JsonNode statuslineNode = JsonNodeFactory.instance.numberNode(1);
    doReturn(statuslineNode).when(logger).createStatusLine(eq(mockStatusLine));

    final JsonNode headersNode = JsonNodeFactory.instance.numberNode(2);
    doReturn(headersNode).when(logger).createHeaders(eq(headers));

    final JsonNode entityNode = JsonNodeFactory.instance.numberNode(3);
    doReturn(entityNode).when(logger).createEntity(eq(mockEntity));

    JsonNode responseNode = logger.createNode(mockResponse);

    assertTrue(responseNode instanceof ObjectNode);
    assertEquals(statuslineNode, responseNode.get("status"));
    assertEquals(headersNode, responseNode.get("headers"));
    assertEquals(entityNode, responseNode.get("content"));
  }

  @Test
  void createResponseNode_Null() {
    final CloseableHttpResponse response = null;
    JsonNode responseNode = logger.createNode(response);

    assertTrue(responseNode instanceof NullNode);
  }

  @Test
  void createExceptionNode() {
    final String message3 = "msg3";
    final Exception ex3 = new NullPointerException(message3);
    final String message2 = "msg2";
    final Exception ex2 = new RuntimeException(message2, ex3);
    final String message1 = "msg1";
    final Exception ex1 = new Exception(message1, ex2);

    final JsonNode ex1Node = logger.createNode(ex1);

    assertTrue(ex1Node instanceof ObjectNode);
    assertEquals(ex1.getClass().getName(), ex1Node.get("type").textValue());
    assertEquals(message1, ex1Node.get("message").textValue());
    assertTrue(ex1Node.has("cause"));

    final JsonNode ex2Node = ex1Node.get("cause");
    assertTrue(ex2Node instanceof ObjectNode);
    assertEquals(ex2.getClass().getName(), ex2Node.get("type").textValue());
    assertEquals(message2, ex2Node.get("message").textValue());
    assertTrue(ex2Node.has("cause"));

    final JsonNode ex3Node = ex2Node.get("cause");
    assertTrue(ex3Node instanceof ObjectNode);
    assertEquals(ex3.getClass().getName(), ex3Node.get("type").textValue());
    assertEquals(message3, ex3Node.get("message").textValue());
    assertTrue(ex3Node.has("cause"));
    assertTrue(ex3Node.get("cause") instanceof NullNode);
  }

  @Test
  void createStatusLine() {
    final int statusCode = 123;
    final String reason = "Why Not";
    StatusLine statusLine = mock(StatusLine.class);
    doReturn(statusCode).when(statusLine).getStatusCode();
    doReturn(reason).when(statusLine).getReasonPhrase();
    final JsonNode statusNode = logger.createStatusLine(statusLine);

    assertTrue(statusNode instanceof ObjectNode);
    assertEquals(statusCode, statusNode.get("code").intValue());
    assertEquals(reason, statusNode.get("reason").textValue());
  }

  @Test
  void createStatusLine_null() {
    StatusLine statusLine = null;
    final JsonNode statusNode = logger.createStatusLine(statusLine);

    assertTrue(statusNode instanceof NullNode);
  }

  @Test
  void createEntity() {
    final String content = "Lets see this";
    final StringEntity entity = new StringEntity(content, StandardCharsets.UTF_8);

    final JsonNode entityNode = logger.createEntity(entity);

    assertTrue(entityNode instanceof TextNode);
    assertEquals(content, entityNode.textValue());
  }

  @Test
  void createEntity_Null() {
    final StringEntity entity = null;

    final JsonNode entityNode = logger.createEntity(entity);

    assertTrue(entityNode instanceof NullNode);
  }

  @Test
  void createEntity_Exception() throws IOException {
    final String exceptionMessage = "!!!Some Error!!!";
    final HttpEntity mockEntity = mock(HttpEntity.class);
    doThrow(new IOException(exceptionMessage)).when(mockEntity).getContent();

    final JsonNode entityNode = logger.createEntity(mockEntity);

    assertTrue(entityNode instanceof TextNode);
    assertTrue(entityNode.textValue().contains(exceptionMessage));
  }

  @Test
  void createHeaders() {
    final String name1 = "header1";
    final String value1 = "value1";
    final String name2 = "header2";
    final String value2 = "value2";
    final BasicHeader[] headers = new BasicHeader[]{new BasicHeader(name1, value1),
        new BasicHeader(name2, value2)};

    final JsonNode headerNode = logger.createHeaders(headers);

    assertTrue(headerNode instanceof ArrayNode);
    assertEquals(headers.length, headerNode.size());
    assertTrue(headerNode.get(0) instanceof ObjectNode);
    assertEquals(name1, headerNode.get(0).get("name").textValue());
    assertEquals(value1, headerNode.get(0).get("value").textValue());

    assertTrue(headerNode.get(1) instanceof ObjectNode);
    assertEquals(name2, headerNode.get(1).get("name").textValue());
    assertEquals(value2, headerNode.get(1).get("value").textValue());
  }


  @Test
  void createException_threeInList() {
    final String message3 = "msg3";
    final Exception ex3 = new NullPointerException(message3);
    final String message2 = "msg2";
    final Exception ex2 = new RuntimeException(message2, ex3);
    final String message1 = "msg1";
    final Exception ex1 = new Exception(message1, ex2);

    final Set<Throwable> processed = new HashSet<>();
    processed.add(ex3);
    final JsonNode ex1Node = logger.createException(ex1, processed);

    assertTrue(ex1Node instanceof ObjectNode);
    assertEquals(ex1.getClass().getName(), ex1Node.get("type").textValue());
    assertEquals(message1, ex1Node.get("message").textValue());
    assertTrue(ex1Node.has("cause"));

    final JsonNode ex2Node = ex1Node.get("cause");
    assertTrue(ex2Node instanceof ObjectNode);
    assertEquals(ex2.getClass().getName(), ex2Node.get("type").textValue());
    assertEquals(message2, ex2Node.get("message").textValue());
    assertTrue(ex2Node.has("cause"));

    final JsonNode ex3Node = ex2Node.get("cause");
    assertTrue(ex3Node instanceof NullNode);
  }

}