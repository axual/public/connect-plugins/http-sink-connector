package io.axual.connect.plugins.http.sender;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import org.apache.http.ProtocolVersion;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicStatusLine;
import org.apache.kafka.connect.sink.SinkRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HttpSenderTest {

  public static final String ENDPOINT = "http://SOMEWHERE";
  public static final String METHOD = "POST";

  @Mock
  HttpSenderConfiguration mockedConfig;

  @Mock
  IAuthenticationProvider mockedAuthenticationProvider;

  @Mock
  IMessageFormatter mockedMessageFormatter;

  @Mock
  IHeaderSelector mockedHeaderSelector;

  @Spy
  HttpClientBuilder mockedClientBuilder;

  @Spy
  HttpSender httpSender;

  @Test
  void configure_initial() {
    CloseableHttpClient mockedHttpClient = mock(CloseableHttpClient.class);
    doReturn(mockedHttpClient).when(mockedClientBuilder).build();
    doReturn(mockedAuthenticationProvider).when(mockedConfig).getAuthenticationProvider();
    doReturn(mockedClientBuilder).when(httpSender)
        .createClientBuilderWithAuthentication(eq(mockedAuthenticationProvider));

    httpSender.configure(mockedConfig);

    verify(mockedConfig, times(1)).getAuthenticationProvider();
    verify(httpSender, times(1))
        .createClientBuilderWithAuthentication(eq(mockedAuthenticationProvider));
  }

  @Test
  void configure_twice() {
    CloseableHttpClient mockedHttpClient = mock(CloseableHttpClient.class);
    doReturn(mockedHttpClient).when(mockedClientBuilder).build();
    doReturn(mockedAuthenticationProvider).when(mockedConfig).getAuthenticationProvider();
    doReturn(mockedClientBuilder).when(httpSender)
        .createClientBuilderWithAuthentication(eq(mockedAuthenticationProvider));

    httpSender.configure(mockedConfig);
    httpSender.configure(mockedConfig);

    verify(mockedConfig, times(1)).getAuthenticationProvider();
    verify(httpSender, times(1))
        .createClientBuilderWithAuthentication(eq(mockedAuthenticationProvider));
  }


  @Mock
  CloseableHttpClient mockedHttpClient;

  @Mock
  CloseableHttpResponse mockedResponse;

  @Test
  void sendRecord_success() throws IOException {
    BasicStatusLine statusLine = new BasicStatusLine(new ProtocolVersion("http", 1, 1), 200, "OK");

    doReturn(mockedHttpClient).when(mockedClientBuilder).build();
    doReturn(mockedResponse).when(mockedHttpClient)
        .execute(any(HttpUriRequest.class));
    doReturn(statusLine).when(mockedResponse).getStatusLine();

    HttpSenderResult result = sendRecord();
    assertNotNull(result);
    assertTrue(result.isSuccess());
    assertSame(statusLine, result.getStatusLine());
    assertNull(result.getException());
  }

  @Test
  void sendRecord_exception() throws IOException {
    IOException exception = new IOException();

    doReturn(mockedHttpClient).when(mockedClientBuilder).build();
    doThrow(exception).when(mockedHttpClient).execute(any(HttpUriRequest.class));

    HttpSenderResult result = sendRecord();
    assertNotNull(result);
    assertFalse(result.isSuccess());
    assertNull(result.getStatusLine());
    assertSame(exception, result.getException());
  }

  HttpSenderResult sendRecord() {
    doReturn(mockedAuthenticationProvider).when(mockedConfig).getAuthenticationProvider();
    doReturn(mockedClientBuilder).when(httpSender)
        .createClientBuilderWithAuthentication(eq(mockedAuthenticationProvider));
    doReturn(mockedHeaderSelector).when(mockedConfig).getHeaderSelector();
    doReturn(mockedMessageFormatter).when(mockedConfig).getMessageFormatter();

    doAnswer(invocation -> invocation.getArgument(0, HttpSenderRequest.class))
        .when(mockedMessageFormatter).formatMessage(any(HttpSenderRequest.class),any());
    doAnswer(invocation -> invocation.getArgument(0, HttpSenderRequest.class))
        .when(mockedHeaderSelector).selectHeaders(any(HttpSenderRequest.class));

    SinkRecord record = mock(SinkRecord.class);
    doReturn(ENDPOINT).when(mockedConfig).getEndpoint();
    doReturn(METHOD).when(mockedConfig).getHttpMethod();

    httpSender.configure(mockedConfig);
    HttpSenderResult result = httpSender.sendRecord(record);

    verify(mockedConfig, atLeast(1)).getAuthenticationProvider();
    verify(httpSender, times(1))
        .createClientBuilderWithAuthentication(eq(mockedAuthenticationProvider));

    verify(mockedHeaderSelector, times(1)).selectHeaders(any(HttpSenderRequest.class));
    verify(mockedMessageFormatter, times(1)).formatMessage(any(HttpSenderRequest.class),any());

    return result;
  }

  @Test
  void close() throws IOException {
    CloseableHttpClient mockedHttpClient = mock(CloseableHttpClient.class);
    doReturn(mockedHttpClient).when(mockedClientBuilder).build();

    doReturn(mockedAuthenticationProvider).when(mockedConfig).getAuthenticationProvider();
    doReturn(mockedClientBuilder).when(httpSender)
        .createClientBuilderWithAuthentication(eq(mockedAuthenticationProvider));

    httpSender.configure(mockedConfig);

    httpSender.close();

    verify(mockedHttpClient, times(1)).close();
  }

  @Test
  void createClientBuilderWithAuthentication() {
    doAnswer(invocation -> invocation.getArgument(0, HttpClientBuilder.class))
        .when(mockedAuthenticationProvider).addAuthentication(any(HttpClientBuilder.class));
    HttpClientBuilder builder = httpSender
        .createClientBuilderWithAuthentication(mockedAuthenticationProvider);

    assertNotNull(builder);

    verify(mockedAuthenticationProvider, times(1)).addAuthentication(any(HttpClientBuilder.class));
  }
}
