package io.axual.connect.plugins.http;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 - 2023 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.http.ParseException;
import org.apache.http.entity.ContentType;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigException;

import java.nio.charset.UnsupportedCharsetException;

/**
 * Configuration validator for Content Types.
 * <p>
 * This class will try to parse the value using the {@link ContentType#parse(String)} method, catch exceptions
 * and throw them as {@link ConfigException}
 */
public class ContentTypeValidator implements ConfigDef.Validator {
    @Override
    public void ensureValid(String configKey, Object value) {
        if (!(value instanceof String)) {
            throw new ConfigException(configKey, value, "Value must be of type string");
        }
        try {
            ContentType.parse((String) value);
        } catch (ParseException e) {
            throw new ConfigException(configKey, value, e.getMessage());
        } catch (UnsupportedCharsetException e) {
            throw new ConfigException(configKey, value, "Unsupported Charset: " + e.getCharsetName());
        }
    }
}
