package io.axual.connect.plugins.http.sender;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import org.apache.http.StatusLine;
import org.apache.kafka.connect.sink.SinkRecord;

/**
 * The result container for the {@link HttpSender#sendRecord(SinkRecord)}.<br>
 */
public class HttpSenderResult {

  private final boolean isSuccess;
  private final StatusLine statusLine;
  private final Exception exception;

  /**
   * <p>Created the container and sets the content.</p>
   * <p>The status line will be set if a response was received. If the status line is set and the
   * isSuccess is false then the status code received in the response did not match the codes set as
   * valid status codes.</p>
   *
   * @param isSuccess  True if the send request was considered a success
   * @param statusLine The status line of the response, can be null in case of an exception
   * @param exception  The exception thrown when sending the request, can be
   */
  public HttpSenderResult(boolean isSuccess, StatusLine statusLine, Exception exception) {
    this.isSuccess = isSuccess;
    this.statusLine = statusLine;
    this.exception = exception;
  }

  /**
   * Was the send request considered successful
   *
   * @return true if the request was send successfully
   */
  public boolean isSuccess() {
    return isSuccess;
  }

  /**
   * Get the status line received as response.
   *
   * @return the Apache Http Client StatusLine object
   */
  public StatusLine getStatusLine() {
    return statusLine;
  }

  /**
   * Get the exception thrown during the sending of the request.
   *
   * @return the exception thrown.
   */
  public Exception getException() {
    return exception;
  }
}
