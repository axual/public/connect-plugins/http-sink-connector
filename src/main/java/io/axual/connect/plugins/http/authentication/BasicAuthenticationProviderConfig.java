package io.axual.connect.plugins.http.authentication;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static org.apache.kafka.common.config.ConfigDef.NO_DEFAULT_VALUE;

import java.util.Map;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigDef.Importance;
import org.apache.kafka.common.config.ConfigDef.Type;
import org.apache.kafka.common.config.ConfigDef.Width;
import org.apache.kafka.common.config.types.Password;

/**
 * <p>Provides the configuration and validation definitions for the {@link BasicAuthenticationProvider}</p>
 *
 * <p>It extends the {@link AuthenticationProviderConfig} to get the reusable endpoint configuration options.</p>
 */
public class BasicAuthenticationProviderConfig extends AuthenticationProviderConfig {

  public static final String GROUP_NAME="BasicAuthentication";
  public static final String USERNAME_CONFIG = "username";
  public static final String USERNAME_DISPLAYNAME = "Username";
  public static final Object USERNAME_DEFAULT = NO_DEFAULT_VALUE;
  public static final String USERNAME_DOC = "The username to use when connecting to the target service";

  public static final String PASSWORD_CONFIG = "password";
  public static final String PASSWORD_DISPLAYNAME = "Password";
  public static final Object PASSWORD_DEFAULT = NO_DEFAULT_VALUE;
  public static final String PASSWORD_DOC = "The password to use when connecting to the target service";

  public static final String ENABLE_PREEMPTIVE_AUTHENTICATION_CONFIG = "preemptive.authentication.enabled";
  public static final String ENABLE_PREEMPTIVE_AUTHENTICATION_DISPLAYNAME = "Enable Preemptive Authentication";
  public static final Boolean ENABLE_PREEMPTIVE_AUTHENTICATION_DEFAULT = Boolean.FALSE;
  public static final String ENABLE_PREEMPTIVE_AUTHENTICATION_DOC = "Enabling preemptive authentication will result in always sending the credentials to the target.";

  private static final ConfigDef CONFIG_DEF = new ConfigDef()
      .define(USERNAME_CONFIG, Type.STRING,USERNAME_DEFAULT, Importance.HIGH,USERNAME_DOC,GROUP_NAME,0,
          Width.MEDIUM,USERNAME_DISPLAYNAME)
      .define(PASSWORD_CONFIG, Type.PASSWORD,PASSWORD_DEFAULT, Importance.HIGH,PASSWORD_DOC,GROUP_NAME,1,
          Width.MEDIUM,PASSWORD_DISPLAYNAME)
      .define(ENABLE_PREEMPTIVE_AUTHENTICATION_CONFIG, Type.BOOLEAN,ENABLE_PREEMPTIVE_AUTHENTICATION_DEFAULT, Importance.HIGH,ENABLE_PREEMPTIVE_AUTHENTICATION_DOC,GROUP_NAME,2,
          Width.SHORT,ENABLE_PREEMPTIVE_AUTHENTICATION_DISPLAYNAME);

  public BasicAuthenticationProviderConfig(
      Map<?, ?> originals) {
    super(CONFIG_DEF, originals);
  }

  /**
   * Returns the username to use for authentication
   * @see #USERNAME_CONFIG
   *
   * @return the configured username
   */
  public String getUsername(){
    return getString(USERNAME_CONFIG);
  }

  /**
   * Returns the password to use for authentication
   * @see #PASSWORD_CONFIG
   *
   * @return the configured password
   */
  public Password getPassword(){
    return getPassword(PASSWORD_CONFIG);
  }

  /**
   * Returns true if preemptive authentication should be enabled
   * @see #ENABLE_PREEMPTIVE_AUTHENTICATION_CONFIG
   *
   * @return true if preemptive authentication should be enabled
   */
  public boolean isPreemptiveAuthenticationEnabled(){
    return getBoolean(ENABLE_PREEMPTIVE_AUTHENTICATION_CONFIG);
  }
}
