package io.axual.connect.plugins.http.sender;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import org.apache.http.impl.client.HttpClientBuilder;

/**
 * Provides authentication logic for the HTTP Client. It extends
 * {@link IConfigurable} for optional configuration support.
 */
public interface IAuthenticationProvider extends IConfigurable {

  /**
   * Add the logic for authentication to the HTTP Client
   *
   * @param clientBuilder to add the required authentication settings and interceptors to the HTTP
   *                      Client
   * @return the clientBuilder received as a parameter
   */
  HttpClientBuilder addAuthentication(HttpClientBuilder clientBuilder);
}
