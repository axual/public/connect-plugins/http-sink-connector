package io.axual.connect.plugins.http.sender;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ContentLogger is used to log the HTTP Request and responses using a separate SLF4J Logger.
 * The logger will write the contents as a JSON string: <br>
 * <pre>{@code
 * {
 *   "request": {
 *     "method" : "POST",
 *     "endpoint" : "https://somewhere.com/service",
 *     "headers" : [ {
 *       "name" : "Custom request header 1",
 *       "value" : "hello",
 *     },
 *     {
 *       "name" : "Custom request header 2",
 *       "value" : "server",
 *     }
 *     ],
 *     "content" : "{\"topic\" : \"test-topic\",\"key\" : 12,\"value\" : \"Hello\"}"
 *   },
 *   "response": {
 *     "status" : {
 *       "code" : 204,
 *       "reason" : "OK - No content"
 *     },
 *     "headers" : [ {
 *       "name" : "Custom response header 1",
 *       "value" : "hello",
 *     },
 *     {
 *       "name" : "Custom response header 2",
 *       "value" : "client",
 *     }
 *     ],
 *     "content" : null
 *   },
 *   "exception": {
 *     "type" : "java.lang.Exception",
 *     "message" : "Something went wrong",
 *     "cause" : {
 *     "type" : "java.io.IOException",
 *     "message" : "Could not write to file",
 *     "cause" : null
 *     }
 *   }
 * }
 * }</pre>
 */
public class ContentLogger {

  private static final Logger LOG = LoggerFactory.getLogger(ContentLogger.class);
  private final Logger logger;
  private static final ObjectMapper MAPPER = new ObjectMapper();

  public ContentLogger(Logger logger) {
    this.logger = logger;
  }

  /**
   * Log only the request and response.
   *
   * @param request  The request object send to the server
   * @param response The response received from the server
   */
  public void log(HttpUriRequest request, CloseableHttpResponse response) {
    log(request, response, null);
  }

  /**
   * Log only the request and the exception that resulted from the request;
   *
   * @param request   The request object send to the server
   * @param throwable The exception data thrown when trying to send the request
   */
  public void log(HttpUriRequest request, Throwable throwable) {
    log(request, null, throwable);
  }

  /**
   * Log the request and response and exception data.
   *
   * @param request   The request object send to the server
   * @param response  The response received from the server
   * @param throwable The exception data thrown when trying to send the request
   */
  public void log(HttpUriRequest request, CloseableHttpResponse response, Throwable throwable) {
    if (logger == null) {
      return;
    }

    ObjectNode msgRoot = JsonNodeFactory.instance.objectNode();
    msgRoot.set("request", createNode(request));
    msgRoot.set("response", createNode(response));
    msgRoot.set("exception", createNode(throwable));

    try {
      String logMessage = MAPPER.writeValueAsString(msgRoot);
      logger.debug(logMessage);
    } catch (JsonProcessingException jpe) {
      LOG.info("Could not create content log message", jpe);
    }
  }

  /**
   * Create a JSON representation of the request object
   *
   * @param request The request send to the server
   * @return the JSON representation of the request
   */
  JsonNode createNode(HttpUriRequest request) {
    if (request == null) {
      return JsonNodeFactory.instance.nullNode();
    }

    if (request instanceof HttpEntityEnclosingRequestBase) {
      ObjectNode requestNode = JsonNodeFactory.instance.objectNode();
      HttpEntityEnclosingRequestBase requestBase = (HttpEntityEnclosingRequestBase) request;
      requestNode.set("method", JsonNodeFactory.instance.textNode(requestBase.getMethod()));
      requestNode
          .set("endpoint", JsonNodeFactory.instance.textNode(requestBase.getURI().toString()));
      requestNode.set("headers", createHeaders(requestBase.getAllHeaders()));
      requestNode.set("content", createEntity(requestBase.getEntity()));
      return requestNode;
    } else {
      return JsonNodeFactory.instance
          .textNode("LOG-ERROR: Log request did not implement HttpEntityEnclosingRequestBase");
    }
  }

  /**
   * Create a JSON representation of the response object
   *
   * @param response The response received from the server
   * @return the JSON representation of the response
   */
  JsonNode createNode(CloseableHttpResponse response) {
    if (response == null) {
      return JsonNodeFactory.instance.nullNode();
    }
    ObjectNode responseNode = JsonNodeFactory.instance.objectNode();

    responseNode.set("status", createStatusLine(response.getStatusLine()));
    responseNode.set("headers", createHeaders(response.getAllHeaders()));
    responseNode.set("content", createEntity(response.getEntity()));

    return responseNode;
  }

  /**
   * Create a JSON representation of the thrown exceptions
   *
   * @param throwable The exception which was thrown trying to send a request to the server
   * @return the JSON representation of the exception
   */
  JsonNode createNode(Throwable throwable) {
    return createException(throwable, new HashSet<>());
  }

  /**
   * Create a JSON representation of the status received from the server
   *
   * @param statusLine The status received to the server
   * @return the JSON representation of the status line
   */
  JsonNode createStatusLine(StatusLine statusLine) {
    if (statusLine == null) {
      return JsonNodeFactory.instance.nullNode();
    }
    ObjectNode statusNode = JsonNodeFactory.instance.objectNode();
    statusNode.set("code", JsonNodeFactory.instance.numberNode(statusLine.getStatusCode()));
    statusNode.set("reason", JsonNodeFactory.instance.textNode(statusLine.getReasonPhrase()));

    return statusNode;
  }

  /**
   * Create a JSON representation of the HttpEntity that was part of the request or response objects
   *
   * @param entity The Apache HTTP Entity that was part of the request or response
   * @return the JSON representation of the entity
   */
  JsonNode createEntity(HttpEntity entity) {
    if (entity == null) {
      return JsonNodeFactory.instance.nullNode();
    }
    try {
      final ContentType contentType = ContentType.getLenient(entity);
      final Charset charset =
          (contentType == null || contentType.getCharset() == null) ? StandardCharsets.UTF_8
              : contentType.getCharset();
      byte[] rawData = EntityUtils.toByteArray(entity);
      String contents = new String(rawData, charset);
      return JsonNodeFactory.instance.textNode(contents);
    } catch (IOException e) {
      LOG.info("Could not read entity content");
      return JsonNodeFactory.instance
          .textNode("LOG-ERROR: Could not read entity content, " + e.getMessage());
    }
  }

  /**
   * Create a JSON representation of the send or received HTTP headers
   *
   * @param headers The HTTP headers that are part of the request or response
   * @return the JSON representation of the HTTP header
   */
  JsonNode createHeaders(Header[] headers) {
    ArrayNode headersNode = JsonNodeFactory.instance.arrayNode();
    for (Header header : headers) {
      ObjectNode headerNode = JsonNodeFactory.instance.objectNode();
      headerNode.set("name", JsonNodeFactory.instance.textNode(header.getName()));
      headerNode.set("value", JsonNodeFactory.instance.textNode(header.getValue()));
      headersNode.add(headerNode);
    }

    return headersNode;
  }

  /**
   * Create a JSON representation of the thrown exceptions. It will follow the cause of each until
   * the cause is found in the processedCauses list. This should prevent infinite looping when
   * causes are chained in a circular fashion
   *
   * @param throwable The exception which was thrown trying to send a request to the server
   * @param processedCauses The list of already processed causes
   * @return the JSON representation of the exception
   */
  JsonNode createException(Throwable throwable, Set<Throwable> processedCauses) {
    if (throwable == null) {
      return JsonNodeFactory.instance.nullNode();
    }
    ObjectNode throwableNode = JsonNodeFactory.instance.objectNode();
    throwableNode.set("type", JsonNodeFactory.instance.textNode(throwable.getClass().getName()));
    throwableNode.set("message", JsonNodeFactory.instance.textNode(throwable.getMessage()));

    Throwable cause = throwable.getCause();
    if (processedCauses.contains(cause)) {
      throwableNode.set("cause", JsonNodeFactory.instance.nullNode());
    } else {
      processedCauses.add(cause);
      throwableNode.set("cause", createException(cause, processedCauses));
    }
    return throwableNode;
  }

}
