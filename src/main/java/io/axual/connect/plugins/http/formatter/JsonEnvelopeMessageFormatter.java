package io.axual.connect.plugins.http.formatter;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.axual.connect.plugins.http.exceptions.MessageFormattingException;
import io.axual.connect.plugins.http.sender.HttpSenderRequest;
import io.axual.connect.plugins.http.sender.IMessageFormatter;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Map;
import java.util.function.BiFunction;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.kafka.connect.data.Field;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.data.Schema.Type;
import org.apache.kafka.connect.data.Struct;
import org.apache.kafka.connect.data.Values;
import org.apache.kafka.connect.sink.SinkRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>This message formatter will use the contents of the SinkRecord and transform it into a
 * JSON message.
 * </p>
 *
 * <p>To use the JSON Envelope Message Formatter the setting {@link io.axual.connect.plugins.http.HttpSinkConnectorConfig#MESSAGE_FORMATTER_CLASS_CONFIG}
 * must be set to this class. <br>
 * The SON Envelope Message Formatter doesn't take any arguments</p>
 */
public class JsonEnvelopeMessageFormatter implements IMessageFormatter {

  private static final Logger LOG = LoggerFactory.getLogger(JsonEnvelopeMessageFormatter.class);

  private final ObjectMapper objectMapper;
  private final JsonNodeFactory jsonNodeFactory;

  JsonEnvelopeMessageFormatter(ObjectMapper objectMapper,
      JsonNodeFactory jsonNodeFactory) {
    this.objectMapper = objectMapper;
    this.jsonNodeFactory = jsonNodeFactory;
  }

  /**
   * Default constructor required for instantiation by the HTTP Sink Connector
   */
  public JsonEnvelopeMessageFormatter() {
    this.objectMapper = new ObjectMapper();
    this.jsonNodeFactory = JsonNodeFactory.instance;
  }


  @Override
  public HttpSenderRequest formatMessage(HttpSenderRequest request, ContentType contentType) {
    LOG.debug("Formatting message");
    request.setEntity(createJsonMessage(request.getSinkRecord(), contentType));
    return request;
  }

  /**
   * Creates the Apache StringEntity containing the Json representation of the Kafka Connect Record
   * @param record The record to transform to Json
   * @param contentType The HTTP Content-Type to use for the entity
   * @return A StringEntity containing the record data transformed to Json
   */
  public StringEntity createJsonMessage(SinkRecord record, ContentType contentType) {
    LOG.debug("Creating JSON Message");
    try {
      ObjectNode rootNode = jsonNodeFactory.objectNode();
      rootNode.set("topic", jsonNodeFactory.textNode(record.topic()));
      LOG.debug("Creating key node");
      rootNode.set("key", createNode(record.keySchema(), record.key()));
      LOG.debug("Creating value node");
      rootNode.set("value", createNode(record.valueSchema(), record.value()));
      String message = objectMapper.writeValueAsString(rootNode);
      return new StringEntity(message, contentType);
    } catch (JsonProcessingException e) {
      throw new MessageFormattingException("Could not serialize message", e);
    }
  }

  /**
   * Creates the Json representation based on the provided {@link Schema}
   * @param schema The schema for the value object
   * @param value The value which needs to be transformed to Json
   * @return The Json representation of the provided value
   */
  public JsonNode createNode(Schema schema, Object value) {
    LOG.debug("Creating node");
    if (schema == null) {
      return createSchemalessNode(value);
    }
    if (value == null) {
      return jsonNodeFactory.nullNode();
    }

    LOG.debug("Type is {}", schema.type());
    switch (schema.type()) {
      case INT8:
      case INT16:
      case INT32:
      case INT64:
        Long longValue = Values.convertToLong(schema, value);
        return jsonNodeFactory.numberNode(longValue);
      case FLOAT32:
        Float floatValue = Values.convertToFloat(schema, value);
        return jsonNodeFactory.numberNode(floatValue);
      case FLOAT64:
        Double doubleValue = Values.convertToDouble(schema, value);
        return jsonNodeFactory.numberNode(doubleValue);
      case BOOLEAN:
        Boolean boolValue = Values.convertToBoolean(schema, value);
        return jsonNodeFactory.booleanNode(boolValue.booleanValue());
      case STRING:
        String stringValue = Values.convertToString(schema, value);
        return jsonNodeFactory.textNode(stringValue);
      case BYTES:
        if (value instanceof byte[]) {
          return jsonNodeFactory.binaryNode((byte[]) value);
        } else if (value instanceof ByteBuffer) {
          return jsonNodeFactory.binaryNode(((ByteBuffer) value).array());
        } else {
          throw new MessageFormattingException("Invalid type for bytes type: " + value.getClass());
        }
      case ARRAY:
        Collection<Object> values = (Collection) value;
        ArrayNode nodes = jsonNodeFactory.arrayNode();
        for (Object element : values) {
          LOG.debug("Creating array element node");
          nodes.add(createNode(schema.valueSchema(), element));
        }
        return nodes;
      case MAP:
        Map<?, ?> map = (Map<?, ?>) value;

        // Connect Maps can have complex keys, this will result in object mode, which is an array of a complex node with a key and value field
        boolean useStringKeys = schema.keySchema().type() == Type.STRING;

        final JsonNode mapNode =
            useStringKeys ? jsonNodeFactory.objectNode() : jsonNodeFactory.arrayNode();
        final BiFunction<?, JsonNode, JsonNode> add;

        for (Map.Entry<?, ?> entry : map.entrySet()) {
          LOG.debug("Creating Map entry with stringKey = {}", useStringKeys);
          JsonNode keyNode = createNode(schema.keySchema(), entry.getKey());
          JsonNode valueNode = createNode(schema.valueSchema(), entry.getValue());

          if (useStringKeys) {
            ((ObjectNode) mapNode).set(keyNode.asText(), valueNode);
          } else {
            ObjectNode wrapper = jsonNodeFactory.objectNode();
            wrapper.set("key", keyNode);
            wrapper.set("value", valueNode);
            ((ArrayNode) mapNode).add(wrapper);
          }
        }
        return mapNode;
      case STRUCT:
        Struct struct = (Struct) value;
        if (!schema.equals(struct.schema())) {
          throw new MessageFormattingException(
              "The provided schema does not match the struct schema");
        }

        ObjectNode structNode = jsonNodeFactory.objectNode();
        for (Field field : schema.fields()) {
          LOG.debug("Creating field for struct");
          structNode.set(field.name(), createNode(field.schema(), struct.get(field)));
        }
        return structNode;
      default:
        // Should be unreachable unless new Schema Type is defined
        throw new MessageFormattingException(
            "Unknown schema type received, type =" + schema.type());
    }
  }

  /**
   * Creates Json nodes from Java Objects when no {@link Schema} is available.
   *
   * @param value The value to transform to Json
   * @return The Json representation of the value.
   */
  public JsonNode createSchemalessNode(Object value) {
    LOG.debug("Creating schemaless node");
    if (value == null) {
      return jsonNodeFactory.nullNode();
    } else {
      if (value instanceof CharSequence) {
        return jsonNodeFactory.textNode(value.toString());
      } else if (value instanceof byte[]) {
        return jsonNodeFactory.binaryNode((byte[]) value);
      } else if (value instanceof ByteBuffer) {
        return jsonNodeFactory.binaryNode(((ByteBuffer) value).array());
      } else if (value instanceof Double) {
        return jsonNodeFactory.numberNode((Double) value);
      } else if (value instanceof Float) {
        return jsonNodeFactory.numberNode((Float) value);
      } else if (value instanceof Number) {
        return jsonNodeFactory.numberNode(((Number) value).longValue());
      } else {
        return jsonNodeFactory.textNode(value.toString());
      }
    }

  }
}
