package io.axual.connect.plugins.http.authentication;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import io.axual.connect.plugins.http.sender.IAuthenticationProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>The default Authentication Provider which does not modify the HTTP client.</p>
 */
public class NoopAuthenticationProvider implements IAuthenticationProvider {
  private static final Logger LOG = LoggerFactory.getLogger(NoopAuthenticationProvider.class);

  @Override
  public HttpClientBuilder addAuthentication(HttpClientBuilder clientBuilder) {
    LOG.debug("Adding authentication");
    return clientBuilder;
  }
}
