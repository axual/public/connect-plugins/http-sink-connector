package io.axual.connect.plugins.http.sender;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import java.util.LinkedList;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.kafka.connect.sink.SinkRecord;

/**
 * The HttpSenderRequest is used by the {@link HttpSender} to create the content and headers of the
 * request being send to the HTTP Service.<br> It is used as input for {@link
 * IMessageFormatter#formatMessage(HttpSenderRequest, ContentType)} and {@link
 * IHeaderSelector#selectHeaders(HttpSenderRequest)}.
 */
public class HttpSenderRequest {

  private final SinkRecord sinkRecord;
  private final List<Header> selectedHeaders;
  private HttpEntity entity;

  /**
   * Constructs the request based on the provided {@code SinkRecord}
   *
   * @param sinkRecord the record which needs to be transformed into the Rest Request
   */
  public HttpSenderRequest(SinkRecord sinkRecord) {
    this.sinkRecord = sinkRecord;
    this.selectedHeaders = new LinkedList<>();
  }

  /**
   * Returns the SinkRecord that needs to be transformed
   *
   * @return the sink record
   */
  public SinkRecord getSinkRecord() {
    return sinkRecord;
  }

  /**
   * Get the list of selected HTTP headers to send as part of the HTTP Request
   *
   * @return a list of HTTP headers
   */
  public List<Header> getSelectedHeaders() {
    return selectedHeaders;
  }

  /**
   * Get the HTTP Entity that was created
   *
   * @return the HTTP Entity
   */
  public HttpEntity getEntity() {
    return entity;
  }

  /**
   * Set the HTTP Entity to be send as the body of the HTTP Request
   *
   * @param entity the HTTP Entity to send
   */
  public void setEntity(HttpEntity entity) {
    this.entity = entity;
  }
}
