package io.axual.connect.plugins.http.sender;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static java.util.Objects.requireNonNull;

import java.util.Map;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;

/**
 * This class is a simple configuration container containing the initialized resources required by
 * the {@link HttpSender}
 */
public class HttpSenderConfiguration {

  private final IAuthenticationProvider authenticationProvider;
  private final IMessageFormatter messageFormatter;
  private final IHeaderSelector headerSelector;
  private final String endpoint;
  private final String httpMethod;

  private final SSLConnectionSocketFactory sslConnectionSocketFactory;
  private final RequestConfig requestConfig;
  private final HttpSenderRetryStrategy retryStrategy;
  private final ContentType contentType;
  private final Map<String, String> staticHeaders;
  private final ContentLogger contentLogger;

  /**
   * <p>Constructor supplying all the resources.</p>
   * <p>None of the parameters can be null</p>
   *
   * @param authenticationProvider     the authentication provider to use. Can not be null
   * @param messageFormatter           the message formatter to use. Can not be null
   * @param headerSelector             the header selector to use. Can not be null
   * @param endpoint                   the endpoint of the HTTP service to cal. Can not be null
   * @param httpMethod                 the HTTP Method to use when calling the target HTTP service.
   *                                   Can not be null
   * @param sslConnectionSocketFactory the Apache Http Client SSLConnectionSocketFactory to use. Can
   *                                   not be null
   * @param requestConfig              The Apache Http Client Request config containing the default
   *                                   configuration settings. Can not be null
   * @param retryStrategy              The initialized {@link HttpSenderRetryStrategy} to use. Can
   *                                   not be null
   * @param contentType                The Content Type to use for the HTTP requests. Can not be
   *                                   null
   * @param staticHeaders              The map containing the header name/values to use. Can not be
   *                                   null, but is allowed to be empty
   * @param contentLogger              The {@link ContentLogger} to use for request/response
   *                                   logging. Can not be null
   */
  public HttpSenderConfiguration(
      IAuthenticationProvider authenticationProvider,
      IMessageFormatter messageFormatter,
      IHeaderSelector headerSelector, String endpoint, String httpMethod,
      SSLConnectionSocketFactory sslConnectionSocketFactory,
      RequestConfig requestConfig,
      HttpSenderRetryStrategy retryStrategy, ContentType contentType,
      Map<String, String> staticHeaders, ContentLogger contentLogger) {
    this.authenticationProvider = requireNonNull(authenticationProvider);
    this.messageFormatter = requireNonNull(messageFormatter);
    this.headerSelector = requireNonNull(headerSelector);
    this.endpoint = requireNonNull(endpoint);
    this.httpMethod = requireNonNull(httpMethod);
    this.sslConnectionSocketFactory = sslConnectionSocketFactory;
    this.requestConfig = requireNonNull(requestConfig);
    this.retryStrategy = requireNonNull(retryStrategy);
    this.contentType = requireNonNull(contentType);
    this.staticHeaders = requireNonNull(staticHeaders);
    this.contentLogger = requireNonNull(contentLogger);
  }

  /**
   * Get the Authentication Provider
   *
   * @return the Authentication Provider
   */
  public IAuthenticationProvider getAuthenticationProvider() {
    return authenticationProvider;
  }

  /**
   * Get the Message Formatter
   *
   * @return the Message Formatter
   */
  public IMessageFormatter getMessageFormatter() {
    return messageFormatter;
  }

  /**
   * Get the Header Selector
   *
   * @return the header selector
   */
  public IHeaderSelector getHeaderSelector() {
    return headerSelector;
  }

  /**
   * Get the HTTP service endpoint
   *
   * @return the endpoint
   */
  public String getEndpoint() {
    return endpoint;
  }

  /**
   * Get the HTTP method to use
   *
   * @return the HTTP Method
   */
  public String getHttpMethod() {
    return httpMethod;
  }

  /**
   * Get the Apache Http Client SSL Connection Socket Factory
   *
   * @return the SSL Connection Socket Factory
   */
  public SSLConnectionSocketFactory getSslConnectionSocketFactory() {
    return sslConnectionSocketFactory;
  }

  /**
   * Get the Apache Http Client Request Config
   *
   * @return the Request Config
   */
  public RequestConfig getRequestConfig() {
    return requestConfig;
  }

  /**
   * Get the Retry Strategy
   *
   * @return the Retry Strategy
   */
  public HttpSenderRetryStrategy getRetryStrategy() {
    return retryStrategy;
  }

  /**
   * Get the Apache Http Client Content Type
   *
   * @return the Content Type
   */
  public ContentType getContentType() {
    return contentType;
  }

  /**
   * Get the map containing the static header names and values
   *
   * @return the header map
   */
  public Map<String, String> getStaticHeaders() {
    return staticHeaders;
  }

  /**
   * Get the Content Logger
   * @return the Content Logger
   */
  public ContentLogger getContentLogger() {
    return contentLogger;
  }
}
