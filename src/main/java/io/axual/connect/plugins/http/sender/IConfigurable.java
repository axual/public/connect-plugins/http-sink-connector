package io.axual.connect.plugins.http.sender;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Map;
import org.apache.kafka.common.config.ConfigDef;

/**
 * Defines the basic configuration methods used to get configuration options and configure the
 * implementations
 */
public interface IConfigurable {

  /**
   * Return a configuration definition object for the implementing class. The implementation can use
   * the configuration properties to enrich the definition
   *
   * @param configs The configuration to be used for possible enrichment
   * @return The configuration definition for this implementation
   */
  default ConfigDef configDefinition(Map<String, ?> configs) {
    return new ConfigDef();
  }


  /**
   * Configure this class with the given key-value pairs
   *
   * @param configs The configuration properties to use
   */
  default void configure(Map<String, ?> configs) {
    // Do nothing
  }

}
