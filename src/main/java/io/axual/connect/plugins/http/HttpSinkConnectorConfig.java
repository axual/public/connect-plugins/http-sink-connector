package io.axual.connect.plugins.http;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static org.apache.kafka.common.config.ConfigDef.Importance;
import static org.apache.kafka.common.config.ConfigDef.NO_DEFAULT_VALUE;
import static org.apache.kafka.common.config.ConfigDef.Type;

import io.axual.connect.plugins.http.authentication.NoopAuthenticationProvider;
import io.axual.connect.plugins.http.formatter.JsonEnvelopeMessageFormatter;
import io.axual.connect.plugins.http.headerselection.BasicHeaderSelector;
import io.axual.connect.plugins.http.helpers.ClassLoadHelper;
import io.axual.connect.plugins.http.sender.IAuthenticationProvider;
import io.axual.connect.plugins.http.sender.IConfigurable;
import io.axual.connect.plugins.http.sender.IHeaderSelector;
import io.axual.connect.plugins.http.sender.IMessageFormatter;

import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import javax.net.ssl.SSLContext;
import org.apache.http.ssl.SSLContexts;
import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigDef.ConfigKey;
import org.apache.kafka.common.config.ConfigDef.Range;
import org.apache.kafka.common.config.ConfigDef.Validator;
import org.apache.kafka.common.config.ConfigDef.Width;
import org.apache.kafka.common.config.ConfigException;

/**
 * <p>Provides the configuration and validation definitions for the {@link HttpSinkConnector}.</p>
 * <p>This class creates the configuration definition and validation dynamically because of the
 * pluggable nature of the Connector.<br> It will use the provided configuration properties to load
 * the defined {@link IAuthenticationProvider}, {@link IMessageFormatter} and {@link
 * IHeaderSelector} to load their configDefinition using {@link IConfigurable#configDefinition(Map)}</p>
 */
public class HttpSinkConnectorConfig extends AbstractConfig {

  public static final String HTTP_GROUP = "HTTP";
  public static final String SSL_GROUP = "TLS/SSL";
  public static final String CONNECTION_GROUP = "Connection";
  public static final String RETRY_GROUP = "Retry";
  public static final String CONTENT_GROUP = "Content";
  public static final String STATIC_HEADER_GROUP = CONTENT_GROUP + ".StaticHeaders";
  public static final String LOGGING_GROUP = "Logging";

  // HTTP Group settings
  public static final String ENDPOINT_CONFIG = "endpoint";
  public static final String ENDPOINT_DISPLAYNAME = "Endpoint";
  public static final String ENDPOINT_DOC = "The URI where the connector should try to send the data";
  public static final ConfigKey ENDPOINT_CONFIG_KEY = new ConfigKey(ENDPOINT_CONFIG, Type.STRING,
      NO_DEFAULT_VALUE, null, Importance.HIGH, ENDPOINT_DOC, HTTP_GROUP, 0, Width.LONG,
      ENDPOINT_DISPLAYNAME, Collections.emptyList(), null, false);


  public static final String METHOD_CONFIG = "method";
  private static final String METHOD_DEFAULT = "POST";
  public static final String METHOD_DISPLAYNAME = "Method";
  public static final String METHOD_DOC = "The HTTP Method used for delivering the data. Defaults to POST";

  public static final String VALID_STATUS_CODES_CONFIG = "valid.status.codes";
  private static final List<String> VALID_STATUS_CODES_DEFAULT = Arrays.asList("200", "204");
  public static final String VALID_STATUS_CODES_DISPLAYNAME = "Valid HTTP Status Codes";
  public static final String VALID_STATUS_CODES_DOC = "A list with the HTTP Response status codes indicating success";

  // CONTENT GROUP SETTINGS
  public static final String MESSAGE_FORMATTER_CLASS_CONFIG = "message.formatter.class";
  private static final String MESSAGE_FORMATTER_CLASS_DEFAULT = JsonEnvelopeMessageFormatter.class
      .getCanonicalName();
  public static final String MESSAGE_FORMATTER_CLASS_DISPLAYNAME = "Message Formatter Class";
  public static final String MESSAGE_FORMATTER_CLASS_DOC = "The name of the class to format the message. This class should implementi the interface io.axual.connect.plugins.http.sender.IMessageFormatter";
  public static final String MESSAGE_FORMATTER_CLASS_PARAM_PREFIX = "message.formatter.param.";
  public static final String MESSAGE_FORMATTER_CLASS_GROUP_PREFIX = CONTENT_GROUP + ".Formatting.";

  public static final String HEADER_SELECTOR_CLASS_CONFIG = "header.selector.class";
  private static final String HEADER_SELECTOR_CLASS_DEFAULT = BasicHeaderSelector.class
      .getCanonicalName();
  public static final String HEADER_SELECTOR_CLASS_DISPLAYNAME = "Header Selector Class";
  public static final String HEADER_SELECTOR_CLASS_DOC = "The name of the class to select the headers to forward. This class should implementi the interface io.axual.connect.plugins.http.sender.IHeaderSelector";
  public static final String HEADER_SELECTOR_CLASS_PARAM_PREFIX = "header.selector.param.";
  public static final String HEADER_SELECTOR_CLASS_GROUP_PREFIX = CONTENT_GROUP + ".Headers.";

  public static final String AUTHENTICATION_PROVIDER_CLASS_CONFIG = "authentication.provider.class";
  private static final String AUTHENTICATION_PROVIDER_CLASS_DEFAULT = NoopAuthenticationProvider.class
      .getCanonicalName();
  public static final String AUTHENTICATION_PROVIDER_CLASS_DISPLAYNAME = "Authentication Provider Class";
  public static final String AUTHENTICATION_PROVIDER_CLASS_DOC = "The name of the class to perform authentication. This class should implementi the interface io.axual.connect.plugins.http.sender.IAuthenticationProvider";
  public static final String AUTHENTICATION_PROVIDER_CLASS_PARAM_PREFIX = "authentication.provider.param.";
  public static final String AUTHENTICATION_PROVIDER_CLASS_GROUP_PREFIX =
      CONTENT_GROUP + ".Authentication.";

  public static final String CONTENT_TYPE_CONFIG = "content.type";
  private static final String CONTENT_TYPE_DEFAULT = "application/json; charset=utf-8";
  public static final String CONTENT_TYPE_DISPLAYNAME = "Content Type";
  public static final String CONTENT_TYPE_DOC = "The value of tht Content-Type header, this can be just a MimeType like 'application/json' or include a standard Java Charset as name like `application/json; charset=utf-8`";

  public static final String STATIC_HEADER_ALIAS_CONFIG = "header.static.aliases";
  public static final List<String> STATIC_HEADER_ALIAS_DEFAULT = Collections.emptyList();
  public static final String STATIC_HEADER_ALIAS_DISPLAYNAME = "Static Header Aliases";
  public static final String STATIC_HEADER_ALIAS_DOC = "A list of aliases which contain static headers";

  public static final String STATIC_HEADER_NAME_CONFIG_FORMAT = "header.static.%s.name";
  private static final Object STATIC_HEADER_NAME_DEFAULT = NO_DEFAULT_VALUE;
  public static final String STATIC_HEADER_NAME_DISPLAYNAME_FORMAT = "Static Header Name for %s";
  public static final String STATIC_HEADER_NAME_DOC_FORMAT = "The name of the %s header as used in the HTTP request";

  public static final String STATIC_HEADER_VALUE_CONFIG_FORMAT = "header.static.%s.value";
  private static final Object STATIC_HEADER_VALUE_DEFAULT = NO_DEFAULT_VALUE;
  public static final String STATIC_HEADER_VALUE_DISPLAYNAME_FORMAT = "Static Header Value for %s";
  public static final String STATIC_HEADER_VALUE_DOC_FORMAT = "The name of the %s header as used in the HTTP request";


  // Connection Group Settings
  public static final String CONNECTION_TIMEOUT_MS_CONFIG = "connection.timeout.ms";
  private static final Integer CONNECTION_TIMEOUT_MS_DEFAULT = 5000;
  public static final String CONNECTION_TIMEOUT_MS_DISPLAYNAME = "Connection Timeout (ms)";
  public static final String CONNECTION_TIMEOUT_MS_DOC = "The connection timeout in milliseconds. This the the maximum amount of time to wait for a connection to be established";

  public static final String CONNECTION_REQUEST_TIMEOUT_MS_CONFIG = "connection.request.timeout.ms";
  private static final Integer CONNECTION_REQUEST_TIMEOUT_MS_DEFAULT = 2000;
  public static final String CONNECTION_REQUEST_TIMEOUT_MS_DISPLAYNAME = "Connection Timeout (ms)";
  public static final String CONNECTION_REQUEST_TIMEOUT_MS_DOC = "The connection request timeout in milliseconds. This the the maximum amount of time to wait for a response from the connection manager when requesting a connection";

  public static final String SOCKET_TIMEOUT_MS_CONFIG = "socket.timeout.ms";
  private static final Integer SOCKET_TIMEOUT_MS_DEFAULT = 1000;
  public static final String SOCKET_TIMEOUT_MS_DISPLAYNAME = "Socket Timeout (ms)";
  public static final String SOCKET_TIMEOUT_MS_DOC = "The socket timeout in milliseconds. This the the maximum amount of time to wait between receiving data packets";

  public static final String REDIRECTS_ALLOW_CONFIG = "redirect.allow";
  private static final Boolean REDIRECTS_ALLOW_DEFAULT = Boolean.TRUE;
  public static final String REDIRECTS_ALLOW_DISPLAYNAME = "Allow Redirects";
  public static final String REDIRECTS_ALLOW_DOC = "Controls redirect behaviour. When true, redirects are followed";

  public static final String REDIRECTS_ALLOW_CIRCULAR_CONFIG = "redirect.allowcircular";
  private static final Boolean REDIRECTS_ALLOW_CIRCULAR_DEFAULT = Boolean.FALSE;
  public static final String REDIRECTS_ALLOW_CIRCULAR_DISPLAYNAME = "Allow Circular Redirects";
  public static final String REDIRECTS_ALLOW_CIRCULAR_DOC = "When true, circular redirects are allowed";

  public static final String REDIRECTS_MAXIMUM_CONFIG = "redirect.maximum";
  private static final Integer REDIRECTS_MAXIMUM_DEFAULT = 50;
  public static final String REDIRECTS_MAXIMUM_DISPLAYNAME = "Maximum Redirects";
  public static final String REDIRECTS_MAXIMUM_DOC = "The maximum amount of redirects to follow";

  // Retry settings
  public static final String RETRY_MAXIMUM_CONFIG = "retry.maximum";
  private static final Integer RETRY_MAXIMUM_DEFAULT = 3;
  public static final String RETRY_MAXIMUM_DISPLAYNAME = "Retries";
  public static final String RETRY_MAXIMUM_DOC = "The maximum amount of retries before failing. Wrong status codes count as failures as well";

  public static final String RETRY_WAIT_MS_CONFIG = "retry.wait.ms";
  private static final Integer RETRY_WAIT_MS_DEFAULT = 50;
  public static final String RETRY_WAIT_MS_DISPLAYNAME = "Retry wait (ms)";
  public static final String RETRY_WAIT_MS_DOC = "Wait this long before retrying when invalid status codes occur.";

  // SSL Group settings
  public static final String SSL_CONFIG_PREFIX = "ssl.";
  public static final String SSL_ENABLE_HOSTNAME_VERIFICATION_CONFIG =
      SSL_CONFIG_PREFIX + "enable.hostname.verification";
  private static final boolean SSL_ENABLE_HOSTNAME_VERIFICATION_DEFAULT = true;
  public static final String SSL_ENABLE_HOSTNAME_VERIFICATION_DISPLAYNAME = "Verify hostname";
  public static final String SSL_ENABLE_HOSTNAME_VERIFICATION_DOC = "The hostname used in the endpoint is matched against the names stored in the provided certificate when set to true.";

  public static final String SSL_CERTIFICATE_AUTHORITY_FILE_LOCATION_CONFIG =
      SSL_CONFIG_PREFIX + "ca.file.locations";
  public static final String SSL_CERTIFICATE_AUTHORITY_FILE_LOCATION_DISPLAYNAME = "Certificate Authority File Location";
  private static final List<String> SSL_CERTIFICATE_AUTHORITY_FILE_LOCATION_DEFAULT = Collections
      .emptyList();
  public static final String SSL_CERTIFICATE_AUTHORITY_FILE_LOCATION_DOC = "The locations of the PEM files containing the valid X509 Certificates for the trusted Certificate Authorities";

  public static final String SSL_CERTIFICATE_AUTHORITY_CERTIFICATES_CONFIG =
      SSL_CONFIG_PREFIX + "ca.certificates";
  public static final String SSL_CERTIFICATE_AUTHORITY_CERTIFICATES_DISPLAYNAME = "Certificate Authority Certificates";
  private static final List<String> SSL_CERTIFICATE_AUTHORITY_CERTIFICATES_DEFAULT = Collections
      .emptyList();
  public static final String SSL_CERTIFICATE_AUTHORITY_CERTIFICATES_DOC = "A comma separated list containing the PEM encoded X509 Certificates for the trusted Certificate Authorities";

  public static final String SSL_PROTOCOL_CONFIG =
      SSL_CONFIG_PREFIX + "protocols";
  public static final String SSL_PROTOCOL_DISPLAYNAME = "SSL/TLS Protocols";
  private static final List<String> SSL_PROTOCOL_DEFAULT;
  public static final String SSL_PROTOCOL_DOC = "Sets the supported SSL/TLS Protocols for the HTTP client";

  public static final String SSL_CIPHER_SUITES_CONFIG =
      SSL_CONFIG_PREFIX + "cipher.suites";
  public static final String SSL_CIPHER_SUITES_DISPLAYNAME = "Cipher Suites";
  private static final List<String> SSL_CIPHER_SUITES_DEFAULT;
  public static final String SSL_CIPHER_SUITES_DOC = "Sets the supported cipher suites for the HTTP client";

  public static final String CONTENT_LOGGER_ENABLED_CONFIG = "content.logger.enabled";
  public static final String CONTENT_LOGGER_ENABLED_DISPLAYNAME = "Content Logger Enabled";
  private static final Boolean CONTENT_LOGGER_ENABLED_DEFAULT = Boolean.FALSE;
  public static final String CONTENT_LOGGER_ENABLED_DOC = "Enable Content Logging. This will log the request and response contents using the io.axual.connect.plugins.http.sender.ContentLogger SLF4J logger at DEBUG level.";

  public static final String CONTENT_LOGGER_NAME_CONFIG = "content.logger.name";
  public static final String CONTENT_LOGGER_NAME_DISPLAYNAME = "Content Logger Name";
  private static final String CONTENT_LOGGER_NAME_DEFAULT = null;
  public static final String CONTENT_LOGGER_NAME_DOC = "The SLF4J logger name to use for logging the request and response contents at DEBUG level.";


  static {
    SSLContext defaultContext = SSLContexts.createDefault();
    SSL_PROTOCOL_DEFAULT = Arrays.asList(defaultContext.getSupportedSSLParameters().getProtocols());
    SSL_CIPHER_SUITES_DEFAULT = Arrays
        .asList(defaultContext.getSupportedSSLParameters().getCipherSuites());
  }

  /**
   * Creates a custom configuration definition based on the provided configuration properties.<br>
   * The Authentication Provider, Message Formatter and Header Selector are determined and
   * initialized to add their specific configuration definitions to the static definitions of the
   * connector.
   *
   * @param originals the configuration properties to use for the configuration definition
   * @return a custom {@code ConfigDef} object, specific for the provided configuration properties
   */
  public static ConfigDef getConfigurationDefinition(Map<?, ?> originals) {
    AtomicInteger staticHeaderCounter = new AtomicInteger(0);
    //String name, Type type, Importance importance, String documentation, String group, int orderInGroup, Width width, String displayName
    ConfigDef basicConfigDef = new ConfigDef()
        // HTTP Group
        .define(ENDPOINT_CONFIG_KEY)
        .define(METHOD_CONFIG, Type.STRING, METHOD_DEFAULT, Importance.HIGH, METHOD_DOC, HTTP_GROUP,
            1, Width.SHORT, METHOD_DISPLAYNAME)
        .define(VALID_STATUS_CODES_CONFIG, Type.LIST, VALID_STATUS_CODES_DEFAULT, Importance.HIGH,
            VALID_STATUS_CODES_DOC, HTTP_GROUP,
            2, Width.SHORT, VALID_STATUS_CODES_DISPLAYNAME)

        // SSL/TLS Group
        .define(SSL_CERTIFICATE_AUTHORITY_CERTIFICATES_CONFIG, Type.LIST,
            SSL_CERTIFICATE_AUTHORITY_CERTIFICATES_DEFAULT, Importance.HIGH,
            SSL_CERTIFICATE_AUTHORITY_CERTIFICATES_DOC, SSL_GROUP, 0, Width.LONG,
            SSL_CERTIFICATE_AUTHORITY_CERTIFICATES_DISPLAYNAME)
        .define(SSL_CERTIFICATE_AUTHORITY_FILE_LOCATION_CONFIG, Type.LIST,
            SSL_CERTIFICATE_AUTHORITY_FILE_LOCATION_DEFAULT, Importance.HIGH,
            SSL_CERTIFICATE_AUTHORITY_FILE_LOCATION_DOC, SSL_GROUP, 1, Width.LONG,
            SSL_CERTIFICATE_AUTHORITY_FILE_LOCATION_DISPLAYNAME)
        .define(SSL_ENABLE_HOSTNAME_VERIFICATION_CONFIG, Type.BOOLEAN,
            SSL_ENABLE_HOSTNAME_VERIFICATION_DEFAULT, Importance.HIGH,
            SSL_ENABLE_HOSTNAME_VERIFICATION_DOC, SSL_GROUP, 2, Width.SHORT,
            SSL_ENABLE_HOSTNAME_VERIFICATION_DISPLAYNAME)
        .define(SSL_PROTOCOL_CONFIG, Type.LIST,
            SSL_PROTOCOL_DEFAULT, Importance.HIGH,
            SSL_PROTOCOL_DOC, SSL_GROUP, 3, Width.MEDIUM,
            SSL_PROTOCOL_DISPLAYNAME)
        .define(SSL_CIPHER_SUITES_CONFIG, Type.LIST,
            SSL_CIPHER_SUITES_DEFAULT, Importance.HIGH,
            SSL_CIPHER_SUITES_DOC, SSL_GROUP, 4, Width.MEDIUM,
            SSL_CIPHER_SUITES_DISPLAYNAME)

        // Connection group
        .define(CONNECTION_TIMEOUT_MS_CONFIG, Type.INT,
            CONNECTION_TIMEOUT_MS_DEFAULT, Range.atLeast(0), Importance.MEDIUM,
            CONNECTION_TIMEOUT_MS_DOC, CONNECTION_GROUP, 0, Width.SHORT,
            CONNECTION_TIMEOUT_MS_DISPLAYNAME)
        .define(CONNECTION_REQUEST_TIMEOUT_MS_CONFIG, Type.INT,
            CONNECTION_REQUEST_TIMEOUT_MS_DEFAULT, Range.atLeast(0), Importance.MEDIUM,
            CONNECTION_REQUEST_TIMEOUT_MS_DOC, CONNECTION_GROUP, 1, Width.SHORT,
            CONNECTION_REQUEST_TIMEOUT_MS_DISPLAYNAME)
        .define(SOCKET_TIMEOUT_MS_CONFIG, Type.INT,
            SOCKET_TIMEOUT_MS_DEFAULT, Range.atLeast(0), Importance.MEDIUM,
            SOCKET_TIMEOUT_MS_DOC, CONNECTION_GROUP, 2, Width.SHORT,
            SOCKET_TIMEOUT_MS_DISPLAYNAME)
        .define(REDIRECTS_ALLOW_CONFIG, Type.BOOLEAN,
            REDIRECTS_ALLOW_DEFAULT, Importance.MEDIUM,
            REDIRECTS_ALLOW_DOC, CONNECTION_GROUP, 3, Width.SHORT,
            REDIRECTS_ALLOW_DISPLAYNAME)
        .define(REDIRECTS_ALLOW_CIRCULAR_CONFIG, Type.BOOLEAN,
            REDIRECTS_ALLOW_CIRCULAR_DEFAULT, Importance.MEDIUM,
            REDIRECTS_ALLOW_CIRCULAR_DOC, CONNECTION_GROUP, 4, Width.SHORT,
            REDIRECTS_ALLOW_CIRCULAR_DISPLAYNAME)
        .define(REDIRECTS_MAXIMUM_CONFIG, Type.INT,
            REDIRECTS_MAXIMUM_DEFAULT, Range.atLeast(0), Importance.MEDIUM,
            REDIRECTS_MAXIMUM_DOC, CONNECTION_GROUP, 5, Width.SHORT,
            REDIRECTS_MAXIMUM_DISPLAYNAME)

        // Retry group
        .define(RETRY_MAXIMUM_CONFIG, Type.INT,
            RETRY_MAXIMUM_DEFAULT, Range.atLeast(0), Importance.MEDIUM,
            RETRY_MAXIMUM_DOC, RETRY_GROUP, 0, Width.SHORT,
            RETRY_MAXIMUM_DISPLAYNAME)
        .define(RETRY_WAIT_MS_CONFIG, Type.INT,
            RETRY_WAIT_MS_DEFAULT, Range.atLeast(0), Importance.MEDIUM,
            RETRY_WAIT_MS_DOC, RETRY_GROUP, 1, Width.SHORT,
            RETRY_WAIT_MS_DISPLAYNAME)

        // Content group
        .define(MESSAGE_FORMATTER_CLASS_CONFIG, Type.STRING,
            MESSAGE_FORMATTER_CLASS_DEFAULT, new ClassValidator(IMessageFormatter.class),
            Importance.MEDIUM,
            MESSAGE_FORMATTER_CLASS_DOC, CONTENT_GROUP, 0, Width.MEDIUM,
            MESSAGE_FORMATTER_CLASS_DISPLAYNAME)
        .define(HEADER_SELECTOR_CLASS_CONFIG, Type.STRING,
            HEADER_SELECTOR_CLASS_DEFAULT, new ClassValidator(IHeaderSelector.class),
            Importance.MEDIUM,
            HEADER_SELECTOR_CLASS_DOC, CONTENT_GROUP, 2, Width.MEDIUM,
            HEADER_SELECTOR_CLASS_DISPLAYNAME)
        .define(AUTHENTICATION_PROVIDER_CLASS_CONFIG, Type.STRING,
            AUTHENTICATION_PROVIDER_CLASS_DEFAULT,
            new ClassValidator(IAuthenticationProvider.class), Importance.MEDIUM,
            AUTHENTICATION_PROVIDER_CLASS_DOC, CONTENT_GROUP, 3, Width.MEDIUM,
            AUTHENTICATION_PROVIDER_CLASS_DISPLAYNAME)
        .define(CONTENT_TYPE_CONFIG, Type.STRING,
                CONTENT_TYPE_DEFAULT, new ContentTypeValidator(),
                Importance.MEDIUM,
                CONTENT_TYPE_DOC, CONTENT_GROUP, 4, Width.MEDIUM,
                CONTENT_TYPE_DISPLAYNAME)

        // Logging group
        .define(CONTENT_LOGGER_ENABLED_CONFIG, Type.BOOLEAN,
            CONTENT_LOGGER_ENABLED_DEFAULT, Importance.HIGH,
            CONTENT_LOGGER_ENABLED_DOC, LOGGING_GROUP, 0, Width.SHORT,
            CONTENT_LOGGER_ENABLED_DISPLAYNAME)
        .define(CONTENT_LOGGER_NAME_CONFIG, Type.STRING,
            CONTENT_LOGGER_NAME_DEFAULT, Importance.HIGH,
            CONTENT_LOGGER_NAME_DOC, LOGGING_GROUP, 1, Width.MEDIUM,
            CONTENT_LOGGER_NAME_DISPLAYNAME)

        // Static header group
        .define(STATIC_HEADER_ALIAS_CONFIG, Type.LIST,
            STATIC_HEADER_ALIAS_DEFAULT, Importance.MEDIUM,
            STATIC_HEADER_ALIAS_DOC, STATIC_HEADER_GROUP, staticHeaderCounter.getAndIncrement(),
            Width.MEDIUM,
            STATIC_HEADER_ALIAS_DISPLAYNAME);

    Object staticHeaderAliases = ConfigDef
        .parseType(STATIC_HEADER_ALIAS_CONFIG, originals.get(STATIC_HEADER_ALIAS_CONFIG),
            Type.LIST);
    if (staticHeaderAliases instanceof List) {
      LinkedHashSet<?> uniqueHeaderAliases = new LinkedHashSet<>((List<?>) staticHeaderAliases);
      for (Object aliasObj : uniqueHeaderAliases) {
        if (!(aliasObj instanceof String)) {
          throw new ConfigException(
              "Items in " + STATIC_HEADER_ALIAS_CONFIG + " is not of type String");
        }
        String alias = (String) aliasObj;
        basicConfigDef
            .define(String.format(STATIC_HEADER_NAME_CONFIG_FORMAT, alias), Type.STRING,
                STATIC_HEADER_NAME_DEFAULT, Importance.MEDIUM,
                String.format(STATIC_HEADER_NAME_DOC_FORMAT, alias), STATIC_HEADER_GROUP,
                staticHeaderCounter.getAndIncrement(), Width.MEDIUM,
                String.format(STATIC_HEADER_NAME_DISPLAYNAME_FORMAT, alias))
            .define(String.format(STATIC_HEADER_VALUE_CONFIG_FORMAT, alias), Type.STRING,
                STATIC_HEADER_VALUE_DEFAULT, Importance.MEDIUM,
                String.format(STATIC_HEADER_VALUE_DOC_FORMAT, alias), STATIC_HEADER_GROUP,
                staticHeaderCounter.getAndIncrement(), Width.MEDIUM,
                String.format(STATIC_HEADER_VALUE_DISPLAYNAME_FORMAT, alias))
        ;
      }
    }

    embedIConfigurableConfigDef(basicConfigDef, originals,
        MESSAGE_FORMATTER_CLASS_CONFIG, MESSAGE_FORMATTER_CLASS_DEFAULT,
        MESSAGE_FORMATTER_CLASS_PARAM_PREFIX, MESSAGE_FORMATTER_CLASS_GROUP_PREFIX);
    embedIConfigurableConfigDef(basicConfigDef, originals,
        HEADER_SELECTOR_CLASS_CONFIG, HEADER_SELECTOR_CLASS_DEFAULT,
        HEADER_SELECTOR_CLASS_PARAM_PREFIX, HEADER_SELECTOR_CLASS_GROUP_PREFIX);
    embedIConfigurableConfigDef(basicConfigDef, originals,
        AUTHENTICATION_PROVIDER_CLASS_CONFIG, AUTHENTICATION_PROVIDER_CLASS_DEFAULT,
        AUTHENTICATION_PROVIDER_CLASS_PARAM_PREFIX, AUTHENTICATION_PROVIDER_CLASS_GROUP_PREFIX);

    return basicConfigDef;
  }

  /**
   * Some {@link IAuthenticationProvider} implementations need the domain and or host of the
   * endpoint to set an authentication mechanism. This update configuration will update the provided
   * configuration properties with the configuration.
   *
   * @param originals the original configuration properties
   * @return the updated configuration properties
   */
  public static Map<String, Object> updateConfigurations(Map<?, ?> originals) {
    // enrich the authentication provider config with the endpoint config
    Map<String, Object> updatedConfig = extractConfig(originals, "");
    String targetKey = AUTHENTICATION_PROVIDER_CLASS_PARAM_PREFIX + ENDPOINT_CONFIG;
    updatedConfig.putIfAbsent(targetKey, originals.get(ENDPOINT_CONFIG));
    return updatedConfig;
  }

  /**
   * This method will load the {@link IConfigurable} implementation defined in the configName or the
   * default value, instantiate the implementation and retrieve the configuration definition.<br>
   * Then it will embed the retrieved configuration definition into the provided base definition,
   * prefixing the config name and group name with the supplied value.
   *
   * @param baseConfigDef the original configuration definition where the extra definitions need to
   *                      be embedded
   * @param originals     the original configuration properties, the properties for the {@link
   *                      IConfigurable} implementation will be extracted from this
   * @param configName    the key to use to retrieve the name of the {@link IConfigurable} to load
   * @param configDefault the name of the {@link IConfigurable} to load, in case the configName was
   *                      not provided in the properties.
   * @param configPrefix  the value to prefix the configuration definition keys from {@link
   *                      IConfigurable} with to embed them in the baseConfigDef
   * @param groupPrefix   the value to prefix the configuration definition groups from {@link
   *                      IConfigurable} with to embed them in the baseConfigDef
   * @return the enriched configuration definition
   */
  public static ConfigDef embedIConfigurableConfigDef(ConfigDef baseConfigDef, Map<?, ?> originals,
      String configName,
      String configDefault, String configPrefix, String groupPrefix) {
    Object iConfigurableClassObj = ConfigDef
        .parseType(configName, originals.get(configName), Type.STRING);
    final String iConfigurableClassName;
    if (iConfigurableClassObj instanceof String) {
      iConfigurableClassName = (String) iConfigurableClassObj;
    } else {
      iConfigurableClassName = configDefault;
    }

    IConfigurable configurable = ClassLoadHelper.INSTANCE
        .newInstance(IConfigurable.class, iConfigurableClassName);
    Map<String, Object> extractedConfig = extractConfig(originals, configPrefix);
    ConfigDef configDefFromConfigurable = configurable.configDefinition(extractedConfig);

    baseConfigDef.embed(configPrefix, groupPrefix, 0, configDefFromConfigurable);

    return baseConfigDef;
  }

  /**
   * Takes the configuration properties, extracts all entries with a key prefixed with the specified
   * prefix and returns these entries in a new typed Map. It will remove the prefixes from the
   * keys.
   *
   * @param originals the original configuration properties
   * @param prefix    the prefix to check for and remove
   * @return a Map containing only the entries that were prefixed with the specified prefix
   */
  public static Map<String, Object> extractConfig(Map<?, ?> originals, String prefix) {
    return originals.entrySet().stream()
        .filter(entry -> entry.getKey() instanceof String && ((String) entry.getKey())
            .startsWith(prefix))
        .map(entry -> cleanEntryKey(entry, prefix))
        .collect(Collectors.toMap(entry -> entry.getKey(), Entry::getValue));
  }

  // Helper to clean the entry and use specific types
  private static Map.Entry<String, Object> cleanEntryKey(Map.Entry<?, ?> entry, String prefix) {
    String key = ((String) entry.getKey()).substring(prefix.length());
    Object value = entry.getValue();
    return new SimpleEntry<>(key, value);
  }

  /**
   * Construct the configuration based on the provided properties. The configuration definition
   * created is specific for the properties supplied.
   *
   * @param originals the configuration properties to use
   * @see #getConfigurationDefinition(Map)
   * @see #updateConfigurations(Map)
   */
  public HttpSinkConnectorConfig(Map<?, ?> originals) {
    super(getConfigurationDefinition(originals), updateConfigurations(originals));
  }

  /**
   * Get the configured endpoint.
   *
   * @return the endpoint
   * @see #ENDPOINT_CONFIG
   */
  public String getEndpoint() {
    return getString(ENDPOINT_CONFIG);
  }

  /**
   * Get the configured HTTP method.
   *
   * @return the HTTP method
   * @see #METHOD_CONFIG
   */
  public String getMethod() {
    return getString(METHOD_CONFIG);
  }

  /**
   * Get the list of valid HTTP status codes.
   *
   * @return the list of valid HTTP status codes
   * @see #VALID_STATUS_CODES_CONFIG
   */
  public List<Integer> getValidStatusCodes() {
    return getList(VALID_STATUS_CODES_CONFIG).stream()
        .map(Integer::parseInt)
        .collect(Collectors.toList());
  }

  /**
   * Get an instance of the specified {@link IAuthenticationProvider} which is not yet
   * configured.<br> A new instance is created everytime this method called.
   *
   * @return an instance of the Authentication Provider
   * @see #AUTHENTICATION_PROVIDER_CLASS_CONFIG
   */
  public IAuthenticationProvider getAuthenticationProvider() {
    return ClassLoadHelper.INSTANCE
        .newInstance(IAuthenticationProvider.class,
            getString(AUTHENTICATION_PROVIDER_CLASS_CONFIG));
  }

  /**
   * Get an instance of the specified {@link IMessageFormatter} which is not yet configured.<br> A
   * new instance is created everytime this method called.
   *
   * @return an instance of the Message Formatter
   * @see #MESSAGE_FORMATTER_CLASS_CONFIG
   */
  public IMessageFormatter getMessageFormatter() {
    return ClassLoadHelper.INSTANCE
        .newInstance(IMessageFormatter.class, getString(MESSAGE_FORMATTER_CLASS_CONFIG));
  }

  /**
   * Get an instance of the specified {@link IHeaderSelector} which is not yet configured.<br> A new
   * instance is created everytime this method called.
   *
   * @return an instance of Header Selector
   * @see #HEADER_SELECTOR_CLASS_CONFIG
   */
  public IHeaderSelector getHeaderSelector() {
    return ClassLoadHelper.INSTANCE
        .newInstance(IHeaderSelector.class, getString(HEADER_SELECTOR_CLASS_CONFIG));
  }

  /**
   * Get the list of allowed SSL protocols.<br> If none are specified the JVM default SSL protocols
   * are used.
   *
   * @return the list of SSL protocols
   * @see #SSL_PROTOCOL_CONFIG
   */
  public List<String> getSslProtocols() {
    return getList(SSL_PROTOCOL_CONFIG);
  }

  /**
   * Get the list of allowed SSL cipher suites.<br> If none are specified the JVM default SSL cipher
   * suites are used.
   *
   * @return the list of SSL cipher suites
   * @see #SSL_CIPHER_SUITES_CONFIG
   */
  public List<String> getSslCipherSuites() {
    return getList(SSL_CIPHER_SUITES_CONFIG);
  }

  /**
   * Get the indicator if the hostname should be verified against the valid names provided in the
   * server certificate.
   *
   * @return true if hostname verification should be enabled
   * @see #SSL_ENABLE_HOSTNAME_VERIFICATION_CONFIG
   */
  public Boolean getSslEnableHostnameVerification() {
    return getBoolean(SSL_ENABLE_HOSTNAME_VERIFICATION_CONFIG);
  }

  /**
   * Get the list of paths to the PEM encoded X509 certificate files.
   *
   * @return the list paths to the certificate files
   * @see #SSL_CERTIFICATE_AUTHORITY_FILE_LOCATION_CONFIG
   */
  public List<String> getSslCertificateAuthorityFileLocation() {
    return getList(SSL_CERTIFICATE_AUTHORITY_FILE_LOCATION_CONFIG);
  }

  /**
   * Get the list of PEM encoded X509 certificates.
   *
   * @return the list of PEM encoded X509 certificates
   * @see #SSL_CERTIFICATE_AUTHORITY_CERTIFICATES_CONFIG
   */
  public List<String> getSslCertificateAuthorityCertificates() {
    return getList(SSL_CERTIFICATE_AUTHORITY_CERTIFICATES_CONFIG);
  }

  /**
   * Get the connection timeout for the HTTP Request.
   *
   * @return the connection timeout in milliseconds
   * @see #CONNECTION_TIMEOUT_MS_CONFIG
   */
  public Integer getConnectionTimeoutMs() {
    return getInt(CONNECTION_TIMEOUT_MS_CONFIG);
  }

  /**
   * Get the connection request timeout for getting a connection from the connection pool.
   *
   * @return the connection reques timeout in milliseconds
   * @see #CONNECTION_REQUEST_TIMEOUT_MS_CONFIG
   */
  public Integer getConnectionRequestTimeoutMs() {
    return getInt(CONNECTION_REQUEST_TIMEOUT_MS_CONFIG);
  }

  /**
   * Get the socket timeout for the connection to the HTTP endpoint.
   *
   * @return the socket timeout in milliseconds
   * @see #SOCKET_TIMEOUT_MS_CONFIG
   */
  public Integer getSocketTimeoutMs() {
    return getInt(SOCKET_TIMEOUT_MS_CONFIG);
  }

  /**
   * Returns true if HTTP redirects are allowed and followed.
   *
   * @return true if redirects are allowed
   * @see #REDIRECTS_ALLOW_CONFIG
   */
  public Boolean allowRedirects() {
    return getBoolean(REDIRECTS_ALLOW_CONFIG);
  }

  /**
   * Returns true if circular HTTP redirects are allowed and followed.
   *
   * @return true if redirects are allowed
   * @see #REDIRECTS_ALLOW_CIRCULAR_CONFIG
   */
  public Boolean allowCircularRedirects() {
    return getBoolean(REDIRECTS_ALLOW_CIRCULAR_CONFIG);
  }

  /**
   * Get the number of redirects to follow before failing.
   *
   * @return the maximum number of redirects to follow
   * @see #REDIRECTS_MAXIMUM_CONFIG
   */
  public Integer getMaximumRedirects() {
    return getInt(REDIRECTS_MAXIMUM_CONFIG);
  }

  /**
   * Get the number of retries before failing the send request.
   *
   * @return the maximum number of retries
   * @see #RETRY_MAXIMUM_CONFIG
   */
  public Integer getMaximumRetries() {
    return getInt(RETRY_MAXIMUM_CONFIG);
  }

  /**
   * Get the number of milliseconds to wait between retry attempts.
   *
   * @return the number of milliseconds to wait before attempting a retry
   * @see #RETRY_WAIT_MS_CONFIG
   */
  public Integer getRetryWaitMs() {
    return getInt(RETRY_WAIT_MS_CONFIG);
  }

  /**
   * Get the Content Type to use when sending the HTTP requests.
   *
   * @return the Content Type to use
   * @see #CONTENT_TYPE_CONFIG
   */
  public String getContentType() {
    return getString(CONTENT_TYPE_CONFIG);
  }

  /**
   * Get the static HTTP headers to send with every HTTP request.
   *
   * @return a map containing the header names and values
   * @see #STATIC_HEADER_ALIAS_CONFIG
   * @see #STATIC_HEADER_NAME_CONFIG_FORMAT
   */
  public Map<String, String> getStaticHeaders() {
    Map<String, String> headers = new HashMap<>();
    for (String alias : getList(STATIC_HEADER_ALIAS_CONFIG)) {
      String configNameKey = String.format(STATIC_HEADER_NAME_CONFIG_FORMAT, alias);
      String configNameValue = String.format(STATIC_HEADER_VALUE_CONFIG_FORMAT, alias);
      headers.put(getString(configNameKey), getString(configNameValue));
    }
    return headers;
  }

  /**
   * Get the name to use for the SLF4J Logger which will log the contents of the HTTP requests,
   * responses and exceptions.<br> If the content logger is disabled this method will return null.
   *
   * @return the name of the Content Logger or null for no Content Logging
   * @see #CONTENT_LOGGER_ENABLED_CONFIG
   * @see #CONTENT_LOGGER_NAME_CONFIG
   */
  public String getContentLoggerName() {
    String name = null;
    if (Boolean.TRUE.equals(getBoolean(CONTENT_LOGGER_ENABLED_CONFIG))) {
      name = getString(CONTENT_LOGGER_NAME_CONFIG);
      if (name == null) {
        // Get connector name, not defined in a global config
        name = originalsStrings().get("name");
      }
    }
    return name;
  }

  /**
   * A validator that checks if the used String value is a class implementing a specific target
   * class or interface.
   */
  static class ClassValidator implements Validator {

    final Class<?> targetClass;

    /**
     * Construct the validator for the specified target class
     *
     * @param targetClass the targeted base class that the configured class must implement or
     *                    extend
     */
    ClassValidator(Class<?> targetClass) {
      if (targetClass == null) {
        throw new IllegalArgumentException("TargetClass cannot be null");
      }
      this.targetClass = targetClass;
    }

    @Override
    public void ensureValid(String name, Object value) {
      if (value == null) {
        throw new ConfigException(name, value, "Value can not be null");
      }

      if (value instanceof String) {
        try {
          Class<?> clazz = ClassLoadHelper.INSTANCE.classForName((String) value);
          if (!targetClass.isAssignableFrom(clazz)) {
            throw new ConfigException(name, value,
                "Class does not implement or extend " + targetClass.getName());
          }
        } catch (ClassNotFoundException cnfe) {
          throw new ConfigException(name, value, "Could not find class");
        }
      } else {
        throw new ConfigException(name, value,
            "Value must be of type String, got " + value.getClass().getName());
      }
    }
  }

}
