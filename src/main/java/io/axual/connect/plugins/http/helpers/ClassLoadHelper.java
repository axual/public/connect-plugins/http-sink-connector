package io.axual.connect.plugins.http.helpers;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.http.exceptions.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>Helper for classloading</p>
 * <p>Classloading for connector resources can be sensitive because a connector might be loaded in
 * plugin isolation. <br> The standard Kafka Classloading utilities cannot be used reliably in this
 * case. This simple helper provides the loading and instantiation implementation to use in the
 * connector.</p>
 */
public class ClassLoadHelper {

  private static final Logger LOG = LoggerFactory.getLogger(ClassLoadHelper.class);

  public static final ClassLoadHelper INSTANCE = new ClassLoadHelper();

  private final ClassLoader classLoader;

  private ClassLoadHelper() {
    classLoader = ClassLoadHelper.class.getClassLoader();
  }

  /**
   * Loads and creates a new instance of the class specified by the classname, and cast to the
   * baseClass.<br> The class needs to have a public constructor without parameters defined for
   * instantiations
   *
   * @param baseClass the base class or interface that the targeted class implements
   * @param className the name of the class to be loaded
   * @param <T>       the type of the baseClass to be used
   * @return An instance targeted className cast to the type defined by the baseClass
   */
  public <T> T newInstance(Class<T> baseClass, String className) {
    LOG.debug("Creating new instance of class {} which implements {}", className, baseClass);
    Class<? extends T> foundClass = classForName(baseClass, className);
    return createInstance(foundClass);
  }

  /**
   * Loads the class based on the className, and casts it to the baseClass.
   *
   * @param baseClass the base class or interface that the targeted class implements
   * @param className the name of the class to be loaded
   * @param <T>       the type of the baseClass to be used
   * @return The Class of the targeted className
   */
  public <T> Class<? extends T> classForName(Class<T> baseClass, String className) {
    LOG.debug("Getting Class for {} which implements {}", className, baseClass);
    try {
      return classForName(className).asSubclass(baseClass);
    } catch (ClassNotFoundException | ClassCastException e) {
      throw new ConfigurationException("Could not find class " + className, e);
    }
  }

  /**
   * Loads the class based on the className using the ClassLoader of the {@code ClossLoadHelper}
   *
   * @param className the name of the class to be loaded
   * @return The Class of the targeted className
   * @throws ClassNotFoundException thrown when the className cannot be found by the classloader
   */
  public Class<?> classForName(String className) throws ClassNotFoundException {
    LOG.debug("Getting Class for {}", className);
    return Class.forName(className, true, classLoader);
  }

  /**
   * Create an instance of {@code Class<T>}. <br> The class must have a public constructor without
   * parameters.
   *
   * @param toInstantiate the {@code Class} for the
   * @param <T>           The type of the class to instantiate
   * @return An instance of the provided {@code Class}
   */
  public <T> T createInstance(Class<T> toInstantiate) {
    LOG.debug("Creating instance of {}", toInstantiate);
    if (toInstantiate == null) {
      throw new ConfigurationException("Class to instantiate cannot be null or empty");
    }
    try {
      return toInstantiate.getDeclaredConstructor().newInstance();
    } catch (NoSuchMethodException e) {
      throw new ConfigurationException(
          "Could not find public constructor without arguments for class " + toInstantiate
              .getName(), e);
    } catch (ReflectiveOperationException | RuntimeException e) {
      throw new ConfigurationException("Could not instantiate class " + toInstantiate.getName(), e);
    }
  }

}
