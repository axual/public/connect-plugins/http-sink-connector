package io.axual.connect.plugins.http.helpers;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.connect.plugins.http.exceptions.HttpSinkConnectorException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import javax.net.ssl.SSLContext;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>Helper for creating SSL Certificates and SSL Contexts</p>
 */
public class SslHelper {

  private static final Logger LOG = LoggerFactory.getLogger(SslHelper.class);

  public static final SslHelper INSTANCE = new SslHelper();

  private static final String CERTIFICATE_CLEAN_REGEX = "(-----BEGIN CERTIFICATE-----|-----END CERTIFICATE-----|\\R)";
  private static final String CERTIFICATE_SPLIT_REGEX = "(-----BEGIN CERTIFICATE-----|-----END CERTIFICATE-----)";

  /**
   * <p>
   * Creates an {@code SSLContext} from the provided certificate files and certificate strings.<br>
   * The certificate files and strings are PEM encoded X509 certificates.<br> If the files and
   * strings did not contains any certificates the {@code org.apache.http.ssl.SSLContexts.createSystemDefault()}
   * is called to create a SSLContext
   * </p>
   * <p>
   * Valid PEM encoded certificates can be recognized by the following format<br>
   * <pre>
   *  -----BEGIN CERTIFICATE-----
   *  BASE64 line
   *  BASE64 line
   *  -----END CERTIFICATE-----
   *  </pre>
   * <br> Multiple Certificates per file or string are allowed, each will be regarded as a separate
   * valid Certificate Authority. They must use the following format<br>
   * <pre>
   *  -----BEGIN CERTIFICATE-----
   *  BASE64 line
   *  BASE64 line
   *  -----END CERTIFICATE-----
   *  -----BEGIN CERTIFICATE-----
   *  BASE64 line
   *  BASE64 line
   *  -----END CERTIFICATE-----
   *  </pre>
   *
   * @param certificateFiles   The paths to files containing PEM encoded X509 Certificates of the
   *                           Certificate Authorities to trust
   * @param certificateStrings A list of PEM encoded X509 Certificates of the Certificate
   *                           Authorities to trust
   * @return an SSLContext trusting the certificate authorities specified by the input, or the
   * system default SSLContext if no certificate authorities where specified
   */
  public SSLContext getContext(final Collection<String> certificateFiles,
      final Collection<String> certificateStrings) {
    LOG.debug("Getting context");

    List<String> allCertificateStrings = new LinkedList<>();

    // Insert all provided certificate strings
    if (certificateStrings != null) {
      LOG.debug("Adding {} certificate strings", certificateStrings.size());
      allCertificateStrings.addAll(certificateStrings);
    }

    // Add the certificates read from file
    LOG.debug("Reading and addind certificate strings from files");
    allCertificateStrings.addAll(getCertificateStringsFromFiles(certificateFiles));

    // Create default context if the certificate strings collection is empty
    if (allCertificateStrings.isEmpty()) {
      LOG.debug("No certificate strings found, creating system default SSLContext");
      return SSLContexts.createSystemDefault();
    }

    // Create the keystore and SSL context
    LOG.debug("Creating KeyStore from certificates");
    KeyStore trustStore = openTruststore(allCertificateStrings);
    try {
      LOG.debug("Creating custom SSL Context");
      return SSLContexts.custom()
          .loadTrustMaterial(trustStore, null)
          .build();
    } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
      throw new HttpSinkConnectorException("Could not create truststore", e);
    }
  }

  /**
   * <p>
   * Opens a {@code KeyStore}, load the provided certificate strings to {@code Certificate} and
   * loads the certificates into the {@code KeyStore}.
   * </p>
   *
   * @param certificateStrings the list of PEM encoded X509 Certificates to load
   * @return A {@code KeyStore} object containing the provided certificates
   */
  public KeyStore openTruststore(Collection<String> certificateStrings) {
    LOG.trace("Creating Keystore");
    try {
      KeyStore keyStore = KeyStore.getInstance("jks");
      keyStore.load(null, new char[0]);

      LOG.debug("Creating certificates from certificate strings");
      Collection<Certificate> certificates = getCertificatesFromString(certificateStrings);

      int aliasCounter = 1;
      for (Certificate certificate : certificates) {
        LOG.debug("Adding certificate");
        keyStore.setCertificateEntry("cert_" + aliasCounter++, certificate);
      }
      return keyStore;
    } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException e) {
      throw new HttpSinkConnectorException("Could not create Keystore", e);
    }
  }

  /**
   * Reads the contents of each of the files into a {@code String} and adds it to the result list.
   *
   * @param certificateFiles A list of paths to the files to be read
   * @return a list of the String contents of the files
   */
  public Collection<String> getCertificateStringsFromFiles(Collection<String> certificateFiles) {
    LOG.debug("Loading certificate strings from files");
    LinkedList<String> certificateStrings = new LinkedList<>();
    if (certificateFiles != null) {
      for (String certificateFile : certificateFiles) {
        LOG.debug("Loading certificate string file {}", certificateFile);
        try {
          certificateStrings.add(new String(Files.readAllBytes(Paths.get(certificateFile)),
              StandardCharsets.UTF_8));
        } catch (IOException e) {
          throw new HttpSinkConnectorException(
              "Could not read all bytes for file " + certificateFile, e);
        }
      }
    }
    return certificateStrings;
  }

  /**
   * <p>Transforms each of the provided Strings into a list of X509 Certificates</p>
   * <p>A single String entry may contain multiple PEM encoded X509 Certificates</p>
   *
   * @param certificateStrings a collection of PEM encoded X509 Certificate Strings
   * @return A collection of Certificates, loaded from the provided Strings
   */
  public Collection<Certificate> getCertificatesFromString(Collection<String> certificateStrings) {
    LOG.debug("Getting certificates from strings");
    try {
      CertificateFactory factory = CertificateFactory.getInstance("X.509");
      Decoder decoder = Base64.getDecoder();
      if (certificateStrings == null) {
        return Collections.emptyList();
      }
      return certificateStrings.stream()
          .filter(crt -> crt != null && !crt.isEmpty())
          .flatMap(crt ->
              Arrays.stream(crt.split(CERTIFICATE_SPLIT_REGEX)))
          .map(crt -> crt.replaceAll(CERTIFICATE_CLEAN_REGEX, ""))
          .filter(crt -> !crt.isEmpty())
          .map(decoder::decode)
          .map(ByteArrayInputStream::new)
          .map(input -> {
            try {
              return factory.generateCertificate(input);
            } catch (CertificateException ce) {
              throw new HttpSinkConnectorException("Could not generate certificateStrings", ce);
            }
          })
          .collect(Collectors.toList());
    } catch (CertificateException ce) {
      throw new HttpSinkConnectorException("Could not generate certificateStrings", ce);
    }
  }
}
