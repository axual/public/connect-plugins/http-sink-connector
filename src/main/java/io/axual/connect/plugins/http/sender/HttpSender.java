package io.axual.connect.plugins.http.sender;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.io.Closeable;
import java.io.IOException;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.kafka.connect.sink.SinkRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>The HTTP Sender is the functional HTTP Client class of the HTTP Sink Connector.
 * It will create a client based on the {@link HttpSenderConfiguration} provided using the {@link
 * #configure(HttpSenderConfiguration)} method.</p>
 *
 * <p>During configuration the Authorization Provider is ysed to prepare authorization for the
 * Apache HttpClient. The static headers are also added as default headers and the ContentLogger is
 * saved. <br> The timeouts and retry settings are created, and the HttpClient is initialized to be
 * used</p>
 *
 * <p>The sending of a record is controlled by the {@link #sendRecord(SinkRecord)} method.<br>
 * The sendRecord will call the configured {@link IMessageFormatter} to create the required Apache
 * HttpEntity to send, followed by a call to the configured {@link IHeaderSelector} to collect the
 * Kafka Headers to be forwarded.<br>
 * </p>
 */
public class HttpSender implements Closeable {

  private static final Logger LOG = LoggerFactory.getLogger(HttpSender.class);

  private HttpSenderConfiguration configuration = null;
  private CloseableHttpClient httpClient = null;
  private Optional<ContentLogger> contentLogger = Optional.empty();

  /**
   * This will configure the sender using the provided configuration. Any subsequent calls to
   * configure will be ignored, unless the {@link #close()} method is called.
   *
   * @param configuration Contains the configuration for the {@code HttpSender}
   */
  public void configure(HttpSenderConfiguration configuration) {
    if (this.configuration == null) {
      this.configuration = configuration;
      HttpClientBuilder clientBuilder = createClientBuilderWithAuthentication(
          configuration.getAuthenticationProvider());
      Set<Header> defaultHeaders = configuration.getStaticHeaders()
          .entrySet()
          .stream()
          .map(entry -> new BasicHeader(entry.getKey(), entry.getValue()))
          .collect(Collectors.toSet());
      this.contentLogger = Optional.ofNullable(configuration.getContentLogger());

      this.httpClient = clientBuilder
          .setSSLSocketFactory(configuration.getSslConnectionSocketFactory())
          .setServiceUnavailableRetryStrategy(configuration.getRetryStrategy())
          .setRetryHandler(configuration.getRetryStrategy())
          .setMaxConnTotal(100)
          .setMaxConnPerRoute(100)
          .setConnectionTimeToLive(5, TimeUnit.MINUTES)
          .setDefaultRequestConfig(configuration.getRequestConfig())
          .setRedirectStrategy(
              new DefaultRedirectStrategy(new String[]{configuration.getHttpMethod()}))
          .setDefaultHeaders(defaultHeaders)
          .build();
    } else {
      LOG.warn("Already configured, skipping configuration");
    }
  }

  // Initialises the HttpClientBuilder and adds the authentication.
  HttpClientBuilder createClientBuilderWithAuthentication(IAuthenticationProvider provider) {
    return provider.addAuthentication(HttpClients.custom());
  }

  // handles the Response object to check if the status indicates success or not
  private HttpSenderResult createSenderResult(CloseableHttpResponse response) {
    final int statusCode = response.getStatusLine().getStatusCode();
    LOG.debug("Received status code {}", statusCode);
    boolean isSuccess = (statusCode >= 200 && statusCode < 300);
    return new HttpSenderResult(isSuccess, response.getStatusLine(), null);
  }

  // handles the exception caught when trying to send the request
  private HttpSenderResult createSenderResult(Exception exception) {
    LOG.debug("An exception occured when sending request");
    return new HttpSenderResult(false, null, exception);
  }

  /**
   * <p>This method will take the {@code SinkRecord} and create a HTTP Request from it using the
   * configuration provided in the {@link #configure(HttpSenderConfiguration) call}.</p>
   *
   * <p>The method will create a {@link HttpSenderRequest} from the request and use it to format
   * the
   * {@code SinkRecord} to get the {@code HttpEntity} to set as payload.<br> Next it will extract
   * the Kafka headers to forward and create and send the HTTP Request.<br> It will handle the HTTP
   * Response or Exception to return a {@link HttpSenderResult}.</p>
   *
   * <p>The contents of the request, response or exception will be logged with a {@link
   * ContentLogger}.</p>
   *
   * @param record The {@code SinkRecord} received from Kafka to send to a HTTP Service
   * @return A result object containing the success indication of the send request.
   */
  public HttpSenderResult sendRecord(SinkRecord record) {
    final HttpSenderRequest senderRequest = new HttpSenderRequest(record);

    configuration.getMessageFormatter()
        .formatMessage(senderRequest, configuration.getContentType());
    configuration.getHeaderSelector().selectHeaders(senderRequest);

    RequestBuilder requestBuilder = RequestBuilder.create(configuration.getHttpMethod())
        .setUri(configuration.getEndpoint())
        .setEntity(senderRequest.getEntity());

    senderRequest.getSelectedHeaders().forEach(requestBuilder::addHeader);

    // call HttpClient
    LOG.debug("Calling endpoint");
    final HttpUriRequest request = requestBuilder.build();
    try (CloseableHttpResponse response = httpClient
        .execute(request)) {
      contentLogger.ifPresent(logger -> logger.log(request, response));
      return createSenderResult(response);
    } catch (Exception e) {
      contentLogger.ifPresent(logger -> logger.log(request, e));
      return createSenderResult(e);
    }
  }

  @Override
  public void close() {
    if (httpClient != null) {
      LOG.debug("Closing HTTP Client");
      try {
        httpClient.close();
      } catch (IOException e) {
        LOG.warn("Could not close HTTP Client", e);
      }
      httpClient = null;
      configuration = null;
    }
  }

}
