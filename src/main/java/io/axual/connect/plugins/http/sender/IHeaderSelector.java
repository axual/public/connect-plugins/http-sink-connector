package io.axual.connect.plugins.http.sender;

/*-
 * ========================LICENSE_START=================================
 * HTTP Sink Connector for Kafka Connect
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


/**
 * This interface is used for extracting the HTTP Headers from the original record. It extends
 * {@link IConfigurable} for optional configuration.
 */
public interface IHeaderSelector extends IConfigurable {

  /**
   * Select and create HTTP headers from the original SinkRecord and add it to the {@link
   * HttpSenderRequest#getSelectedHeaders()}
   *
   * @param request The container for storing the original SinkRecord and the HTTP Headers and
   *                Entity to set
   * @return the HttpSenderRequest instance given as a parameter
   */
  HttpSenderRequest selectHeaders(HttpSenderRequest request);

}
